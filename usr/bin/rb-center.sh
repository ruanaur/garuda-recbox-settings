#!/usr/bin/sh

# VERSION=2.1

export RED='\033[0;31m'
export GREEN='\033[0;32m'
export YELLOW='\033[0;33m'
export WHITE='\033[0;37m'
export RESETCOLOR='\033[1;00m'

MAIN_TITLE="RecBox Center - Main menu"
PRO_AUDIO_TITLE="RecBox Center - Pro Audio Software"
PRO_AUDIO_MULTI_TITLE="RecBox Center - Pro Audio Plugins"
DSSI_TITLE="RecBox Center - DSSI software"
DSSI_MULTI_TITLE="RecBox Center - DSSI Plugins"
LADSPA_TITLE="RecBox Center - LADSPA software"
LADSPA_MULTI_TITLE="RecBox Center - LADSPA Plugins"
LV2_TITLE="RecBox Center - LV2 Plugins"
LV2_MULTI_TITLE="RecBox Center - LV2 Plugins"
VST_TITLE="RecBox Center - VST software"
VST_MULTI_TITLE="RecBox Center - VST software"
NON_DAW_TITLE="RecBox Center - Non DAW"
NON_DAW_MULTI_TITLE="RecBox Center - Non DAW"
RECBOX_ONES_TITLE="RecBox Center - Selected by RecBox"
EXTRA_PLUGINS_TITLE="RecBox Center - Extra Software"
DAW_TITLE="RecBox Center - Digital Audio Workstation (DAW)"
REAL_TIME_TITLE="RecBox Center - Real Time Kernels"
TOOLS_TITLE="RecBox Center - Kernel Tools"
PANEL_MENU_TITLE="RecBox Center - Studio panel menus"

MAIN_TEXT="<b>Welcome in RecBox Center.</b>\n\nHere you can find and install Pro Audio plugins and software from Garuda repository and AUR [helpers no needed].\n"
EXTRA_PLUGINS_TEXT="<b>Choose which software you want to install (AUR).</b>\n\nRemember that AUR is a user repository and some of listed plugins may not work properly.\nIf something went wrong search for package in AUR repository (https://aur.archlinux.org) and look into comment section.\n"
DAW_TEXT="List of available DAWs in Garuda repository and AUR*\n\n*This is my personal selection so if you need something more you need to search in AUR repository.\n"
REAL_TIME_TEXT="List of available kernels in Garuda repository.\n"
TOOLS_TEXT="List of available tools in Garuda repository.\n"
PANEL_MENUS_TEXT="Pick config file you want to edit.\n"
GROUPS_TEXT=""
RECBOX_ONES_TEXT="This list is what I assume will be worth to install, feel free to uncheck software you don't need.\n"

OK_BUTTON="Apply"
CANCEL_BUTTON="Close"
EXTRA_BUTTON="Previous"

COLUMN_1="Pick"
COLUMN_2="Description"
COLUMN_3="Description"

ICON_PATH="/usr/share/icons/RecBox/light-menu-icon.png"

### Garuda repo
DES_PAALSA="ALSA configuration for PulseAudio."
DES_PAJACK="JACK support for PulseAudio."

DES_A2JMIDID="A daemon for exposing legacy ALSA sequencer applications in JACK MIDI system."
DES_ADLJACK="A standalone synthesizer for ADLMIDI and OPNMIDI on text console."
DES_ADLPLUG="FM synthesizer for ADLMIDI with OPL3 chip emulation."
DES_AEOLUS="Synthesised pip organ emulator."
DES_AJSNAPSHOT="Command line utility to store/restore ALSA nad/or JACK connections to/from an XML file)."
DES_ALIKI="Measure Impulse Responses using a sine sweep and deconvolution."
DES_AMBPLUGINS="A set of LADSPA ambisonics plugins."
DES_AMBDEC="An Ambisonic decoder for first and second order."
DES_AMS="A real-time modular synthesizer and effect processor."
DES_AMSLV2="A port of internal modules found in Alsa Modular Synth."
DES_AMSYNTH="An analog modelling software synthesizer."
DES_ARDOUR="Digital Audio Workstation."
DES_ARTYFX="A plugin bundle of artistic real-time audio effects."
DES_AUBIO="A tool for extracting annotations from audio signal."
DES_AUDACITY="Record and edit audio files."
DES_AVLDRUMSLV2="A simple Drum Sample Player Plugin,dedicated to the AVLinux Drumkits."
DES_BEATSLASHLV2="A set of plugins for live beat repeating beat slicing."
DES_BLOP="Bandlimited LADSPA Oscillaror Plugins."
DES_BLOPLV2="A port of the BLOP LADSPA plugins by Mike Rawes to LV2."
DES_BSEQUENCER="Multi  channel MIDI step sequencer LV2 plugin width a variable matrix."
DES_BSHAPR="Beat/envelope shaper LV2 plugin."
DES_BSLIZR="LV2 audio effect plugin for sequenced slicing of stereo audio input signals."
DES_CADENCE="JACK toolbox for audio production."
DES_CALF="Process and produce sounds using a set of plugins with JACK interface."
DES_CAPS="The LADSPA C* Audio Plugin Suite."
DES_CARLA="Audio Plugin Host."
DES_CHUCK="Concurrent, on-the-fly audio programming language."
DES_CMT="LADSPA plugins for use with software synthesis and recording packages on Linux."
DES_CSOUND="A programming language for sound rendering and signal processing."
DES_CSOUNDQT="Csound frontend with highlighting editor, autocomplete, interactive widgets and integrated help."
DES_DETERIORATELV2="A set of plugins to deteriorate the sound quality of a live input."
DES_DGEDIT="The DrumGizmo drumkit editor."
DES_DIN="A sound synthesizer and musical instrument."
DES_DPFPLUGINS="Collection of DPF-based plugins."
DES_DRAGONFLYREVERB="A free hall-style reverb based on freeverb3 alghorithms."
DES_DRUMGIZMO="Multichannel, multilayered, cross-platform drum plugin and stand-alone application."
DES_DRUMKV1="An old-school drum-kit sampler synthesizer with stereo fx."
DES_DSSI="An API for audio processing plugins & softsynths with UIs."
DES_DSSIVST="DSSI adapter/wrapper for win32 VST plugins."
DES_EBUMETER="Loudness measurement according to EBU-R128."
DES_ECASOUND="Command-line multitrack audio processor."
DES_ELEMENT="A modular LV2/VST3 audio plugin host."
DES_EQ10Q="Audio plugin bundle over the LV2 standard for Linux."
DES_ETEROJLV2="Open Sound Control for LV2."
DES_FABLA="An open-source LV2 drum sampler plugin instrument."
DES_FAUST="A functional programming language for realtime audio signal processing."
DES_FILPLUGINS="LADSPA four-band parametric equaliser plugins."
DES_FLUAJHO="A simple sf2 soundfont host/player."
DES_FLUIDSYNTH="A real-time software Synthesizer based on the SoundFont 2 specifications."
DES_FOMPLV2="An LV2 port of the MCP, VCO, FIL and WAH plugins by Fons Adriaensen."
DES_FOXDOT="Live Coding with Python."
DES_FREEWHEELING="A highly configurable, intuitive, and fluid user interface for instrumentalists to capture audio loops in real-time."
DES_G2REVERB="LADSPA stereo reverb plugin based on greverb."
DES_GEONKICK="A free software percussion synthesizer."
DES_GIADA="A free, minimal, hardcore audio tool for DJs, live performers and electric musicians."
DES_GIGEDIT="Gigasampler instrument editor."
DES_GMSYNTHLV2="General MIDI LV2 Synth."
DES_GUITARIX="Simple mono amplifier simulation."
DES_GXPLUGINSLV2="A set of extra LV2 plugins from the guitarix project."
DES_HELM="A cross-platform, polyphonic synthesizer, available standalone and as an LV2 plugin."
DES_HEXTER="A Yamaha DX7 modeling software synthesizer for the DSSI Soft Synth Interface."
DES_HYDROGEN="An advanced drum machine."
DES_IEMPLUGINSUITE="plug-in suite including Ambisonic plugins up to 7th order."
DES_INFAMOUSPLUGINS="A collection of open-source LV2 plugins."
DES_IRLV2="No-latency/low-latency, realtime, high performance signal convolver for reverb effects."
DES_JAAA="JACK and ALSA Audio Analyser."
DES_JACKSTDIO="Unix pipe audio-data from and to JACK."
DES_JACKCAPTURE="Simple command line tool to record JACK audio output to a file."
DES_JACKDELAY="Measure and round-trip latency of a soundcard."
DES_JACKMETER="A basic console based DPM (Digital Peak Meter) for JACK."
DES_JACKMINIMIX="A simple mixer for the Jack Audio Connection Kit with an OSC based control interface."
DES_JACKTRIP="Tool to manage and tune JACK settings for optimum performance between networked machines."
DES_JALV="A simple but fully featured LV2 host for JACK."
DES_JAPA="A 'perceptual' or 'psychoacoustic' audio spectrum analuser."
DES_JCONVOLVER="A real-time convolution engine."
DES_JNOISEMETER="Measure audio test signal and in particular noise signal."
DES_JSAMPLER="LinuxSampler Java GUI."
DES_LINUXSAMPLER="Professional-grade audio sampler alternative to Gigasampler."
DES_LMMS="Music sequencer and sunthesizer."
DES_LSPPLUGINS="Collection of free plugins compatible with LADSPA, LV2 and LinuxVST."
DES_LUPPP="A music creation tool, intended for live use."
DES_LV2FILE="A simple program which you can use to apply effects to your audio files."
DES_MARSYAS="Music Analysis, Retrieval and Synthesis for Audio Signals."
DES_MCPPLUGINS="A set of LADSPA filters plugins."
DES_MDALV2="A port of the MDA VST plugins to LV2."
DES_MEPHISTOLV2="A just-in-Time FAUST compiler embedded in an LV2 plugin."
DES_METERBRIDGE="Collection of Audio meters for the JACK audio server."
DES_MIDIMATRIXLV2="A 3-in-1 filter plugin with a simple UI enabling you to easily accomplish MIDI channel filtering, multiplication and rerouting."
DES_MIDIMSGLV2="A Collection of basic LV2 plugins to translate midi messages to usable values."
DES_MIXXX="Everything you need to perform live DJ mixes."
DES_MOONYLV2="Easily add real-time programmable logic glue in LV2 plugin graphs."
DES_NINJAS2="A sampler slicer  audio plugin."
DES_NJCONNECT="Curses JACK connection manager."
DES_NOISEREPELLENT="An LV2 plugin for broadband noise reduction."
DES_NONMIXER="Modular Digital Audio Workstations - Mixer."
DES_NONSEQUENCER="Realtime MIDI sequencer for JACK MIDI."
DES_NONSESSIONMANAGER="Audio session manager from the land of Non."
DES_NONTIMELINE="Modular Digital Audio Workstations - Timeline Editor."
DES_OPNPLUG="FM synthesizer for OPNMIDI with OPN2 chip emulation."
DES_OSC2MIDI="A highly flexible and configurable OSC to JACK MIDI (and back) bridge."
DES_OSMID="A lightweight, portable, easy to use tool to convert MIDI to OSC and OSC to MIDI."
DES_PADTHV1="An old-school polyphonic additive synthesizer."
DES_PATCHMATRIX="Connect JACK clients together."
DES_PATRONEO="An easy to use pattern based MIDI sequencer."
DES_PD="The Pure Data real-time music and multimedia environment."
DES_PDLUA="LUA embedding for pd."
DES_POLYPHONE="A sounfont editor for quickly designing musical instruments."
DES_PVOC="LADSPA plugins and a tool for time compression/expansion using phase-vocoding."
DES_QASTOOLS="A collection of desktop applications for the Linux sound system ALSA."
DES_QJACKCTL="A Qt front-end for the JACK low-latency audio server."
DES_QMIDIARP="A MIDI Arpeggiator, Step Sequencer and LFO."
DES_QMIDICTL="MIDI remote control application sending MIDI data over network using UDP/IP multicast."
DES_QMIDINET="A MIDI Network Gateway via UDP/IP Multicast."
DES_QMIDIROUTE="MIDI Router and Processor for ALSA."
DES_QSAMPLER="A Linux Sampler Qu GUI interface."
DES_QSYNTH="Qt GUI for FluidSynth."
DES_QTRACTOR="An Audio/MIDI multi-track sequencer."
DES_QXGEDIT="GUI for editing MIDI System Exclusive files forr XG devices(eg. Yamaha DB50XG)."
DES_REALTIMEPRIVILEGES="Realtime privileges for user."
DES_REVPLUGINS="LADSPA stereo and ambisonic reverb plugin based on zita-rev."
DES_ROSEGARDEN="MIDI and Audio Sequencer and Notation Editor."
DES_SAMPLV1="An old-school polyphonic sampler."
DES_SC3PLUGINS="Extension plugins for the SuperCollider3 audio synthesis server."
DES_SETBFREE="DSP tonewheel organ."
DES_SHERLOCKLV2="An investigative plugin bundle."
DES_SND="An advanced sound editor."
DES_SOLFEGE="Music education and ear training software."
DES_SONICPI="The Live Coding Music Synth for Everyone."
DES_SONICVISUALISER="Viewing and analysing the contents of music audio files."
DES_SORCER="A polyphonic wavetable synth LV2 plugin."
DES_SPECTMORPH="SpectMorph Sound Morphing standalone JACK Client."
DES_SSR="A tool for real-time spatial audio reproduction."
DES_SUPERCOLLIDER="IDE for the SuperCollider audio synthesis language."
DES_SWEEP="Sweep Sound Editor."
DES_SWHPLUGINS="Steve Harris' LADSPA plugins suite."
DES_SYNTHV1="An old-school 4-oscillator polyphonic synthesizer with stereo fx."
DES_TAPPLUGINS="Tom's LADSPA Plugins."
DES_TIDALCYCLES="A domain specific language for live coding pattern."
DES_TIMIDITY="A MIDI to WAVE converer and player."
DES_VAMPAUBIOPLUGINS="Onset detection, pitch tracking and tempo tracking plugins using aubio."
DES_VAMPPLUGINSSDK="The Vamp audio analysis plugin system."
DES_VCOPLUGINS="LADSPA anti-aliased oscillator plugins."
DES_VICO="Minimalistic MIDI sequencer with piano roll for JACK and NSM."
DES_VMLV2="A virtual machine LV2 plugin bundle."
DES_VMPK="Virtual MIDI Piano Keyboard."
DES_WAHPLUGINS="LADSPA Wah filter plugin."
DES_WOLFSHAPER="A waveshaper plugin with a graph editor."
DES_WOLFSPECTRUM="Wolf Spectrum is a spectrogram plugin."
DES_X42PLUGINS="Collection of LV2 plugins."
DES_XMONKLV2="A simple sound generator LV2 plugin to have some fun with."
DES_YASS="Yet Another Scrolling Scope."
DES_YOSHIMI="A Software Synthesizer for Linux."
DES_ZAMPLUGINS="Collection of LADSPA/LV2/VST/JACK audio plugins for high-quality processing."
DES_ZITAAJBRIDGE="JACK client to use additional ALSA devices."
DES_ZITAAT1="An 'autotuner' JACK application."
DES_ZITABLS1="Blumlein Shuffler (binural stereo signal to convential stereo speaker conversion)."
DES_ZITADC1="Dynamic Compressor."
DES_ZITADPL1="A look-ahead digital peak level limiter."
DES_ZITALRX="Command line JACK application providing 2,3 or 4 band, 4th order crossover filters."
DES_ZITAMU1="A simple JACK app used to organise stereo monitoring."
DES_ZITANJBRIDGE="JACK client to transmit audio over a local IP network."
DES_ZITAREV1="A reworked jack version of the reverb originally developed for Aeolus."
DES_ZYNADDSUBFX="Open-source software synthesizer capable of making a countless number of instruments."

### AUR repo
DES_BITROTGIT="Bitrot audio plugins (LV2, VST2 and LADSPA)."
DES_CREOXGIT="A real-time sound processor for electric guitar or any other musical instrument."
DES_DEXED="A software synth closely modelled on the Yamaha DX7."
DES_DIGITSVSTGIT="Digits is an advanced phase distortion synthesizer."
DES_DISTRHOLV2GIT="DISTRHO LV2 Audio Plugins, using the JUCE Toolkit."
DES_DISTRHOVSTGIT="DISTRHO VST audio plugins ports."
DES_DISTRHOEXTRALV2GIT="Extra LV2 ports of JUCE-based audio plugins using the DISTRHO framework."
DES_DRMRGIT="DrMr is an LV2 sampler plugin that can play Hydrogen drumkits. This version enables plugin state saving using strings."
DES_DSSIVST="DSSI adaper/wrapper for win32 VST plugins."
DES_OPENAVFABLA2GIT="Multi-purpose advanced LV2 sampler plugin."
DES_LUFTIKUS="Digital adaptation of an analog EQ with fixed half-octave bands and additional high frequency boost."
DES_TALPLUGINS="Togu Audio Line VST plugins - NoiseMaker, Dub III, Filter, Filter II, Reverb, Reverb II, Reverb III, Vocoder II."
DES_FLUIDPLUGGIT="SoundFonts as LV2 plugins via FluidSynth."
DES_FOOYC20="Faust implementation of a 1969 designed Yamaha combo organ, the YC-20 (VST, LV2 and a standalone)."
DES_GHOSTESS="A simple GTK host for DSSI plugins."
DES_HYBRIDREVERB2GIT="Hybrid impulse convolution reverb, available as LV2 and VST."
DES_INGENGIT="A modular plugin host for JACK and LV2."
DES_INSTALOOPERVST="A simple looper with four integrated FX."
DES_JACKASSGIT="A VST plugin that provides JACK-MIDI support for VST hosts."
DES_JACKMANGIT="Collection of scripts that help managing multiple audio interfaces with Jack."
DES_JACKMANKCMGIT="KCM module for JackManm, a collection of scripts that help managing multiple audio interfaces with JACK."
DES_MUSTANGPLUG="Linux replacement for Fender FUSE software for Fender Mustang guitar amplifier."
DES_OBXD="Virtual Analog Synthesizer VST based on the Oberheim OB-X."
DES_OXEFMSYNTH="An 8-OP FM synthesizer VST plug-in."
DES_RAKARRACK="Versatile guitar multi-effects processor."
DES_RKRLV2GIT="RakarRack effects ported to LV2 plugins (git version)."
DES_RTPVC="Real-time phase vocoder library for synthesis/analysis."
DES_TONELIBGFXBIN="the perfect custom tone with a complete guitar studio in your computer!"
DES_TONELIBJAMBIN="the learning and practice software for guitar players."
DES_TUNEFISH4="An additive wavetable-based synthesizer VST plugin."
DES_VCVRACK="Open-source virtual modular synthesizer."
DES_VCVRACKFUNDAMENTAL="Fundamental VCV modules."
DES_VCVRACKAUDIBLEINSTRUMENTS="Mutable Instruments' VCV modules."
DES_VCVRACKBEFACO="Befaco's VCV modules."
DES_VCVRACKESERIES="E-Series VCV modules."
DES_VCVRACKSONUSMODULARGIT="is a collection of modules for the open source modular synthesizer emulation VCV Rack."
DES_VLEVELGIT="VLevel is a tool to amplify the soft parts of music so you don't have to fiddle with the volume control."
DES_VOCODERJACK="Standalone JACK vocoder."
DES_VOCODERLADSPA="Vocoder LADSPA plugin."
DES_WHYSYNTH="A versatile softsynth plugin for the DSSI Soft Synth Interface."
DES_XSYNTHDSSI="An analog-style (VCOs-VCF-VCA) synth plugin for DSSI."
DES_BITROTGIT="Bitrot audio plugins (LV2, VST2 and LADSPA)."
DES_CREOXGIT="A real-time sound processor for electric guitar or any other musical instrument."
DES_DEXED="A software synth closely modelled on the Yamaha DX7."
DES_DIGITSVSTGIT="Digits is an advanced phase distortion synthesizer."
DES_DISTRHOLV2GIT="DISTRHO LV2 Audio Plugins, using the JUCE Toolkit."
DES_DISTRHOVSTGIT="DISTRHO VST audio plugins ports."
DES_DISTRHOEXTRALV2GIT="Extra LV2 ports of JUCE-based audio plugins using the DISTRHO framework."
DES_DRMRGIT="DrMr is an LV2 sampler plugin that can play Hydrogen drumkits. This version enables plugin state saving using strings."
DES_DSSIVST="DSSI adaper/wrapper for win32 VST plugins."
DES_OPENAVFABLA2GIT="Multi-purpose advanced LV2 sampler plugin."
DES_LUFTIKUS="Digital adaptation of an analog EQ with fixed half-octave bands and additional high frequency boost."
DES_TALPLUGINS="Togu Audio Line VST plugins - NoiseMaker, Dub III, Filter, Filter II, Reverb, Reverb II, Reverb III, Vocoder II."
DES_FLUIDPLUGGIT="SoundFonts as LV2 plugins via FluidSynth."
DES_FOOYC20="Faust implementation of a 1969 designed Yamaha combo organ, the YC-20 (VST, LV2 and a standalone)."
DES_GHOSTESS="A simple GTK host for DSSI plugins."
DES_HYBRIDREVERB2GIT="Hybrid impulse convolution reverb, available as LV2 and VST."
DES_INGENGIT="A modular plugin host for JACK and LV2."
DES_INSTALOOPERVST="A simple looper with four integrated FX."
DES_JACKASSGIT="A VST plugin that provides JACK-MIDI support for VST hosts."
DES_JACKMANGIT="Collection of scripts that help managing multiple audio interfaces with Jack."
DES_JACKMANKCMGIT="KCM module for JackManm, a collection of scripts that help managing multiple audio interfaces with JACK."
DES_MUSTANGPLUG="Linux replacement for Fender FUSE software for Fender Mustang guitar amplifier."
DES_OBXD="Virtual Analog Synthesizer VST based on the Oberheim OB-X."
DES_OXEFMSYNTH="An 8-OP FM synthesizer VST plug-in."
DES_RAKARRACK="Versatile guitar multi-effects processor."
DES_RKRLV2GIT="RakarRack effects ported to LV2 plugins (git version)."
DES_RTPVC="Real-time phase vocoder library for synthesis/analysis."
DES_TONELIBGFXBIN="the perfect custom tone with a complete guitar studio in your computer!"
DES_TONELIBJAMBIN="the learning and practice software for guitar players."
DES_TUNEFISH4="An additive wavetable-based synthesizer VST plugin."
DES_VCVRACK="Open-source virtual modular synthesizer."
DES_VCVRACKFUNDAMENTAL="Fundamental VCV modules."
DES_VCVRACKAUDIBLEINSTRUMENTS="Mutable Instruments' VCV modules."
DES_VCVRACKBEFACO="Befaco's VCV modules."
DES_VCVRACKESERIES="E-Series VCV modules."
DES_VCVRACKSONUSMODULARGIT="is a collection of modules for the open source modular synthesizer emulation VCV Rack."
DES_VLEVELGIT="VLevel is a tool to amplify the soft parts of music so you don't have to fiddle with the volume control."
DES_VOCODERJACK="Standalone JACK vocoder."
DES_VOCODERLADSPA="Vocoder LADSPA plugin."
DES_WHYSYNTH="A versatile softsynth plugin for the DSSI Soft Synth Interface."
DES_XSYNTHDSSI="An analog-style (VCOs-VCF-VCA) synth plugin for DSSI."

main() {

MENU="zenity --list --width=620 --height=311 --hide-header"

	CMD=$($MENU --title="$MAIN_TITLE" --text="$MAIN_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="$COLUMN_2" --window-icon="$ICON_PATH" \
		"Pro Audio Software" "Collection of audio plugins and software from Garuda repository." \
		"Extra Software" "Collection of audio plugins and software from AUR." \
		"Digital Audio Workstations" "Software for recording and editing audio." \
		"Realtime Kernels" "Tweaked kernels for audio recording." \
		"Kernel Tools" "Tools for tweaking RT kernels." \
		"Edit Audio Software" "Open jgmenu config file with Audio Software items." \
		"Edit Daily panel menus" "Open menu with config files to edit." \
		"Edit Studio panel menus" "Open menu with config files to edit." \
		)

	if [ "$CMD" = "Pro Audio Software" ]; then

		exec rb-center.sh pro_audio

	elif [ "$CMD" = "Extra Software" ]; then

		exec rb-center.sh extra_plugins

	elif [ "$CMD" = "Digital Audio Workstations" ]; then

		exec rb-center.sh daw

	elif [ "$CMD" = "Realtime Kernels" ]; then

		exec rb-center.sh real_time

	elif [ "$CMD" = "Kernel Tools" ]; then

		exec rb-center.sh tools

	elif [ "$CMD" = "Edit Audio Software" ]; then

		exo-open ~/.config/jgmenu/append.csv; exec rb-center.sh main

	elif [ "$CMD" = "Edit Daily panel menus" ]; then

		exec rb-center.sh panel_menus_daily

	elif [ "$CMD" = "Edit Studio panel menus" ]; then

		exec rb-center.sh panel_menus_studio

	fi 2>/dev/null
}

pro_audio() {

MENU="zenity --list --width=840 --height=600 --hide-header"

	BOX=$($MENU --title="$PRO_AUDIO_TITLE" --text="$GROUPS_TEXT" --ok-label="$OK_BUTTON" \
	--cancel-label="$CANCEL_BUTTON" --column="$COLUMN_1" --column="$COLUMN_2" --extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
		"DSSI software" "Plugins from dssi-plugins group." \
		"LADSPA software" "Plugins from ladspa-plugins group." \
		"LV2 software" "Plugins from lv2-plugins group." \
		"VST software" "Plugins from vst-plugins group." \
		"Non DAW" "Non DAW Modules." \
		"" "" \
		"Install a few at once" "Opens multiple choice menu." \
		"Selected by RecBox" "Can be considered as Minimal installation." \
		"" "" \
		"a2jmidid" "$DES_A2JMIDID" \
		"adljack" "$DES_ADLJACK" \
		"adlplug" "$DES_ADLPLUG" \
		"aeolus" "$DES_AEOLUS" \
		"aj-snapshot" "$DES_AJSNAPSHOT" \
		"aliki" "$DES_ALIKI" \
		"amb-plugins" "$DES_AMBPLUGINS" \
		"ambdec" "$DES_AMBDEC" \
		"ams" "$DES_AMS" \
		"ams-lv2" "$DES_AMSLV2" \
		"amsynth" "$DES_AMSYNTH" \
		"ardour" "$DES_ARDOUR" \
		"artyfx" "$DES_ARTYFX" \
		"aubio" "$DES_AUBIO" \
		"audacity" "$DES_AUDACITY" \
		"avldrums.lv2" "$DES_AVLDRUMSLV2" \
		"beatslash-lv2" "$DES_BEATSLASHLV2" \
		"blop" "$DES_BLOP" \
		"blop.lv2" "$DES_BLOPLV2" \
		"bsequencer" "$DES_BSEQUENCER" \
		"bshapr" "$DES_BSHAPR" \
		"bslizr" "$DES_BSLIZR" \
		"cadence" "$DES_CADENCE" \
		"calf" "$DES_CALF" \
		"caps" "$DES_CAPS" \
		"carla" "$DES_CARLA" \
		"chuck" "$DES_CHUCK" \
		"cmt" "$DES_CMT" \
		"csound" "$DES_CSOUND" \
		"csoundqt" "$DES_CSOUNDQT" \
		"deteriorate-lv2" "$DES_DETERIORATELV2" \
		"dgedit" "$DES_DGEDIT" \
		"din" "$DES_DIN" \
		"dpf-plugins" "$DES_DPFPLUGINS" \
		"dragonfly-reverb" "$DES_DRAGONFLYREVERB" \
		"drumgizmo" "$DES_DRUMGIZMO" \
		"drumkv1" "$DES_DRUMKV1" \
		"dssi" "$DES_DSSI" \
		"ebumeter" "$DES_EBUMETER" \
		"ecasound" "$DES_ECASOUND" \
		"element" "$DES_ELEMENT" \
		"eq10q" "$DES_EQ10Q" \
		"eteroj.lv2" "$DES_ETEROJLV2" \
		"fabla" "$DES_FABLA" \
		"faust" "$DES_FAUST" \
		"fil-plugins" "$DES_FILPLUGINS" \
		"fluajho" "$DES_FLUAJHO" \
		"fluidsynth" "$DES_FLUIDSYNTH" \
		"fomp.lv2" "$DES_FOMPLV2" \
		"foxdot" "$DES_FOXDOT" \
		"freewheeling" "$DES_FREEWHEELING" \
		"g2reverb" "$DES_G2REVERB" \
		"geonkick" "$DES_GEONKICK" \
		"giada" "$DES_GIADA" \
		"gigedit" "$DES_GIGEDIT" \
		"gmsynth.lv2" "$DES_GMSYNTHLV2" \
		"guitarix" "$DES_GUITARIX" \
		"gxplugins.lv2" "$DES_GXPLUGINSLV2" \
		"helm-synth" "$DES_HELM" \
		"hexter" "$DES_HEXTER" \
		"hydrogen" "$DES_HYDROGEN" \
		"iempluginsuite" "$DES_IEMPLUGINSUITE"\
		"infamousplugins" "$DES_INFAMOUSPLUGINS" \
		"ir.lv2" "$DES_IRLV2" \
		"jaaa" "$DES_JAAA" \
		"jack-stdio" "$DES_JACKSTDIO" \
		"jack_capture" "$DES_JACKCAPTURE" \
		"jack_delay" "$DES_JACKDELAY" \
		"jackmeter" "$DES_JACKMETER" \
		"jackminimix" "$DES_JACKMINIMIX" \
		"jacktrip" "$DES_JACKTRIP" \
		"jalv" "$DES_JALV" \
		"japa" "$DES_JAPA" \
		"jconvolver" "$DES_JCONVOLVER" \
		"jnoisemeter" "$DES_JNOISEMETER" \
		"jsampler" "$DES_JSAMPLER" \
		"linuxsampler" "$DES_LINUXSAMPLER" \
		"lmms" "$DES_LMMS" \
		"lsp-plugins" "$DES_LSPPLUGINS" \
		"luppp" "$DES_LUPPP" \
		"lv2file" "$DES_LV2FILE" \
		"marsyas" "$DES_MARSYAS" \
		"mcp-plugins" "$DES_MCPPLUGINS" \
		"mda.lv2" "$DES_MDALV2" \
		"mephisto.lv2" "$DES_MEPHISTOLV2" \
		"meterbridge" "$DES_METERBRIDGE" \
		"midi_matrix.lv2" "$DES_MIDIMATRIXLV2" \
		"midimsg-lv2" "$DES_MIDIMSGLV2" \
		"mixxx" "$DES_MIXXX" \
		"moony.lv2" "$DES_MOONYLV2" \
		"ninjas2" "$DES_NINJAS2" \
		"njconnect" "$DES_NJCONNECT" \
		"noise-repellent" "$DES_NOISEREPELLENT" \
		"non-mixer" "$DES_NONMIXER" \
		"non-sequencer" "$DES_NONSEQUENCER" \
		"non-session-manager" "$DES_NONSESSIONMANAGER" \
		"non-timeline" "$DES_NONTIMELINE" \
		"opnplug" "$DES_OPNPLUG" \
		"osc2midi" "$DES_OSC2MIDI" \
		"osmid" "$DES_OSMID" \
		"padthv1" "$DES_PADTHV1" \
		"patchmatrix" "$DES_PATCHMATRIX" \
		"patroneo" "$DES_PATRONEO" \
		"pd" "$DES_PD" \
		"pd-lua" "$DES_PDLUA" \
		"polyphone" "$DES_POLYPHONE" \
		"pvoc" "$DES_PVOC" \
		"qastools" "$DES_QASTOOLS" \
		"qjackctl" "$DES_QJACKCTL" \
		"qmidiarp" "$DES_QMIDIARP" \
		"qmidictl" "$DES_QMIDICTL" \
		"qmidinet" "$DES_QMIDINET" \
		"qmidiroute" "$DES_QMIDIROUTE" \
		"qsampler" "$DES_QSAMPLER" \
		"qsynth" "$DES_QSYNTH" \
		"qtractor" "$DES_QTRACTOR" \
		"qxgedit" "$DES_QXGEDIT" \
		"realtime-privileges" "$DES_REALTIMEPRIVILEGES" \
		"rev-plugins" "$DES_REVPLUGINS" \
		"rosegarden" "$DES_ROSEGARDEN" \
		"samplv1" "$DES_SAMPLV1" \
		"sc3-plugins" "$DES_SC3PLUGINS" \
		"setbfree" "$DES_SETBFREE" \
		"sherlock.lv2" "$DES_SHERLOCKLV2" \
		"snd" "$DES_SND" \
		"solfege" "$DES_SOLFEGE" \
		"sonic-pi" "$DES_SONICPI" \
		"sonic-visualiser" "$DES_SONICVISUALISER" \
		"sorcer" "$DES_SORCER" \
		"spectmorph" "$DES_SPECTMORPH" \
		"ssr" "$DES_SSR" \
		"supercollider" "$DES_SUPERCOLLIDER" \
		"sweep" "$DES_SWEEP" \
		"swh-plugins" "$DES_SWHPLUGINS" \
		"synthv1" "$DES_SYNTHV1" \
		"tap-plugins" "$DES_TAPPLUGINS" \
		"tidalcycles" "$DES_TIDALCYCLES" \
		"timidity++" "$DES_TIMIDITY" \
		"vamp-aubio-plugins" "$DES_VAMPAUBIOPLUGINS" \
		"vamp-plugin-sdk" "$DES_VAMPPLUGINSSDK" \
		"vco-plugins" "$DES_VCOPLUGINS" \
		"vico" "$DES_VICO" \
		"vm.lv2" "$DES_VMLV2" \
		"vmpk" "$DES_VMPK" \
		"wah-plugins" "$DES_WAHPLUGINS" \
		"wolf-shaper" "$DES_WOLFSHAPER" \
		"wolf-spectrum" "$DES_WOLFSPECTRUM" \
		"x42-plugins" "$DES_X42PLUGINS" \
		"xmonk.lv2" "$DES_XMONKLV2" \
		"yass" "$DES_YASS" \
		"yoshimi" "$DES_YOSHIMI" \
		"zam-plugins" "$DES_ZAMPLUGINS" \
		"zita-ajbridge" "$DES_ZITAAJBRIDGE" \
		"zita-at1" "$DES_ZITAAT1" \
		"zita-bls1" "$DES_ZITABLS1" \
		"zita-dc1" "$DES_ZITADC1" \
		"zita-dpl1" "$DES_ZITADPL1" \
		"zita-lrx" "$DES_ZITALRX" \
		"zita-mu1" "$DES_ZITAMU1" \
		"zita-njbridge" "$DES_ZITANJBRIDGE" \
		"zita-rev1" "$DES_ZITAREV1" \
		"zynaddsubfx" "$DES_ZYNADDSUBFX" \
		)

	if [ "$BOX" = "Install a few at once" ]; then

		exec rb-center.sh pro_audio_multiple

	elif [ "$BOX" = "a2jmidid" ]; then

		pamac-installer a2jmidid; exec rb-center.sh pro_audio

	elif [ "$BOX" = "adljack" ]; then
	
		pamac-installer adljack; exec rb-center.sh pro_audio

	elif [ "$BOX" = "adlplug" ]; then

		pamac-installer adlplug; exec rb-center.sh pro_audio

	elif [ "$BOX" = "aeolus" ]; then

		pamac-installer aeolus; exec rb-center.sh pro_audio

	elif [ "$BOX" = "aj-snapshot" ]; then

		pamac-installer aj-snapshot; exec rb-center.sh pro_audio

	elif [ "$BOX" = "aliki" ]; then

		pamac-installer aliki; exec rb-center.sh pro_audio

	elif [ "$BOX" = "amb-plugins" ]; then

		pamac-installer amb-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "ambdec" ]; then

		pamac-installer ambdec; exec rb-center.sh pro_audio

	elif [ "$BOX" = "ams" ]; then

		pamac-installer ams; exec rb-center.sh pro_audio

	elif [ "$BOX" = "ams-lv2" ]; then

		pamac-installer ams-lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "amsynth" ]; then

		pamac-installer amsynth; exec rb-center.sh pro_audio

	elif [ "$BOX" = "ardour" ]; then
	
		pamac-installer ardour; exec rb-center.sh pro_audio

	elif [ "$BOX" = "artyfx" ]; then

		pamac-installer artyfx; exec rb-center.sh pro_audio

	elif [ "$BOX" = "aubio" ]; then

		pamac-installer aubio; exec rb-center.sh pro_audio

	elif [ "$BOX" = "audacity" ]; then
	
		pamac-installer audacity; exec rb-center.sh pro_audio

	elif [ "$BOX" = "avldrums.lv2" ]; then

		pamac-installer avldrums.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "beatslash-lv2" ]; then

		pamac-installer beatslash-lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "blop" ]; then

		pamac-installer blop; exec rb-center.sh pro_audio

	elif [ "$BOX" = "blop.lv2" ]; then

		pamac-installer blop.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "bsequencer" ]; then
	
		pamac-installer bsequencer; exec rb-center.sh pro_audio

	elif [ "$BOX" = "bshapr" ]; then
	
		pamac-installer bshapr; exec rb-center.sh pro_audio

	elif [ "$BOX" = "bslizr" ]; then

		pamac-installer bslizr; exec rb-center.sh pro_audio

	elif [ "$BOX" = "cadence" ]; then

		pamac-installer cadence; exec rb-center.sh pro_audio

	elif [ "$BOX" = "calf" ]; then

		pamac-installer calf; exec rb-center.sh pro_audio

	elif [ "$BOX" = "caps" ]; then

		pamac-installer caps; exec rb-center.sh pro_audio

	elif [ "$BOX" = "carla" ]; then

		pamac-installer carla; exec rb-center.sh pro_audio

	elif [ "$BOX" = "chuck" ]; then

		pamac-installer chuck; exec rb-center.sh pro_audio

	elif [ "$BOX" = "cmt" ]; then

		pamac-installer cmt; exec rb-center.sh pro_audio

	elif [ "$BOX" = "csound" ]; then

		pamac-installer csound; exec rb-center.sh pro_audio

	elif [ "$BOX" = "csoundqt" ]; then

		pamac-installer csoundqt; exec rb-center.sh pro_audio

	elif [ "$BOX" = "deteriorate-lv2" ]; then

		pamac-installer deteriorate-lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "dgedit" ]; then

		pamac-installer dgedit; exec rb-center.sh pro_audio

	elif [ "$BOX" = "din" ]; then

		pamac-installer din; exec rb-center.sh pro_audio

	elif [ "$BOX" = "dpf-plugins" ]; then

		pamac-installer dpf-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "dragonfly-reverb" ]; then

		pamac-installer dragonfly-reverb; exec rb-center.sh pro_audio

	elif [ "$BOX" = "drumgizmo" ]; then

		pamac-installer drumgizmo; exec rb-center.sh pro_audio

	elif [ "$BOX" = "drumkv1" ]; then

		pamac-installer drumkv1; exec rb-center.sh pro_audio

	elif [ "$BOX" = "dssi" ]; then

		pamac-installer dssi; exec rb-center.sh pro_audio

	elif [ "$BOX" = "ebumeter" ]; then

		pamac-installer ebumeter; exec rb-center.sh pro_audio

	elif [ "$BOX" = "ecasound" ]; then

		pamac-installer ecasound; exec rb-center.sh pro_audio

	elif [ "$BOX" = "element" ]; then

		pamac-installer element; exec rb-center.sh pro_audio

	elif [ "$BOX" = "eq10q" ]; then

		pamac-installer eq10q; exec rb-center.sh pro_audio

	elif [ "$BOX" = "eteroj.lv2" ]; then

		pamac-installer eteroj.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "fabla" ]; then

		pamac-installer fabla; exec rb-center.sh pro_audio

	elif [ "$BOX" = "faust" ]; then

		pamac-installer faust; exec rb-center.sh pro_audio

	elif [ "$BOX" = "fil-plugins" ]; then

		pamac-installer fil-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "fluajho" ]; then

		pamac-installer fluajho; exec rb-center.sh pro_audio

	elif [ "$BOX" = "fluidsynth" ]; then

		pamac-installer fluidsynth; exec rb-center.sh pro_audio

	elif [ "$BOX" = "fomp.lv2" ]; then

		pamac-installer fomp.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "foxdot" ]; then
	
		pamac-installer foxdot; exec rb-center.sh pro_audio

	elif [ "$BOX" = "freewheeling" ]; then

		pamac-installer freewheeling; exec rb-center.sh pro_audio

	elif [ "$BOX" = "g2reverb" ]; then

		pamac-installer g2reverb; exec rb-center.sh pro_audio

	elif [ "$BOX" = "geonkick" ]; then

		pamac-installer geonkick; exec rb-center.sh pro_audio

	elif [ "$BOX" = "giada" ]; then

		pamac-installer giada; exec rb-center.sh pro_audio

	elif [ "$BOX" = "gigedit" ]; then

		pamac-installer gigedit; exec rb-center.sh pro_audio

	elif [ "$BOX" = "gmsynth.lv2" ]; then

		pamac-installer gmsynth.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "guitarix" ]; then

		pamac-installer guitarix; exec rb-center.sh pro_audio

	elif [ "$BOX" = "gxplugins.lv2" ]; then

		pamac-installer gxplugins.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "helm-synth" ]; then

		pamac-installer helm-synth; exec rb-center.sh pro_audio

	elif [ "$BOX" = "hexter" ]; then

		pamac-installer hexter; exec rb-center.sh pro_audio

	elif [ "$BOX" = "hydrogen" ]; then

		pamac-installer hydrogen; exec rb-center.sh pro_audio

	elif [ "$BOX" = "iempluginsuite" ]; then

		pamac-installer iempluginsuite; exec rb-center.sh pro_audio

	elif [ "$BOX" = "infamousplugins" ]; then

		pamac-installer infamousplugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "ir.lv2" ]; then

		pamac-installer ir.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "jaaa" ]; then

		pamac-installer jaaa; exec rb-center.sh pro_audio

	elif [ "$BOX" = "jack-stdio" ]; then
	
		pamac-installer jack-stdio; exec rb-center.sh pro_audio

	elif [ "$BOX" = "jack_capture" ]; then

		pamac-installer jack_capture; exec rb-center.sh pro_audio

	elif [ "$BOX" = "jack_delay" ]; then
	
		pamac-installer jack_delay; exec rb-center.sh pro_audio

	elif [ "$BOX" = "jackmeter" ]; then
	
		pamac-installer jackmeter; exec rb-center.sh pro_audio

	elif [ "$BOX" = "jackminimix" ]; then
	
		pamac-installer jackminimix; exec rb-center.sh pro_audio

	elif [ "$BOX" = "jacktrip" ]; then

		pamac-installer jacktrip; exec rb-center.sh pro_audio

	elif [ "$BOX" = "jalv" ]; then

		pamac-installer jalv; exec rb-center.sh pro_audio

	elif [ "$BOX" = "japa" ]; then

		pamac-installer japa; exec rb-center.sh pro_audio

	elif [ "$BOX" = "jconvolver" ]; then

		pamac-installer jconvolver; exec rb-center.sh pro_audio

	elif [ "$BOX" = "jnoisemeter" ]; then

		pamac-installer jnoisemeter; exec rb-center.sh pro_audio

	elif [ "$BOX" = "jsampler" ]; then

		pamac-installer jsampler; exec rb-center.sh pro_audio

	elif [ "$BOX" = "linuxsampler" ]; then

		pamac-installer linuxsampler; exec rb-center.sh pro_audio

	elif [ "$BOX" = "lmms" ]; then
	
		pamac-installer lmms; exec rb-center.sh pro_audio

	elif [ "$BOX" = "lsp-plugins" ]; then

		pamac-installer lsp-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "luppp" ]; then

		pamac-installer luppp; exec rb-center.sh pro_audio

	elif [ "$BOX" = "lv2file" ]; then

		pamac-installer lv2file; exec rb-center.sh pro_audio

	elif [ "$BOX" = "marsyas" ]; then

		pamac-installer marsyas; exec rb-center.sh pro_audio

	elif [ "$BOX" = "mcp-plugins" ]; then

		pamac-installer mcp-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "mda.lv2" ]; then

		pamac-installer mda.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "mephisto.lv2" ]; then
	
		pamac-installer mephisto.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "meterbridge" ]; then

		pamac-installer meterbridge; exec rb-center.sh pro_audio

	elif [ "$BOX" = "midi_matrix.lv2" ]; then

		pamac-installer midi_matrix.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "midimsg-lv2" ]; then

		pamac-installer midimsg-lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "mixxx" ]; then

		pamac-installer mixxx; exec rb-center.sh pro_audio

	elif [ "$BOX" = "moony.lv2" ]; then

		pamac-installer moony.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "ninjas2" ]; then
	
		pamac-installer ninjas2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "njconnect" ]; then

		pamac-installer njconnect; exec rb-center.sh pro_audio

	elif [ "$BOX" = "noise-repellent" ]; then

		pamac-installer noise-repellent; exec rb-center.sh pro_audio

	elif [ "$BOX" = "non-mixer" ]; then

		pamac-installer non-mixer; exec rb-center.sh pro_audio

	elif [ "$BOX" = "non-sequencer" ]; then

		pamac-installer non-sequencer; exec rb-center.sh pro_audio

	elif [ "$BOX" = "non-session-manager" ]; then

		pamac-installer non-session-manager; exec rb-center.sh pro_audio

	elif [ "$BOX" = "non-timeline" ]; then

		pamac-installer non-timeline; exec rb-center.sh pro_audio

	elif [ "$BOX" = "opnplug" ]; then

		pamac-installer opnplug; exec rb-center.sh pro_audio

	elif [ "$BOX" = "osc2midi" ]; then

		pamac-installer osc2midi; exec rb-center.sh pro_audio

	elif [ "$BOX" = "osmid" ]; then

		pamac-installer osmid; exec rb-center.sh pro_audio

	elif [ "$BOX" = "padthv1" ]; then

		pamac-installer padthv1; exec rb-center.sh pro_audio

	elif [ "$BOX" = "patchmatrix" ]; then

		pamac-installer patchmatrix; exec rb-center.sh pro_audio

	elif [ "$BOX" = "patroneo" ]; then

		pamac-installer patroneo; exec rb-center.sh pro_audio

	elif [ "$BOX" = "pd" ]; then

		pamac-installer pd; exec rb-center.sh pro_audio

	elif [ "$BOX" = "pd-lua" ]; then

		pamac-installer pd-lua; exec rb-center.sh pro_audio

	elif [ "$BOX" = "polyphone" ]; then

		pamac-installer polyphone; exec rb-center.sh pro_audio

	elif [ "$BOX" = "pvoc" ]; then

		pamac-installer pvoc; exec rb-center.sh pro_audio

	elif [ "$BOX" = "qastools" ]; then

		pamac-installer qastools; exec rb-center.sh pro_audio

	elif [ "$BOX" = "qjackctl" ]; then

		pamac-installer qjackctl; exec rb-center.sh pro_audio

	elif [ "$BOX" = "qmidiarp" ]; then

		pamac-installer qmidiarp; exec rb-center.sh pro_audio

	elif [ "$BOX" = "qmidictl" ]; then

		pamac-installer qmidictl; exec rb-center.sh pro_audio

	elif [ "$BOX" = "qmidinet" ]; then

		pamac-installer qmidinet; exec rb-center.sh pro_audio

	elif [ "$BOX" = "qmidiroute" ]; then

		pamac-installer qmidiroute; exec rb-center.sh pro_audio

	elif [ "$BOX" = "qsampler" ]; then

		pamac-installer qsampler; exec rb-center.sh pro_audio

	elif [ "$BOX" = "qsynth" ]; then

		pamac-installer qsynth; exec rb-center.sh pro_audio

	elif [ "$BOX" = "qtractor" ]; then

		pamac-installer qtractor; exec rb-center.sh pro_audio

	elif [ "$BOX" = "qxgedit" ]; then

		pamac-installer qxgedit; exec rb-center.sh pro_audio

	elif [ "$BOX" = "realtime-privileges" ]; then

		pamac-installer realtime-privileges; exec rb-center.sh pro_audio

	elif [ "$BOX" = "rev-plugins" ]; then

		pamac-installer rev-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "rosegarden" ]; then

		pamac-installer rosegarden; exec rb-center.sh pro_audio

	elif [ "$BOX" = "samplv1" ]; then

		pamac-installer samplv1; exec rb-center.sh pro_audio

	elif [ "$BOX" = "sc3-plugins" ]; then

		pamac-installer sc3-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "setbfree" ]; then

		pamac-installer setbfree; exec rb-center.sh pro_audio

	elif [ "$BOX" = "sherlock.lv2" ]; then

		pamac-installer sherlock.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "snd" ]; then

		pamac-installer snd; exec rb-center.sh pro_audio

	elif [ "$BOX" = "solfege" ]; then

		pamac-installer solfege; exec rb-center.sh pro_audio

	elif [ "$BOX" = "sonic-pi" ]; then

		pamac-installer sonic-pi; exec rb-center.sh pro_audio

	elif [ "$BOX" = "sonic-visualiser" ]; then

		pamac-installer sonic-visualiser; exec rb-center.sh pro_audio

	elif [ "$BOX" = "sorcer" ]; then

		pamac-installer sorcer; exec rb-center.sh pro_audio

	elif [ "$BOX" = "spectmorph" ]; then

		pamac-installer spectmorph; exec rb-center.sh pro_audio

	elif [ "$BOX" = "ssr" ]; then

		pamac-installer ssr; exec rb-center.sh pro_audio

	elif [ "$BOX" = "supercollider" ]; then

		pamac-installer supercollider; exec rb-center.sh pro_audio

	elif [ "$BOX" = "sweep" ]; then

		pamac-installer sweep; exec rb-center.sh pro_audio

	elif [ "$BOX" = "swh-plugins" ]; then

		pamac-installer swh-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "synthv1" ]; then

		pamac-installer synthv1; exec rb-center.sh pro_audio

	elif [ "$BOX" = "tap-plugins" ]; then

		pamac-installer tap-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "tidalcycles" ]; then
	
		pamac-installer tidalcycles; exec rb-center.sh pro_audio

	elif [ "$BOX" = "timidity++" ]; then

		pamac-installer timidity++; exec rb-center.sh pro_audio

	elif [ "$BOX" = "vamp-aubio-plugins" ]; then

		pamac-installer vamp-aubio-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "vamp-plugin-sdk" ]; then

		pamac-installer vamp-plugin-sdk; exec rb-center.sh pro_audio

	elif [ "$BOX" = "vco-plugins" ]; then

		pamac-installer vco-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "vico" ]; then
	
		pamac-installer vico; exec rb-center.sh pro_audio

	elif [ "$BOX" = "vm.lv2" ]; then

		pamac-installer vm.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "vmpk" ]; then

		pamac-installer vmpk; exec rb-center.sh pro_audio

	elif [ "$BOX" = "wah-plugins" ]; then

		pamac-installer wah-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "wolf-shaper" ]; then

		pamac-installer wolf-shaper; exec rb-center.sh pro_audio

	elif [ "$BOX" = "wolf-spectrum" ]; then

		pamac-installer wolf-spectrum; exec rb-center.sh pro_audio

	elif [ "$BOX" = "x42-plugins" ]; then

		pamac-installer x42-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "xmonk.lv2" ]; then
	
		pamac-installer xmonk.lv2; exec rb-center.sh pro_audio

	elif [ "$BOX" = "yass" ]; then

		pamac-installer yass; exec rb-center.sh pro_audio

	elif [ "$BOX" = "yoshimi" ]; then

		pamac-installer yoshimi; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zam-plugins" ]; then

		pamac-installer zam-plugins; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zita-ajbridge" ]; then

		pamac-installer zita-ajbridge; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zita-at1" ]; then

		pamac-installer zita-at1; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zita-bls1" ]; then

		pamac-installer zita-bls1; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zita-dc1" ]; then

		pamac-installer zita-dc1; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zita-dpl1" ]; then

		pamac-installer zita-dpl1; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zita-dc1" ]; then
	
		pamac-installer zita-dc1; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zita-dpl1" ]; then
	
		pamac-installer zita-dc1; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zita-lrx" ]; then

		pamac-installer zita-lrx; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zita-mu1" ]; then

		pamac-installer zita-mu1; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zita-njbridge" ]; then

		pamac-installer zita-njbridge; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zita-rev1" ]; then

		pamac-installer zita-rev1; exec rb-center.sh pro_audio

	elif [ "$BOX" = "zynaddsubfx" ]; then

		pamac-installer zynaddsubfx; exec rb-center.sh pro_audio

	elif [ "$BOX" = "DSSI software" ]; then

		exec rb-center.sh dssi_group

	elif [ "$BOX" = "LADSPA software" ]; then

		exec rb-center.sh ladspa_group

	elif [ "$BOX" = "LV2 software" ]; then

		exec rb-center.sh lv2_group

	elif [ "$BOX" = "VST software" ]; then

		exec rb-center.sh vst_group

	elif [ "$BOX" = "Non DAW" ]; then

		exec rb-center.sh non_daw

	elif [ "$BOX" = "Selected by RecBox" ]; then

		exec rb-center.sh recbox_ones

	elif [ "$BOX" = $EXTRA_BUTTON ]; then

		exec rb-center.sh main

	fi

}

pro_audio_multiple() {

MENU="zenity --list --checklist --width=840 --height=600 --hide-header"

	BOX=$($MENU --title="$PRO_AUDIO_MULTI_TITLE" --text="$GROUPS_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="$COLUMN_2" --column="$COLUMN_3" --extra-button="$EXTRA_BUTTON" --multiple --separator=" " --window-icon="$ICON_PATH" \
	FALSE "a2jmidid" "$DES_A2JMIDID" \
	FALSE "adljack" "$DES_ADLJACK" \
	FALSE "adlplug" "$DES_ADLPLUG" \
	FALSE "aeolus" "$DES_AEOLUS" \
	FALSE "aj-snapshot" "$DES_AJSNAPSHOT" \
	FALSE "aliki" "$DES_ALIKI" \
	FALSE "amb-plugins" "$DES_AMBPLUGINS" \
	FALSE "ambdec" "$DES_AMBDEC" \
	FALSE "ams" "$DES_AMS" \
	FALSE "ams-lv2" "$DES_AMSLV2" \
	FALSE "amsynth" "$DES_AMSYNTH" \
	FALSE "ardour" "$DES_ARDOUR" \
	FALSE "artyfx" "$DES_ARTYFX" \
	FALSE "aubio" "$DES_AUBIO" \
	FALSE "audacity" "$DES_AUDACITY" \
	FALSE "avldrums.lv2" "$DES_AVLDRUMSLV2" \
	FALSE "beatslash-lv2" "$DES_BEATSLASHLV2" \
	FALSE "blop" "$DES_BLOP" \
	FALSE "blop.lv2" "$DES_BLOPLV2" \
	FALSE "bsequencer" "$DES_BSEQUENCER" \
	FALSE "bshapr" "$DES_BSHAPR" \
	FALSE "bslizr" "$DES_BSLIZR" \
	FALSE "cadence" "$DES_CADENCE" \
	FALSE "calf" "$DES_CALF" \
	FALSE "caps" "$DES_CAPS" \
	FALSE "carla" "$DES_CARLA" \
	FALSE "chuck" "$DES_CHUCK" \
	FALSE "cmt" "$DES_CMT" \
	FALSE "csound" "$DES_CSOUND" \
	FALSE "csoundqt" "$DES_CSOUNDQT" \
	FALSE "deteriorate-lv2" "$DES_DETERIORATELV2" \
	FALSE "dgedit" "$DES_DGEDIT" \
	FALSE "din" "$DES_DIN" \
	FALSE "dpf-plugins" "$DES_DPFPLUGINS" \
	FALSE "dragonfly-reverb" "$DES_DRAGONFLYREVERB" \
	FALSE "drumgizmo" "$DES_DRUMGIZMO" \
	FALSE "drumkv1" "$DES_DRUMKV1" \
	FALSE "dssi" "$DES_DSSI" \
	FALSE "ebumeter" "$DES_EBUMETER" \
	FALSE "ecasound" "$DES_ECASOUND" \
	FALSE "eq10q" "$DES_EQ10Q" \
	FALSE "eteroj.lv2" "$DES_ETEROJLV2" \
	FALSE "fabla" "$DES_FABLA" \
	FALSE "faust" "$DES_FAUST" \
	FALSE "fil-plugins" "$DES_FILPLUGINS" \
	FALSE "fluajho" "$DES_FLUAJHO" \
	FALSE "fluidsynth" "$DES_FLUIDSYNTH" \
	FALSE "fomp.lv2" "$DES_FOMPLV2" \
	FALSE "foxdot" "$DES_FOXDOT" \
	FALSE "freewheeling" "$DES_FREEWHEELING" \
	FALSE "g2reverb" "$DES_G2REVERB" \
	FALSE "geonkick" "$DES_GEONKICK" \
	FALSE "giada" "$DES_GIADA" \
	FALSE "gigedit" "$DES_GIGEDIT" \
	FALSE "gmsynth.lv2" "$DES_GMSYNTHLV2" \
	FALSE "guitarix" "$DES_GUITARIX" \
	FALSE "gxplugins.lv2" "$DES_GXPLUGINSLV2" \
	FALSE "helm-synth" "$DES_HELM" \
	FALSE "hexter" "$DES_HEXTER" \
	FALSE "hydrogen" "$DES_HYDROGEN" \
	FALSE "iempluginsuite" "$DES_IEMPLUGINSUITE" \
	FALSE "infamousplugins" "$DES_INFAMOUSPLUGINS" \
	FALSE "ir.lv2" "$DES_IRLV2" \
	FALSE "jaaa" "$DES_JAAA" \
	FALSE "jack-stdio" "$DES_JACKSTDIO" \
	FALSE "jack_capture" "$DES_JACKCAPTURE" \
	FALSE "jack_delay" "$DES_JACKDELAY" \
	FALSE "jackmeter" "$DES_JACKMETER" \
	FALSE "jackminimix" "$DES_JACKMINIMIX" \
	FALSE "jacktrip" "$DES_JACKTRIP" \
	FALSE "jalv" "$DES_JALV" \
	FALSE "japa" "$DES_JAPA" \
	FALSE "jconvolver" "$DES_JCONVOLVER" \
	FALSE "jnoisemeter" "$DES_JNOISEMETER" \
	FALSE "jsampler" "$DES_JSAMPLER" \
	FALSE "linuxsampler" "$DES_LINUXSAMPLER" \
	FALSE "lmms" "$DES_LMMS" \
	FALSE "lsp-plugins" "$DES_LSPPLUGINS" \
	FALSE "luppp" "$DES_LUPPP" \
	FALSE "lv2file" "$DES_LV2FILE" \
	FALSE "marsyas" "$DES_MARSYAS" \
	FALSE "mcp-plugins" "$DES_MCPPLUGINS" \
	FALSE "mda.lv2" "$DES_MDALV2" \
	FALSE "mephisto.lv2" "$DES_MEPHISTOLV2" \
	FALSE "meterbridge" "$DES_METERBRIDGE" \
	FALSE "midi_matrix.lv2" "$DES_MIDIMATRIXLV2" \
	FALSE "midimsg-lv2" "$DES_MIDIMSGLV2" \
	FALSE "mixxx" "$DES_MIXXX" \
	FALSE "moony.lv2" "$DES_MOONYLV2" \
	FALSE "ninjas2" "$DES_NINJAS2" \
	FALSE "njconnect" "$DES_NJCONNECT" \
	FALSE "noise-repellent" "$DES_NOISEREPELLENT" \
	FALSE "non-mixer" "$DES_NONMIXER" \
	FALSE "non-sequencer" "$DES_NONSEQUENCER" \
	FALSE "non-session-manager" "$DES_NONSESSIONMANAGER" \
	FALSE "non-timeline" "$DES_NONTIMELINE" \
	FALSE "opnplug" "$DES_OPNPLUG" \
	FALSE "osc2midi" "$DES_OSC2MIDI" \
	FALSE "osmid" "$DES_OSMID" \
	FALSE "padthv1" "$DES_PADTHV1" \
	FALSE "patchmatrix" "$DES_PATCHMATRIX" \
	FALSE "patroneo" "$DES_PATRONEO" \
	FALSE "pd" "$DES_PD" \
	FALSE "pd-lua" "$DES_PDLUA" \
	FALSE "polyphone" "$DES_POLYPHONE" \
	FALSE "pvoc" "$DES_PVOC" \
	FALSE "qastools" "$DES_QASTOOLS" \
	FALSE "qjackctl" "$DES_QJACKCTL" \
	FALSE "qmidiarp" "$DES_QMIDIARP" \
	FALSE "qmidictl" "$DES_QMIDICTL" \
	FALSE "qmidinet" "$DES_QMIDINET" \
	FALSE "qmidiroute" "$DES_QMIDIROUTE" \
	FALSE "qsampler" "$DES_QSAMPLER" \
	FALSE "qsynth" "$DES_QSYNTH" \
	FALSE "qtractor" "$DES_QTRACTOR" \
	FALSE "qxgedit" "$DES_QXGEDIT" \
	FALSE "realtime-privileges" "$DES_REALTIMEPRIVILEGES" \
	FALSE "rev-plugins" "$DES_REVPLUGINS" \
	FALSE "rosegarden" "$DES_ROSEGARDEN" \
	FALSE "samplv1" "$DES_SAMPLV1" \
	FALSE "sc3-plugins" "$DES_SC3PLUGINS" \
	FALSE "setbfree" "$DES_SETBFREE" \
	FALSE "sherlock.lv2" "$DES_SHERLOCKLV2" \
	FALSE "snd" "$DES_SND" \
	FALSE "solfege" "$DES_SOLFEGE" \
	FALSE "sonic-pi" "$DES_SONICPI" \
	FALSE "sonic-visualiser" "$DES_SONICVISUALISER" \
	FALSE "sorcer" "$DES_SORCER" \
	FALSE "spectmorph" "$DES_SPECTMORPH" \
	FALSE "ssr" "$DES_SSR" \
	FALSE "supercollider" "$DES_SUPERCOLLIDER" \
	FALSE "sweep" "$DES_SWEEP" \
	FALSE "swh-plugins" "$DES_SWHPLUGINS" \
	FALSE "synthv1" "$DES_SYNTHV1" \
	FALSE "tap-plugins" "$DES_TAPPLUGINS" \
	FALSE "tidalcycles" "$DES_TIDALCYCLES" \
	FALSE "timidity++" "$DES_TIMIDITY" \
	FALSE "vamp-aubio-plugins" "$DES_VAMPAUBIOPLUGINS" \
	FALSE "vamp-plugin-sdk" "$DES_VAMPPLUGINSSDK" \
	FALSE "vco-plugins" "$DES_VCOPLUGINS" \
	FALSE "vico" "$DES_VICO" \
	FALSE "vm.lv2" "$DES_VMLV2" \
	FALSE "vmpk" "$DES_VMPK" \
	FALSE "wah-plugins" "$DES_WAHPLUGINS" \
	FALSE "wolf-shaper" "$DES_WOLFSHAPER" \
	FALSE "wolf-spectrum" "$DES_WOLFSPECTRUM" \
	FALSE "x42-plugins" "$DES_X42PLUGINS" \
	FALSE "xmonk.lv2" "$DES_XMONKLV2" \
	FALSE "yass" "$DES_YASS" \
	FALSE "yoshimi" "$DES_YOSHIMI" \
	FALSE "zam-plugins" "$DES_ZAMPLUGINS" \
	FALSE "zita-ajbridge" "$DES_ZITAAJBRIDGE" \
	FALSE "zita-at1" "$DES_ZITAAT1" \
	FALSE "zita-bls1" "$DES_ZITABLS1" \
	FALSE "zita-dc1" "$DES_ZITADC1" \
	FALSE "zita-dpl1" "$DES_ZITADPL1" \
	FALSE "zita-lrx" "$DES_ZITALRX" \
	FALSE "zita-mu1" "$DES_ZITAMU1" \
	FALSE "zita-njbridge" "$DES_ZITANJBRIDGE" \
	FALSE "zita-rev1" "$DES_ZITAREV1" \
	FALSE "zynaddsubfx" "$DES_ZYNADDSUBFX" \
	)

	case $? in

		0 )

			pamac-installer $BOX; exec rb-center.sh pro_audio

		;;

		1 )

			[ "$BOX" = $EXTRA_BUTTON ] && exec rb-center.sh pro_audio

		;;

		* )

	esac

}

dssi_group() {

MENU="zenity --list --checklist --width=840 --height=600 --hide-header"

	BOX=$($MENU --title="$DSSI_TITLE" --text="$GROUPS_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="$COLUMN_2" --column="$COLUMN_3" --extra-button="$EXTRA_BUTTON" --multiple --separator=" " --window-icon="$ICON_PATH" \
	TRUE "amsynth" "$DES_AMSYNTH" \
	TRUE "hexter" "$DES_HEXTER" \
	TRUE "wolf-shaper" "$DES_WOLFSHAPER" \
	)

	case $? in

		0 )

			pamac-installer $BOX; exec rb-center.sh pro_audio

		;;

		1 )

			[ "$BOX" = $EXTRA_BUTTON ] && exec rb-center.sh pro_audio

		;;

		* )

	esac

}

ladspa_group() {

MENU="zenity --list --checklist --width=840 --height=570 --hide-header"

	BOX=$($MENU --title="$LADSPA_TITLE" --text="$GROUPS_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="$COLUMN_2" --column="$COLUMN_3" --extra-button="$EXTRA_BUTTON" --multiple --separator=" " --window-icon="$ICON_PATH" \
	TRUE "amb-plugins" "$DES_AMBPLUGINS" \
	TRUE "blop" "$DES_BLOP" \
	TRUE "caps" "$DES_CAPS" \
	TRUE "cmt" "$DES_CMT" \
	TRUE "dpf-plugins" "$DES_DPFPLUGINS" \
	TRUE "fil-plugins" "$DES_FILPLUGINS" \
	TRUE "g2reverb" "$DES_G2REVERB" \
	TRUE "guitarix" "$DES_GUITARIX" \
	TRUE "lsp-plugins" "DES_LSPPLUGINS" \
	TRUE "mcp-plugins" "$DES_MCPPLUGINS" \
	TRUE "pvoc" "$DES_PVOC" \
	TRUE "rev-plugins" "$DES_REVPLUGINS" \
	TRUE "swh-plugins" "$DES_SWHPLUGINS" \
	TRUE "tap-plugins" "$DES_TAPPLUGINS" \
	TRUE "vco-plugins" "$DES_VCOPLUGINS" \
	TRUE "wah-plugins" "$DES_WAHPLUGINS" \
	TRUE "zam-plugins" "$DES_ZAMPLUGINS" \
	)

	case $? in

		0 )

			pamac-installer $BOX; exec rb-center.sh pro_audio

		;;

		1 )

			[ "$BOX" = $EXTRA_BUTTON ] && exec rb-center.sh pro_audio

		;;

		* )

	esac

}

lv2_group() {

MENU="zenity --list --checklist --width=840 --height=600 --hide-header"

	BOX=$($MENU --title="$LV2_TITLE" --text="$GROUPS_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="$COLUMN_2" --column="$COLUMN_3" --extra-button="$EXTRA_BUTTON" --multiple --separator=" " --window-icon="$ICON_PATH" \
	FALSE "adlplug" "$DES_ADLPLUG" \
	FALSE "ams-lv2" "$DES_AMSLV2" \
	FALSE "amsynth" "$DES_AMSYNTH" \
	FALSE "artyfx" "$DES_ARTYFX" \
	FALSE "avldrums.lv2" "$DES_AVLDRUMSLV2" \
	FALSE "beatslash-lv2" "$DES_BEATSLASHLV2" \
	FALSE "blop.lv2" "$DES_BLOPLV2" \
	FALSE "bsequencer" "$DES_BSEQUENCER" \
	FALSE "bshapr" "$DES_BSHAPR" \
	FALSE "bslizr" "$DES_BSLIZR" \
	FALSE "calf" "$DES_CALF" \
	FALSE "deteriorate-lv2" "$DES_DETERIORATELV2" \
	FALSE "dpf-plugins" "$DES_DPFPLUGINS" \
	FALSE "dragonfly-reverb" "$DES_DRAGONFLYREVERB" \
	FALSE "drumgizmo" "$DES_DRUMGIZMO" \
	FALSE "drumkv1" "$DES_DRUMKV1" \
	FALSE "eq10q" "$DES_EQ10Q" \
	FALSE "eteroj.lv2" "$DES_ETEROJLV2" \
	FALSE "fabla" "$DES_FABLA" \
	FALSE "fomp.lv2" "$DES_FOMPLV2" \
	FALSE "gmsynth.lv2" "$DES_GMSYNTHLV2" \
	FALSE "guitarix" "$DES_GUITARIX" \
	FALSE "gxplugins.lv2" "$DES_GXPLUGINSLV2" \
	FALSE "helm-synth" "$DES_HELM" \
	FALSE "infamousplugins" "$DES_INFAMOUSPLUGINS" \
	FALSE "ir.lv2" "$DES_IRLV2" \
	FALSE "lsp-plugins" "$DES_LSPPLUGINS" \
	FALSE "lv2file" "$DES_LV2FILE" \
	FALSE "mda.lv2" "$DES_MDALV2" \
	FALSE "mephisto.lv2" "$DES_MEPHISTOLV2" \
	FALSE "midi_matrix.lv2" "$DES_MIDIMATRIXLV2" \
	FALSE "midimsg-lv2" "$DES_MIDIMSGLV2" \
	FALSE "moony.lv2" "$DES_MOONYLV2" \
	FALSE "ninjas2" "$DES_NINJAS2" \
	FALSE "noise-repellent" "$DES_NOISEREPELLENT" \
	FALSE "opnplug" "$DES_OPNPLUG" \
	FALSE "padthv1" "$DES_PADTHV1" \
	FALSE "patchmatrix" "$DES_PATCHMATRIX" \
	FALSE "qmidiarp" "$DES_QMIDIARP" \
	FALSE "samplv1" "$DES_SAMPLV1" \
	FALSE "setbfree" "$DES_SETBFREE" \
	FALSE "sherlock.lv2" "$DES_SHERLOCKLV2" \
	FALSE "sorcer" "$DES_SORCER" \
	FALSE "spectmorph" "$DES_SPECTMORPH" \
	FALSE "synthv1" "$DES_SYNTHV1" \
	FALSE "vm.lv2" "$DES_VMLV2" \
	FALSE "wolf-shaper" "$DES_WOLFSHAPER" \
	FALSE "wolf-spectrum" "$DES_WOLFSPECTRUM" \
	FALSE "x42-plugins" "$DES_X42PLUGINS" \
	FALSE "xmonk.lv2" "$DES_XMONKLV2" \
	FALSE "zam-plugins" "$DES_ZAMPLUGINS" \
	FALSE "zynaddsubfx" "$DES_ZYNADDSUBFX" \
	)

	case $? in

		0 )

			pamac-installer $BOX; exec rb-center.sh pro_audio

		;;

		1 )

			[ "$BOX" = $EXTRA_BUTTON ] && exec rb-center.sh pro_audio

		;;

		* )

	esac

}

vst_group() {

MENU="zenity --list --checklist --width=840 --height=600 --hide-header"

	BOX=$($MENU --title="$VST_TITLE" --text="$GROUPS_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="$COLUMN_2" --column="$COLUMN_3" --extra-button="$EXTRA_BUTTON" --multiple --separator=" " --window-icon="$ICON_PATH" \
	TRUE "adlplug" "$DES_ADLPLUG" \
	TRUE "amsynth" "$DES_AMSYNTH" \
	TRUE "dpf-plugins" "$DES_DPFPLUGINS" \
	TRUE "dragonfly-reverb" "$DES_DRAGONFLYREVERB" \
	TRUE "iempluginsuite" "$DES_IEMPLUGINSUITE" \
	TRUE "lsp-plugins" "$DES_LSPPLUGINS" \
	TRUE "opnplug" "$DES_OPNPLUG" \
	TRUE "wolf-shaper" "$DES_WOLFSHAPER" \
	TRUE "wolf-spectrum" "$DES_WOLFSPECTRUM" \
	TRUE "zam-plugins" "$DES_ZAMPLUGINS" \
	TRUE "zynaddsubfx" "$DES_ZYNADDSUBFX" \
	)

	case $? in

		0 )

			pamac-installer $BOX; exec rb-center.sh pro_audio

		;;

		1 )

			[ "$BOX" = $EXTRA_BUTTON ] && exec rb-center.sh pro_audio

		;;

		* )

	esac

}

non_daw() {

MENU="zenity --list --checklist --width=840 --height=600 --hide-header"

	BOX=$($MENU --title="$NON_DAW_TITLE" --text="$GROUPS_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="$COLUMN_2" --column="$COLUMN_3" --extra-button="$EXTRA_BUTTON"  --multiple --separator=" " --window-icon="$ICON_PATH" \
	TRUE "non-mixer" "$DES_NONMIXER" \
	TRUE "non-sequencer" "$DES_NONSEQUENCER" \
	TRUE "non-session-manager" "$DES_NONSESSIONMANAGER" \
	TRUE "non-timeline" "$DES_NONTIMELINE" \
	)

	case $? in

		0 )

			pamac-installer $BOX; exec rb-center.sh pro_audio

		;;

		1 )

			[ "$BOX" = $EXTRA_BUTTON ] && exec rb-center.sh pro_audio

		;;

		* )

	esac

}

recbox_ones() {

MENU="zenity --list --checklist --width=840 --height=600 --hide-header"

	BOX=$($MENU --title="$RECBOX_ONES_TITLE" --text="$RECBOX_ONES_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="$COLUMN_2" --column="$COLUMN_3" --extra-button="$EXTRA_BUTTON" --multiple --separator=" " --window-icon="$ICON_PATH" \
	TRUE "a2jmidid" "$DES_A2JMIDID" \
	TRUE "aeolus" "$DES_AEOLUS" \
	TRUE "amb-plugins" "$DES_AMBPLUGINS" \
	TRUE "ardour" "$DES_ARDOUR" \
	TRUE "artyfx" "$DES_ARTYFX" \
	TRUE "aubio" "$DES_AUBIO" \
	TRUE "avldrums.lv2" "$DES_AVLDRUMSLV2" \
	TRUE "cadence" "$DES_CADENCE" \
	TRUE "calf" "$DES_CALF" \
	TRUE "caps" "$DES_CAPS" \
	TRUE "carla" "$DES_CARLA" \
	TRUE "cmt" "$DES_CMT" \
	TRUE "dpf-plugins" "$DES_DPFPLUGINS" \
	TRUE "dragonfly-reverb" "$DES_DRAGONFLYREVERB" \
	TRUE "dssi" "$DES_DSSI" \
	TRUE "eq10q" "$DES_EQ10Q" \
	TRUE "eteroj.lv2" "$DES_ETEROJLV2" \
	TRUE "fabla" "$DES_FABLA" \
	TRUE "fil-plugins" "$DES_FILPLUGINS" \
	TRUE "fluidsynth" "$DES_FLUIDSYNTH" \
	TRUE "fomp.lv2" "$DES_FOMPLV2" \
	TRUE "gmsynth.lv2" "$DES_GMSYNTHLV2" \
	TRUE "guitarix" "$DES_GUITARIX" \
	TRUE "helm-synth" "$DES_HELM" \
	TRUE "hydrogen" "$DES_HYDROGEN" \
	TRUE "ir.lv2" "$DES_IRLV2" \
	TRUE "jack_capture" "$DES_JACKCAPTURE" \
	TRUE "jacktrip" "$DES_JACKTRIP" \
	TRUE "jalv" "$DES_JALV" \
	TRUE "mcp-plugins" "$DES_MCPPLUGINS" \
	TRUE "mda.lv2" "$DES_MDALV2" \
	TRUE "midi_matrix.lv2" "$DES_MIDIMATRIXLV2" \
	TRUE "njconnect" "$DES_NJCONNECT" \
	TRUE "osc2midi" "$DES_OSC2MIDI" \
	TRUE "osmid" "$DES_OSMID" \
	TRUE "patchmatrix" "$DES_PATCHMATRIX" \
	TRUE "pulseaudio-alsa" "$DES_PAALSA" \
	TRUE "pulseaudio-jack" "$DES_PAJACK" \
	TRUE "pvoc" "$DES_PVOC" \
	TRUE "qastools" "$DES_QASTOOLS" \
	TRUE "qjackctl" "$DES_QJACKCTL" \
	TRUE "qmidiarp" "$DES_QMIDIARP" \
	TRUE "qmidictl" "$DES_QMIDICTL" \
	TRUE "qmidinet" "$DES_QMIDINET" \
	TRUE "qmidiroute" "$DES_QMIDIROUTE" \
	TRUE "qxgedit" "$DES_QXGEDIT" \
	TRUE "realtime-privileges" "$DES_REALTIMEPRIVILEGES" \
	TRUE "rev-plugins" "$DES_REVPLUGINS" \
	TRUE "setbfree" "$DES_SETBFREE" \
	TRUE "swh-plugins" "$DES_SWHPLUGINS" \
	TRUE "tap-plugins" "$DES_TAPPLUGINS" \
	TRUE "vamp-plugin-sdk" "$DES_VAMPPLUGINSSDK" \
	TRUE "vco-plugins" "$DES_VCOPLUGINS" \
	TRUE "vm.lv2" "$DES_VCOPLUGINS" \
	TRUE "wolf-shaper" "$DES_WOLFSHAPER" \
	TRUE "yoshimi" "$DES_YOSHIMI" \
	TRUE "zam-plugins" "$DES_ZAMPLUGINS" \
	TRUE "zita-ajbridge" "$DES_ZITAAJBRIDGE" \
	TRUE "zita-at1" "$DES_ZITAAT1" \
	TRUE "zita-bls1" "$DES_ZITABLS1" \
	TRUE "zita-dpl1" "$DES_ZITADPL1" \
	TRUE "zita-lrx" "$DES_ZITALRX" \
	TRUE "zita-mu1" "$DES_ZITAMU1" \
	TRUE "zita-njbridge" "$DES_ZITANJBRIDGE" \
	TRUE "zita-rev1" "$DES_ZITAREV1" \
	TRUE "zynaddsubfx" "$DES_ZYNADDSUBFX" \
	)

	case $? in

		0 )

			pamac-installer $BOX; exec rb-center.sh pro_audio

		;;

		1 )

			[ "$BOX" = $EXTRA_BUTTON ] && exec rb-center.sh pro_audio

		;;

		* )

	esac

}

extra_plugins() {

MENU="zenity --list --checklist --width=840 --height=600 --hide-header"

	BOX=$($MENU --title="$EXTRA_PLUGINS_TITLE" --text="$EXTRA_PLUGINS_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="$COLUMN_2" --column="$COLUMN_3" --extra-button="$EXTRA_BUTTON" --multiple --separator=" " --window-icon="$ICON_PATH" \
	FALSE "bitrot-git" "$DES_BITROTGIT" \
	FALSE "creox-git" "$DES_CREOXGIT" \
	FALSE "dexed" "$DES_DEXED" \
	FALSE "digitsvst-git" "$DES_DIGITSVSTGIT" \
	FALSE "distrho-lv2-git" "$DES_DISTRHOLV2GIT" \
	FALSE "distrho-vst-git" "$DES_DISTRHOVSTGIT" \
	FALSE "distrho-extra-lv2-git" "$DES_DISTRHOEXTRALV2GIT" \
	FALSE "drmr-git" "$DES_DRMRGIT" \
	FALSE "dssi-vst" "$DES_DSSIVST" \
	FALSE "openav-fabla2-git" "$DES_OPENAVFABLA2GIT" \
	FALSE "luftikus" "$DES_LUFTIKUS" \
	FALSE "tal-plugins" "$DES_TALPLUGINS" \
	FALSE "fluidplug-git" "$DES_FLUIDPLUGGIT" \
	FALSE "foo-yc20" "$DES_FOOYC20" \
	FALSE "ghostess" "$DES_GHOSTESS" \
	FALSE "hybridreverb2-git" "$DES_HYBRIDREVERB2GIT" \
	FALSE "ingen-git" "$DES_INGENGIT" \
	FALSE "instalooper-vst" "$DES_INSTALOOPERVST" \
	FALSE "jackass-git" "$DES_JACKASSGIT" \
	FALSE "jackman-git" "$DES_JACKMANGIT" \
	FALSE "jackman-kcm-git" "$DES_JACKMANKCMGIT" \
	FALSE "mustang-plug" "$DES_MUSTANGPLUG" \
	FALSE "ob-xd" "$DES_OBXD" \
	FALSE "oxefmsynth" "$DES_OXEFMSYNTH" \
	FALSE "rakarrack" "$DES_RAKARRACK" \
	FALSE "rkr-lv2-git" "$DES_RKRLV2GIT" \
	FALSE "rt_pvc" "$DES_RTPVC" \
	FALSE "tonelib-gfx-bin" "$DES_TONELIBGFXBIN" \
	FALSE "tonelib-jam-bin" "$DES_TONELIBJAMBIN" \
	FALSE "tunefish4" "$DES_TUNEFISH4" \
	FALSE "vcvrack" "$DES_VCVRACK" \
	FALSE "vcvrack-fundamental" "$DES_VCVRACKFUNDAMENTAL" \
	FALSE "vcvrack-audible-instruments" "$DES_VCVRACKAUDIBLEINSTRUMENTS" \
	FALSE "vcvrack-befaco" "$DES_VCVRACKBEFACO" \
	FALSE "vcvrack-eseries" "$DES_VCVRACKESERIES" \
	FALSE "vcvrack-sonusmodular-git" "$DES_VCVRACKSONUSMODULARGIT" \
	FALSE "vlevel-git" "$DES_VLEVELGIT" \
	FALSE "vocoder-jack" "$DES_VOCODERJACK" \
	FALSE "vocoder-ladspa" "$DES_VOCODERLADSPA." \
	FALSE "whysynth" "$DES_WHYSYNTH" \
	FALSE "xsynth-dssi" "$DES_XSYNTHDSSI" \
	)

	case $? in

		0 )

			pamac-installer --build $BOX; exec rb-center.sh main

		;;

		1 )

			[ "$BOX" = $EXTRA_BUTTON ] && exec rb-center.sh main

		;;

		* )

	esac

}

daw() {

MENU="zenity --list --width=635 --height=325 --hide-header"

	BOX=$($MENU --title="$DAW_TITLE" --text="$DAW_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="$COLUMN_2" --extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
		"Ardour 6" "Professional-grade digital audio workstation." \
		"Audacity" "A program that lets you manipulate digital audio waveforms." \
		"Bitwig Studio" "Digital audio workstation for music production, remixing and live performance." \
		"LMMS" "The Linux MultiMedia Studio." \
		"Reaper" "Digital Audio Workstation." \
		"Tracktion Waveform" "Audio and MIDI Workstation." \
		"Zrythm" "Free GNU/Linux music production system." \
		)

	if [ "$BOX" = "Ardour 6" ]; then

		pamac-installer ardour; exec rb-center.sh daw

	elif [ "$BOX" = "Audacity" ]; then

		pamac-installer audacity; exec rb-center.sh daw

	elif [ "$BOX" = "Bitwig Studio" ]; then

		pamac-installer --build bitwig-studio; exec rb-center.sh daw

	elif [ "$BOX" = "LMMS" ]; then

		pamac-installer lmms; exec rb-center.sh daw

	elif [ "$BOX" = "Reaper" ]; then

		pamac-installer --build reaper-bin; exec rb-center.sh daw

	elif [ "$BOX" = "Tracktion Waveform" ]; then

		pamac-installer --build tracktion-waveform; exec rb-center.sh daw

	elif [ "$BOX" = "Zrythm" ]; then

		pamac-installer --build zrythm; exec rb-center.sh daw

	elif [ "$BOX" = "$EXTRA_BUTTON" ]; then

		exec rb-center.sh main

	fi

}

real_time() {

LIST="$LIST $(mhwd-kernel -l | grep 'rt' | cut -d '*' -f2 | cut -d ' ' -f 2 | sed 's/^/FALSE /')"
ZEN_BOX="zenity --list --checklist --width=620 --height=298  --hide-header"

 	BOX=$($ZEN_BOX --title="$REAL_TIME_TITLE" --text="$REAL_TIME_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
 	--column="$COLUMN_1" --column="$COLUMN_2" --extra-button="$EXTRA_BUTTON" $LIST --separator=" " --multiple --window-icon="$ICON_PATH")

 	case $? in

 		0 )

		 	termite --exec="sudo mhwd-kernel -i $BOX"; exec rb-center.sh real_time

 		;;

 		1 )

		 	[ "$BOX" = $EXTRA_BUTTON ] && exec rb-center.sh main

		;;

		* )

 	esac

}

tools() {

MENU="zenity --list --checklist --width=635 --height=298 --hide-header"

	BOX=$($MENU --title="$TOOLS_TITLE" --text="$TOOLS_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="$COLUMN_2" --column="$COLUMN_3" --extra-button="$EXTRA_BUTTON" --multiple --separator=" " --window-icon="$ICON_PATH" \
	FALSE "realtime-privileges" "Realtime privileges for users." \
	FALSE "rt-tests" "A collection of latency testing tools for linux-rt kernel." \
	FALSE "rtirq" "Realtime IRQ thread system tunung." \
	FALSE "tuna" "Thread and IRQ affinity setting GUI and cmd line tool." \
	FALSE "schedtool" "Query or alter a process' scheduling policy." \
	)

	case $? in

		0 )

			pamac-installer $BOX; exec rb-center.sh main

		;;

		1 )

			[ "$BOX" = $EXTRA_BUTTON ] && exec rb-center.sh main

		;;

		* )

	esac

}

panel_menus_daily() {

MENU="zenity --list --width=620 --height=298 --hide-header"

	BOX=$($MENU --title="$PANEL_MENU_TITLE" --text="$PANEL_MENUS_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="COLUMN_2" --extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
		"  Window" "" \
		"  Tools" "" \
		"  Shortcuts" "" \
		"  Software" "" \
		"  Info" "" \
		)

	if [ "$BOX" = "  Window" ]; then

		exo-open $HOME/.config/jgmenu/window.csv & exec rb-center.sh panel_menus_daily

	elif [ "$BOX" = "  Tools" ]; then

		exo-open $HOME/.config/jgmenu/tools.csv & exec rb-center.sh panel_menus_daily

	elif [ "$BOX" = "  Shortcuts" ]; then

		exo-open $HOME/.config/jgmenu/shortcuts.csv & exec rb-center.sh panel_menus_daily

	elif [ "$BOX" = "  Software" ]; then

		exo-open $HOME/.config/jgmenu/software.csv & exec rb-center.sh panel_menus_daily

	elif [ "$BOX" = "  Info" ]; then

		exo-open $HOME/.config/jgmenu/info.csv & exec rb-center.sh panel_menus_daily

 	elif [ "$BOX" = $EXTRA_BUTTON ]; then

		exec rb-center.sh main

	fi 2>/dev/null

}

panel_menus_studio() {

MENU="zenity --list --width=620 --height=298 --hide-header"

	BOX=$($MENU --title="$PANEL_MENU_TITLE" --text="$PANEL_MENUS_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
	--column="$COLUMN_1" --column="COLUMN_2" --extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
		"  Jack Connection" "" \
		"  Mixer" "" \
		"  Instrument FX" "" \
		"  Carla" "" \
		"  Notes" "" \
		)

	if [ "$BOX" = "  Jack Connection" ]; then

		exo-open $HOME/.config/jgmenu/jack-connection.csv & exec rb-center.sh panel_menus_studio

	elif [ "$BOX" = "  Mixer" ]; then

		exo-open $HOME/.config/jgmenu/mixer.csv & exec rb-center.sh panel_menus_studio

	elif [ "$BOX" = "  Instrument FX" ]; then

		exo-open $HOME/.config/jgmenu/instrumentfx.csv & exec rb-center.sh panel_menus_studio

	elif [ "$BOX" = "  Carla" ]; then

		exo-open $HOME/.config/jgmenu/carla.csv & exec rb-center.sh panel_menus_studio

	elif [ "$BOX" = "  Notes" ]; then

		exo-open $HOME/.config/jgmenu/notes.csv & exec rb-center.sh panel_menus_studio

 	elif [ "$BOX" = $EXTRA_BUTTON ]; then

		exec rb-center.sh main

	fi

}

ver() {

	echo -e "\n    ${WHITE}Version 2.1${RESETCOLOR}"

}

version() {

	echo -e "\n    ${WHITE}Version 2.1${RESETCOLOR}"

}

case "$1" in
	main) main;;
	pro_audio) pro_audio;;
	pro_audio_multiple) pro_audio_multiple;;
	dssi_group) dssi_group;;
	ladspa_group) ladspa_group;;
	lv2_group) lv2_group;;
	vst_group) vst_group;;
	non_daw) non_daw;;
	recbox_ones) recbox_ones;;
	extra_plugins) extra_plugins;;
	daw) daw;;
	real_time) real_time;;
	tools) tools;;
	edit_jgmenu) edit_jgmenu;;
	panel_menus_daily) panel_menus_daily;;
	panel_menus_studio) panel_menus_studio;;
	ver) ver;;
	version) version;;
	*)

	echo -e "
${GREEN}\n	--------------------------------------------------------------------------
				RecBox Software Center
	--------------------------------------------------------------------------\n
	Made by Projekt:Root projekt.root@tuta.io
	for RecBox and Garuda OpenBox.
	If you want to use script on other setup 
	probably you'll need to modify it.\n
	Script require zenity, pamac-gtk, pamac-cli and termite terminal.\n
	--------------------------------------------------------------------------${RESETCOLOR}

	Usage:
	rb-center.sh${YELLOW} [${RED} OPTION${YELLOW} ]

${GREEN}	Options:

${YELLOW}	>${RED} main $YELLOW          -${RESETCOLOR} execute main menu
${YELLOW}	>${RED} pro_audio $YELLOW     -${RESETCOLOR} execute menu with Pro Audio plugins and software
${YELLOW}	>${RED} dssi_group $YELLOW    -${RESETCOLOR} execute sub menu with DSSI plugins group
${YELLOW}	>${RED} ladspa_group $YELLOW  -${RESETCOLOR} execute sub menu with LADSPA plugins group
${YELLOW}	>${RED} lv2_group $YELLOW     -${RESETCOLOR} execute sub menu with LV2 plugins group
${YELLOW}	>${RED} vst_group $YELLOW     -${RESETCOLOR} execute sub menu with VST plugins group
${YELLOW}	>${RED} non_daw $YELLOW       -${RESETCOLOR} execute sub menu with Non DAW modules
${YELLOW}	>${RED} recbox_ones $YELLOW   -${RESETCOLOR} execute sub menu with plugins selected by RecBox
${YELLOW}	>${RED} extra_plugins $YELLOW -${RESETCOLOR} execute menu with plugins and software from AUR
${YELLOW}	>${RED} daw $YELLOW           -${RESETCOLOR} execute menu audio production software
${YELLOW}	>${RED} real_time $YELLOW     -${RESETCOLOR} execute menu real-time kernels
${YELLOW}	>${RED} tools $YELLOW         -${RESETCOLOR} execute menu with tools for tuning real-time kernels
${YELLOW}	>${RED} edit_jgmenu $YELLOW   -${RESETCOLOR} open config file with Audio Software

${YELLOW}	>${RED} ver, version $YELLOW  -${RESETCOLOR} show script version
${RESETCOLOR}
" >&2
exit 1
;;
esac

echo -e $RESETCOLOR
exit 0
