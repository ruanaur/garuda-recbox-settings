#!/bin/sh

CURRENT_SETTINGS=$(grep XRANDR_CURRENT $HOME/.config/night-light-settings/config | cut -d "'" -f 2)
SHUTDOWN_TITLE="Session Shutdown"
SHUTDOWN_TEXT="Do you want to <b>Shutdown Session</b>?"
SHUTDOWN_ICON_PATH="/usr/share/icons/Flat-Remix-Green/categories/scalable/system-shutdown.svg"
REBOOT_TITLE="Session Reboot"
REBOOT_TEXT="Do you want to <b>Reboot Session</b>?"
REBOOT_ICON_PATH="/usr/share/icons/Flat-Remix-Green/categories/scalable/system-reboot.svg"
HIBERNATE_TITLE="Session Hibernate"
HIBERNATE_TEXT="Do you want to <b>Hibernate Session</b>?"
HIBERNATE_ICON_PATH="/usr/share/icons/Flat-Remix-Green/categories/scalable/system-suspend-hibernate.svg"
LOGOUT_TITLE="Session Logout"
LOGOUT_TEXT="Do you want to <b>Logout</b>?"
LOGOUT_ICON_PATH="/usr/share/icons/Flat-Remix-Green/categories/scalable/system-log-out.svg"
MENU_WIDTH="180"

shutdown() {

	MENU_BOX=$(zenity --question --no-wrap --title="$SHUTDOWN_TITLE" --text="$SHUTDOWN_TEXT" --window-icon="$SHUTDOWN_ICON_PATH" --width="$MENU_WIDTH")

		case $? in

			0)

				if [ $CURRENT_SETTINGS = "ON" ]; then

					sed -i "s/$CURRENT/OFF/" $HOME/.config/night-light-settings/config && systemctl -i poweroff

					elif [ $CURRENT_SETTINGS = "OFF" ]; then

						systemctl -i poweroff

				fi

			;;

			1) exit 0;;

		esac

}

reboot() {

	MENU_BOX=$(zenity --question --no-wrap --title="$REBOOT_TITLE" --text="$REBOOT_TEXT" --window-icon="$REBOOT_ICON_PATH" --width="$MENU_WIDTH")

		case $? in

			0)

				if [ $CURRENT_SETTINGS = "ON" ]; then

					sed -i "s/$CURRENT/OFF/" $HOME/.config/night-light-settings/config && systemctl -i reboot

					elif [ $CURRENT_SETTINGS = "OFF" ]; then

						systemctl -i reboot

				fi

			;;

			1) exit 0;;

		esac

}

hibernate() {

	MENU_BOX=$(zenity --question --no-wrap --title="$HIBERNATE_TITLE" --text="$HIBERNATE_TEXT" --window-icon="$HIBERNATE_ICON_PATH" --width="$MENU_WIDTH")

		case $? in

			0) systemctl hibernate;;
			1) exit 0;;

		esac

}

logout() {

	MENU_BOX=$(zenity --question --no-wrap --title="$LOGOUT_TITLE" --text="$LOGOUT_TEXT" --window-icon="$LOGOUT_ICON_PATH" --width="$MENU_WIDTH")

		case $? in

			0)

				if [ $CURRENT_SETTINGS = "ON" ]; then

					sed -i "s/$CURRENT/OFF/" $HOME/.config/night-light-settings/config && systemctl -i poweroff

					elif [ $CURRENT_SETTINGS = "OFF" ]; then

						openbox --exit

				fi

			;;

			1) exit 0;;

		esac

}

rofi_session_menu() {

	SESSION=$(echo "Shutdown|Reboot|Suspend|Hibernate|Logout|Lock" | \
	rofi -sep "|" -dmenu -i -p ' Session ' "")

		case $SESSION in

			*"Shutdown")

				SESSION_SHUTDOWN=$(echo "Yes|No" | rofi -sep "|" -dmenu -i -p ' Shutdown ' "")


					case $SESSION_SHUTDOWN in

						*"Yes")

							if [ $CURRENT_SETTINGS = "ON" ]; then

								sed -i "s/$CURRENT/OFF/" $HOME/.config/night-light-settings/config && systemctl -i poweroff

								elif [ $CURRENT_SETTINGS = "OFF" ]; then

									systemctl -i poweroff

							fi

						;;

						*"No") rb-session.sh rofi_session_menu;;
						*)

					esac

			;;

			*"Reboot")

				SESSION_REBOOT=$(echo "Yes|No" | rofi -sep "|" -dmenu -i -p ' Reboot ' "")


					case $SESSION_REBOOT in

						*"Yes")

							if [ $CURRENT_SETTINGS = "ON" ]; then

								sed -i "s/$CURRENT/OFF/" $HOME/.config/night-light-settings/config && systemctl reboot

								elif [ $CURRENT_SETTINGS = "OFF" ]; then

									systemctl reboot

							fi

						;;

						*"No") rb-session.sh rofi_session_menu;;
						*)

					esac

			;;

			*"Suspend")

				TIMER=$(echo "Now|15 minutes|45 minutes|1 hour|2 hours|Cancel|Back" | \
				rofi -sep "|" -dmenu -i -p ' Suspend ' "")

					case $TIMER in

						*"Now") sleep 1; systemctl suspend;;
						*"15 minutes") rb-session-suspend.sh susp_a15ms;;
						*"45 minutes") rb-session-suspend.sh susp_a45ms;;
						*"1 hour") rb-session-suspend.sh susp_a1h;;
						*"2 hours") rb-session-suspend.sh susp_a2hs;;
						*"Cancel") exec rb-session-suspend.sh cancel;;
						*"Back") rb-session.sh rofi_session_menu;;
						*)

					esac

			;;

			*"Hibernate")

				SESSION_HIBERNATE=$(echo "Yes|No" | rofi -sep "|" -dmenu -i -p ' Hibernate ' "")


					case $SESSION_HIBERNATE in

						*"Yes") systemctl hibernate;;
						*"No") rb-session.sh rofi_session_menu;;
						*)

					esac

			;;

			*"Logout")

				SESSION_LOGOUT=$(echo "Yes|No" | rofi -sep "|" -dmenu -i -p ' Logout ' "")


					case $SESSION_LOGOUT in

						*"Yes")

							if [ $CURRENT_SETTINGS = "ON" ]; then

								sed -i "s/$CURRENT/OFF/" $HOME/.config/night-light-settings/config && openbox --exit

								elif [ $CURRENT_SETTINGS = "OFF" ]; then

									openbox --exit

							fi

						;;

						*"No") rb-session.sh rofi_session_menu;;
						*)

					esac

			;;
			*"Lock") betterlockscreen -l;;
			*)

		esac

}

ver() {

	echo "\n    Version 1.1.1"

}

version() {

	echo "\n    Version 1.1.1"

}

case $1 in

	shutdown) shutdown;;
	reboot) reboot;;
	hibernate) hibernate;;
	logout) logout;;
	rofi_session_menu) rofi_session_menu;;
	ver) ver;;
	version) version;;
	*)

	echo -e "
	RecBox Session\n
	Made by Projekt:Root projekt.root@tuta.io
	for RecBox and Garuda OpenBox.
	If you want to use script on other setup 
	probably you'll need to modify it.\n
	Usage:\n
	scripts-box.sh [ OPTION ]\n
	Options:\n
	> shutdown
	> reboot
	> hibernate
	> logout
	> rofi_session_menu
	> ver, version
" >&2

exit 1;;

esac

exit 0
