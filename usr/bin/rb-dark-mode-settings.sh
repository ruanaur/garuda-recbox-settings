#!/usr/bin/sh

GTK_LIGHT_THEME=$(grep 'GTK_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
GTK_DARK_THEME=$(grep 'GTK_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
QT_LIGHT_THEME=$(grep 'QT_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
QT_DARK_THEME=$(grep 'QT_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
OPEN_BOX_LIGHT_THEME=$(grep 'OBOX_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
OPEN_BOX_DARK_THEME=$(grep 'OBOX_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
OPEN_BOX_THEME_XML=$(grep 'THEME_NAME' $HOME/.config/openbox/rc.xml | sed -e 's|<name>\(.*\)</name> <!-- THEME_NAME -->|\1|' | sed 's/  *//')
ROFI_LIGHT_THEME=$(grep 'ROFI_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
ROFI_DARK_THEME=$(grep 'ROFI_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
ICONS_LIGHT_THEME=$(grep 'ICONS_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
ICONS_DARK_THEME=$(grep 'ICONS_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
MAINMENU_ICONS_LIGHT_THEME=$(grep 'MAINMENU_ICONS_LIGHT_THEME=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
MAINMENU_ICONS_DARK_THEME=$(grep 'MAINMENU_ICONS_DARK_THEME=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)

GTK_LIGHT_THEME_DEF="Matcha-light-sea"
GTK_DARK_THEME_DEF="Matcha-dark-sea"
QT_LIGHT_THEME_DEF="Matcha-Light#"
QT_DARK_THEME_DEF="Matcha-Dark#"
OPEN_BOX_LIGHT_THEME_DEF="light-recbox"
OPEN_BOX_DARK_THEME_DEF="dark-recbox"
ROFI_LIGHT_THEME_DEF="RecBox-Light.rasi"
ROFI_DARK_THEME_DEF="RecBox-Dark.rasi"
ICONS_LIGHT_THEME_DEF="Flat-Remix-Green-Light"
ICONS_DARK_THEME_DEF="Flat-Remix-Green-Dark"
MAINMENU_ICONS_LIGHT_THEME_DEF="Flat-Remix-Green-Light"
MAINMENU_ICONS_DARK_THEME_DEF="Flat-Remix-Green-Dark"

MODE_TEST=$(grep SIDEMENU_RC $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)

TITLE="RecBox Dark Mode Settings"
TEXT="<b>GTK themes:</b>\n<b>light: </b>$GTK_LIGHT_THEME    <b>dark: </b>$GTK_DARK_THEME\n\n<b>QT themes (Kvantum): </b>\n<b>light: </b>$QT_LIGHT_THEME    <b>dark: </b>$QT_DARK_THEME\n\n<b>Openbox themes:</b>\n<b>light: </b>$OPEN_BOX_LIGHT_THEME    <b>dark: </b>$OPEN_BOX_DARK_THEME\n\n<b>Rofi themes:</b>\n<b>light: </b>$ROFI_LIGHT_THEME    <b>dark: </b>$ROFI_DARK_THEME\n\n<b>Icons themes:</b>\n<b>light: </b>$ICONS_LIGHT_THEME    <b>dark: </b>$ICONS_DARK_THEME\n\n<b>Main menu Icons themes:</b>\n<b>light: </b>$MAINMENU_ICONS_LIGHT_THEME    <b>dark: </b>$MAINMENU_ICONS_DARK_THEME\n"
PICK_COLUMN_TITLE="Pick"
THEME_COLUMN_TITLE="Theme"
OK_BUTTON="Apply"
CANCEL_BUTTON="Close"
EXTRA_BUTTON="Default"
EXTRA_BUTTON_BACK="Back"
ZEN_MENU="zenity --list --radiolist --width=440 --height=551 --hide-header"

		BOX=$($ZEN_MENU --title="$TITLE" --text="$TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON" \
			--column="$PICK_COLUMN_TITLE" --column "$THEME_COLUMN_TITLE" TRUE "Change GTK light theme" FALSE "Change GTK dark theme" \
			FALSE "Change QT light theme" FALSE "Change QT dark theme" FALSE "Change Openbox light theme" FALSE "Change Openbox dark theme" FALSE "Change Rofi light theme" FALSE "Change Rofi dark theme" \
			FALSE "Change Icons light theme" FALSE "Change Icons dark theme" FALSE "Change Main menu Icons light theme" FALSE "Change Main menu Icons dark theme" 2>/dev/null)

			case $? in

				0 )

					if [ "$BOX" = "Change GTK light theme" ]; then

						TITLE="GTK ligh theme chooser"
						TEXT="Choose which theme you want to use as GTK light theme."
						ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
						COLUMN_TITLE="Pick"
						THEME_ITEMS=$(ls -d /usr/share/themes/* | cut -b 19-)

						BOX=$(ls -d /usr/share/themes/* | cut -b 19- | $ZEN_MENU --title="$TITLE" --text="$TEXT" \
						--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --column="$COLUMN_TITLE" 2>/dev/null)

							case $? in

								0 )

									if [ $MODE_TEST = "sidemenu-light-rc" ]; then

										sed -i "3s/$GTK_LIGHT_THEME/$BOX/" $HOME/.config/darkmode-settings/config
										sed -i "4s/$GTK_LIGHT_THEME/$BOX/" $HOME/.gtkrc-2.0
										sed -i "2s/$GTK_LIGHT_THEME/$BOX/" $HOME/.config/gtk-3.0/settings.ini

										xfconf-query -c xsettings -p /Net/ThemeName -s $BOX; notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

										sed -i "3s/$GTK_LIGHT_THEME/$BOX/" $HOME/.config/darkmode-settings/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									fi

								;;

								1 )

									[ "$BOX" = $EXTRA_BUTTON_BACK ] && exec rb-dark-mode-settings.sh

								;;

								* )

							esac

					elif [ "$BOX" = "Change GTK dark theme" ]; then

						TITLE="Dark theme chooser"
						TEXT="Choose which theme you want to use as GTK dark theme."
						ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
						COLUMN_TITLE="Pick"

						BOX=$(ls -d /usr/share/themes/* | cut -b 19- | $ZEN_MENU --title="$TITLE" --text="$TEXT" \
						--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --column="$COLUMN_TITLE" 2>/dev/null)

							case $? in

								0 )

									if [ $MODE_TEST = "sidemenu-light-rc" ]; then

										sed -i "4s/$GTK_DARK_THEME/$BOX/" $HOME/.config/darkmode-settings/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

										sed -i "4s/$GTK_DARK_THEME/$BOX/" $HOME/.config/darkmode-settings/config
										sed -i "4s/$GTK_DARK_THEME/$BOX/" $HOME/.gtkrc-2.0
										sed -i "2s/$GTK_DARK_THEME/$BOX/" $HOME/.config/gtk-3.0/settings.ini

										xfconf-query -c xsettings -p /Net/ThemeName -s $BOX; notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									fi

								;;

								1 )

									[ "$BOX" = $EXTRA_BUTTON_BACK ] && exec rb-dark-mode-settings.sh

								;;

								* )

							esac

					elif [ "$BOX" = "Change QT light theme" ]; then

						TITLE="Ligh theme chooser"
						TEXT="Choose which theme you want to use as QT (Kvantum) light theme."
						ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
						COLUMN_TITLE="Pick"

						BOX=$(ls -d ~/.config/Kvantum/* | cut -b 30- | grep -v kvantum.kvconfig | $ZEN_MENU --title="$TITLE" --text="$TEXT" \
						--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --column="$COLUMN_TITLE" 2>/dev/null)

							case $? in

								0 )

									if [ $MODE_TEST = "sidemenu-light-rc" ]; then

										sed -i "5s/$QT_LIGHT_THEME/$BOX/" $HOME/.config/darkmode-settings/config
										sed -i "2s/$QT_LIGHT_THEME/$BOX/" $HOME/.config/Kvantum/kvantum.kvconfig

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

										sed -i "5s/$QT_LIGHT_THEME/$BOX/" $HOME/.config/darkmode-settings/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									fi



								;;

								1 )

									[ "$BOX" = $EXTRA_BUTTON_BACK ] && exec rb-dark-mode-settings.sh

								;;

								* )

							esac

					elif [ "$BOX" = "Change QT dark theme" ]; then

						TITLE="Dark theme chooser"
						TEXT="Choose which theme you want to use as QT (Kvantum) dark theme."
						ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
						COLUMN_TITLE="Pick"

						BOX=$(ls -d ~/.config/Kvantum/* | cut -b 30- | grep -v kvantum.kvconfig | $ZEN_MENU --title="$TITLE" --text="$TEXT" \
						--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --column="$COLUMN_TITLE" 2>/dev/null)

							case $? in

								0 )

									if [ $MODE_TEST = "sidemenu-light-rc" ]; then

										sed -i "6s/$QT_DARK_THEME/$BOX/" $HOME/.config/darkmode-settings/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

										sed -i "6s/$QT_DARK_THEME/$BOX/" $HOME/.config/darkmode-settings/config
										sed -i "2s/$QT_DARK_THEME/$BOX/" $HOME/.config/Kvantum/kvantum.kvconfig

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									fi

								;;

								1 )

									[ "$BOX" = $EXTRA_BUTTON_BACK ] && exec rb-dark-mode-settings.sh

								;;

								* )

							esac

					elif [ "$BOX" = "Change Openbox light theme" ]; then

						TITLE="Ligh theme chooser"
						TEXT="Choose which theme you want to use as Openbox light theme."
						ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
						COLUMN_TITLE="Pick"

						BOX=$(ls -d /usr/share/themes/* | cut -b 19- | $ZEN_MENU --title="$TITLE" --text="$TEXT" \
						--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --column="$COLUMN_TITLE" 2>/dev/null)

							case $? in

								0 )

									if [ $MODE_TEST = "sidemenu-light-rc" ]; then

										sed -i "11s/$OPEN_BOX_LIGHT_THEME/$BOX/" $HOME/.config/darkmode-settings/config
										sed -i "s/$OPEN_BOX_THEME_XML/$BOX/" $HOME/.config/openbox/rc.xml

										openbox --reconfigure; notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

										sed -i "11s/$OPEN_BOX_LIGHT_THEME/$BOX/" $HOME/.config/darkmode-settings/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									fi

								;;

								1 )

									[ "$BOX" = $EXTRA_BUTTON_BACK ] && exec rb-dark-mode-settings.sh

								;;

								* )

							esac

					elif [ "$BOX" = "Change Openbox dark theme" ]; then

						TITLE="Dark theme chooser"
						TEXT="Choose which theme you want to use as Openbox dark theme."
						ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
						COLUMN_TITLE="Pick"

						BOX=$(ls -d /usr/share/themes/* | cut -b 19- | $ZEN_MENU --title="$TITLE" --text="$TEXT" \
						--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --column="$COLUMN_TITLE" 2>/dev/null)

							case $? in

								0 )

									if [ $MODE_TEST = "sidemenu-light-rc" ]; then

										sed -i "12s/$OPEN_BOX_DARK_THEME/$BOX/" $HOME/.config/darkmode-settings/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

										sed -i "12s/$OPEN_BOX_DARK_THEME/$BOX/" $HOME/.config/darkmode-settings/config
										sed -i "s/$OPEN_BOX_THEME_XML/$BOX/" $HOME/.config/openbox/rc.xml

										openbox --reconfigure; notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									fi

								;;

								1 )

									[ "$BOX" = $EXTRA_BUTTON_BACK ] && exec rb-dark-mode-settings.sh

								;;

								* )

							esac

					elif [ "$BOX" = "Change Rofi light theme" ]; then

						TITLE="Ligh theme chooser"
						TEXT="Choose which theme you want to use as Rofi light theme."
						ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
						COLUMN_TITLE="Pick"

						BOX=$(ls -d /usr/share/rofi/themes/* | cut -b 24- | cut -d '.' -f 1 | $ZEN_MENU --title="$TITLE" --text="$TEXT" \
						--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --column="$COLUMN_TITLE" 2>/dev/null)

							case $? in

								0 )

									if [ $MODE_TEST = "sidemenu-light-rc" ]; then

										sed -i "9s/$ROFI_LIGHT_THEME/$BOX.rasi/" $HOME/.config/darkmode-settings/config
										sed -i "1s/$ROFI_LIGHT_THEME/$BOX.rasi/" $HOME/.config/rofi/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

										sed -i "9s/$ROFI_LIGHT_THEME/$BOX.rasi/" $HOME/.config/darkmode-settings/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									fi

								;;

								1 )

									[ "$BOX" = $EXTRA_BUTTON_BACK ] && exec rb-dark-mode-settings.sh

								;;

								* )

							esac

					elif [ "$BOX" = "Change Rofi dark theme" ]; then

						TITLE="Dark theme chooser"
						TEXT="Choose which theme you want to use as Rofi dark theme."
						ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
						COLUMN_TITLE="Pick"

						BOX=$(ls -d /usr/share/rofi/themes/* | cut -b 24- | cut -d '.' -f 1 | $ZEN_MENU --title="$TITLE" --text="$TEXT" \
						--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --column="$COLUMN_TITLE" 2>/dev/null)

							case $? in

								0 )

									if [ $MODE_TEST = "sidemenu-light-rc" ]; then

										sed -i "10s/$ROFI_DARK_THEME/$BOX.rasi/" $HOME/.config/darkmode-settings/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

										sed -i "10s/$ROFI_DARK_THEME/$BOX.rasi/" $HOME/.config/darkmode-settings/config
										sed -i "1s/$ROFI_DARK_THEME/$BOX.rasi/" $HOME/.config/rofi/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									fi

								;;

								1 )

									[ "$BOX" = $EXTRA_BUTTON_BACK ] && exec rb-dark-mode-settings.sh

								;;

								* )

							esac

					elif [ "$BOX" = "Change Icons light theme" ]; then

						TITLE="Ligh theme chooser"
						TEXT="Choose which theme you want to use as Icons light theme."
						ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
						COLUMN_TITLE="Pick"

						BOX=$(ls -d /usr/share/icons/* | cut -b 18- | grep -v .png | grep -v .svg | $ZEN_MENU --title="$TITLE" --text="$TEXT" \
						--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --column="$COLUMN_TITLE" 2>/dev/null)

							case $? in

								0 )

									if [ $MODE_TEST = "sidemenu-light-rc" ]; then

										sed -i "7s/$ICONS_LIGHT_THEME/$BOX/" $HOME/.config/darkmode-settings/config
										sed -i "5s/$ICONS_LIGHT_THEME/$BOX/" $HOME/.gtkrc-2.0
										sed -i "3s/$ICONS_LIGHT_THEME/$BOX/" $HOME/.config/gtk-3.0/settings.ini
										sed -i "4s/$ICONS_LIGHT_THEME/$BOX/" $HOME/.config/qt5ct/qt5ct.conf

										xfconf-query -c xsettings -p /Net/IconThemeName -s $BOX; notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

										sed -i "7s/$ICONS_LIGHT_THEME/$BOX/" $HOME/.config/darkmode-settings/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									fi

								;;

								1 )

									[ "$BOX" = $EXTRA_BUTTON_BACK ] && exec rb-dark-mode-settings.sh

								;;

								* )

							esac

					elif [ "$BOX" = "Change Icons dark theme" ]; then

						TITLE="Dark theme chooser"
						TEXT="Choose which theme you want to use as Icons dark theme."
						ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
						COLUMN_TITLE="Pick"

						BOX=$(ls -d /usr/share/icons/* | cut -b 18- | grep -v .png | grep -v .svg | $ZEN_MENU --title="$TITLE" --text="$TEXT" \
						--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --column="$COLUMN_TITLE" 2>/dev/null)

							case $? in

								0 )

									if [ $MODE_TEST = "sidemenu-light-rc" ]; then

										sed -i "8s/$ICONS_DARK_THEME/$BOX/" $HOME/.config/darkmode-settings/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

										sed -i "8s/$ICONS_DARK_THEME/$BOX/" $HOME/.config/darkmode-settings/config
										sed -i "5s/$ICONS_DARK_THEME/$BOX/" $HOME/.gtkrc-2.0
										sed -i "3s/$ICONS_DARK_THEME/$BOX/" $HOME/.config/gtk-3.0/settings.ini
										sed -i "4s/$ICONS_DARK_THEME/$BOX/" $HOME/.config/qt5ct/qt5ct.conf

										xfconf-query -c xsettings -p /Net/IconThemeName -s $BOX; notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									fi

								;;

								1 )

									[ "$BOX" = $EXTRA_BUTTON_BACK ] && exec rb-dark-mode-settings.sh

								;;

								* )

							esac

					elif [ "$BOX" = "Change Main menu Icons light theme" ]; then

						TITLE="Ligh theme chooser"
						TEXT="Choose which theme you want to use as Main menu Icons light theme."
						ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
						COLUMN_TITLE="Pick"

						BOX=$(ls -d /usr/share/icons/* | cut -b 18- | grep -v .png | grep -v .svg | $ZEN_MENU --title="$TITLE" --text="$TEXT" \
						--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --column="$COLUMN_TITLE" 2>/dev/null)

							case $? in

								0 )

									if [ $MODE_TEST = "sidemenu-light-rc" ]; then

										sed -i "17s/$MAINMENU_ICONS_LIGHT_THEME/$BOX/" $HOME/.config/darkmode-settings/config
										sed -i "48s/$MAINMENU_ICONS_LIGHT_THEME/$BOX/" $HOME/.config/jgmenu/jgmenurc
										sed -i "48s/$MAINMENU_ICONS_LIGHT_THEME/$BOX/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

										sed -i "17s/$MAINMENU_ICONS_LIGHT_THEME/$BOX/" $HOME/.config/darkmode-settings/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									fi

								;;

								1 )

									[ "$BOX" = $EXTRA_BUTTON_BACK ] && exec rb-dark-mode-settings.sh

								;;

								* )

							esac

					elif [ "$BOX" = "Change Main menu Icons dark theme" ]; then

						TITLE="Dark theme chooser"
						TEXT="Choose which theme you want to use as Main menu Icons dark theme."
						ZEN_MENU="zenity --list --width=400 --height=458 --hide-header"
						COLUMN_TITLE="Pick"

						BOX=$(ls -d /usr/share/icons/* | cut -b 18- | grep -v .png | grep -v .svg | $ZEN_MENU --title="$TITLE" --text="$TEXT" \
						--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --column="$COLUMN_TITLE" 2>/dev/null)

							case $? in

								0 )

									if [ $MODE_TEST = "sidemenu-light-rc" ]; then

										sed -i "18s/$MAINMENU_ICONS_DARK_THEME/$BOX/" $HOME/.config/darkmode-settings/config

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

										sed -i "18s/$MAINMENU_ICONS_DARK_THEME/$BOX/" $HOME/.config/darkmode-settings/config
										sed -i "48s/$MAINMENU_ICONS_DARK_THEME/$BOX/" $HOME/.config/jgmenu/jgmenurc
										sed -i "48s/$MAINMENU_ICONS_DARK_THEME/$BOX/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

										notify-send -t 1200 "Setting up theme, please wait."; sleep 1; exec rb-dark-mode-settings.sh

									fi

								;;

								1 )

									[ "$BOX" = $EXTRA_BUTTON_BACK ] && exec rb-dark-mode-settings.sh

								;;

								* )

							esac

					else

						notify-send -t 2600 "You've not chosen any option."

					fi

				;;

				1 )

					if [ $BOX = $EXTRA_BUTTON ]; then

						xdotool key super+u; sleep 1

						sed -i "3s/$GTK_LIGHT_THEME/$GTK_LIGHT_THEME_DEF/" $HOME/.config/darkmode-settings/config
						sed -i "4s/$GTK_DARK_THEME/$GTK_DARK_THEME_DEF/" $HOME/.config/darkmode-settings/config
						sed -i "5s/$QT_LIGHT_THEME/$QT_LIGHT_THEME_DEF/" $HOME/.config/darkmode-settings/config
						sed -i "6s/$QT_DARK_THEME/$QT_DARK_THEME_DEF/" $HOME/.config/darkmode-settings/config
						sed -i "7s/$ICONS_LIGHT_THEME/$ICONS_LIGHT_THEME_DEF/" $HOME/.config/darkmode-settings/config
						sed -i "8s/$ICONS_DARK_THEME/$ICONS_DARK_THEME_DEF/" $HOME/.config/darkmode-settings/config
						sed -i "9s/$ROFI_LIGHT_THEME/$ROFI_LIGHT_THEME_DEF/" $HOME/.config/darkmode-settings/config
						sed -i "10s/$ROFI_DARK_THEME/$ROFI_DARK_THEME_DEF/" $HOME/.config/darkmode-settings/config
						sed -i "11s/$OPEN_BOX_LIGHT_THEME/$OPEN_BOX_LIGHT_THEME_DEF/" $HOME/.config/darkmode-settings/config
						sed -i "12s/$OPEN_BOX_DARK_THEME/$OPEN_BOX_DARK_THEME_DEF/" $HOME/.config/darkmode-settings/config
						sed -i "17s/$MAINMENU_ICONS_LIGHT_THEME/$MAINMENU_ICONS_LIGHT_THEME_DEF/" $HOME/.config/darkmode-settings/config
						sed -i "18s/$MAINMENU_ICONS_DARK_THEME/$MAINMENU_ICONS_DARK_THEME_DEF/" $HOME/.config/darkmode-settings/config

						if [ $MODE_TEST = "sidemenu-light-rc" ]; then

							sed -i "4s/$GTK_LIGHT_THEME/$GTK_LIGHT_THEME_DEF/" $HOME/.gtkrc-2.0
							sed -i "5s/$ICONS_LIGHT_THEME/$ICONS_LIGHT_THEME_DEF/" $HOME/.gtkrc-2.0
							sed -i "2s/$GTK_LIGHT_THEME/$GTK_LIGHT_THEME_DEF/" $HOME/.config/gtk-3.0/settings.ini
							sed -i "3s/$ICONS_LIGHT_THEME/$ICONS_LIGHT_THEME_DEF/" $HOME/.config/gtk-3.0/settings.ini
							sed -i "2s/$QT_LIGHT_THEME/$QT_LIGHT_THEME_DEF/" $HOME/.config/Kvantum/kvantum.kvconfig
							sed -i "4s/$ICONS_LIGHT_THEME/$ICONS_LIGHT_THEME_DEF/" $HOME/.config/qt5ct/qt5ct.conf
							sed -i "1s/$ROFI_LIGHT_THEME/$ROFI_LIGHT_THEME_DEF/" $HOME/.config/rofi/config
							sed -i "s/$OPEN_BOX_THEME_XML/$OPEN_BOX_LIGHT_THEME_DEF/" $HOME/.config/openbox/rc.xml
							sed -i "48s/$MAINMENU_ICONS_LIGHT_THEME/$MAINMENU_ICONS_LIGHT_THEME_DEF/" $HOME/.config/jgmenu/jgmenurc
							sed -i "48s/$MAINMENU_ICONS_LIGHT_THEME/$MAINMENU_ICONS_LIGHT_THEME_DEF/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu
							sed -i "48s/$MAINMENU_ICONS_DARK_THEME/$MAINMENU_ICONS_DARK_THEME_DEF/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

						elif [ $MODE_TEST = "sidemenu-dark-rc" ]; then

							sed -i "4s/$GTK_DARK_THEME/$GTK_DARK_THEME_DEF/" $HOME/.gtkrc-2.0
							sed -i "5s/$ICONS_DARK_THEME/$ICONS_DARK_THEME_DEF/" $HOME/.gtkrc-2.0
							sed -i "2s/$GTK_DARK_THEME/$GTK_DARK_THEME_DEF/" $HOME/.config/gtk-3.0/settings.ini
							sed -i "3s/$ICONS_DARK_THEME/$ICONS_DARK_THEME_DEF/" $HOME/.config/gtk-3.0/settings.ini
							sed -i "2s/$QT_DARK_THEME/$QT_DARK_THEME_DEF/" $HOME/.config/Kvantum/kvantum.kvconfig
							sed -i "4s/$ICONS_DARK_THEME/$ICONS_DARK_THEME_DEF/" $HOME/.config/qt5ct/qt5ct.conf
							sed -i "1s/$ROFI_DARK_THEME/$ROFI_DARK_THEME_DEF/" $HOME/.config/rofi/config
							sed -i "s/$OPEN_BOX_THEME_XML/$OPEN_BOX_DARK_THEME_DEF/" $HOME/.config/openbox/rc.xml
							sed -i "48s/$MAINMENU_ICONS_DARK_THEME/$MAINMENU_ICONS_DARK_THEME_DEF/" $HOME/.config/jgmenu/jgmenurc
							sed -i "48s/$MAINMENU_ICONS_LIGHT_THEME/$MAINMENU_ICONS_LIGHT_THEME_DEF/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu
							sed -i "48s/$MAINMENU_ICONS_DARK_THEME/$MAINMENU_ICONS_DARK_THEME_DEF/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

						fi

						openbox --reconfigure
						sleep 0.5
						xfconf-query -c xsettings -p /Net/ThemeName -s Matcha-light-sea
						sleep 0.5
						xfconf-query -c xsettings -p /Net/IconThemeName -s Flat-Remix-Green-Light
						xdotool key super+u
						notify-send -t 2300 "Default settings restored."
						exec rb-dark-mode-settings.sh

					fi

				;;

				* )

			esac
