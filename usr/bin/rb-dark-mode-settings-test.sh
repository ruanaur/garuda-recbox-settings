#!/usr/bin/sh

GTK_THEME=$(grep 'gtk-theme-name' $HOME/.config/gtk-3.0/settings.ini | cut -c 16-)
GTK_LIGHT_THEME=$(grep 'GTK_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
GTK_DARK_THEME=$(grep 'GTK_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
QT_THEME=$(grep 'theme=' $HOME/.config/Kvantum/kvantum.kvconfig | cut -c 7-)
QT_LIGHT_THEME=$(grep 'QT_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
QT_DARK_THEME=$(grep 'QT_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
ICONS_THEME=$(grep 'gtk-icon-theme-name' $HOME/.config/gtk-3.0/settings.ini | cut -c 21-)
ICONS_LIGHT_THEME=$(grep 'ICONS_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
ICONS_DARK_THEME=$(grep 'ICONS_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
ROFI_THEME=$(grep 'rofi.theme:' $HOME/.config/rofi/config | cut -c 36-)
ROFI_LIGHT_THEME=$(grep 'ROFI_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
ROFI_DARK_THEME=$(grep 'ROFI_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
OBOX_THEME=$(grep 'THEME_NAME' $HOME/.config/openbox/rc.xml | sed -e 's|<name>\(.*\)</name> <!-- THEME_NAME -->|\1|' | sed 's/  *//')
OBOX_LIGHT_THEME=$(grep 'OBOX_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
OBOX_DARK_THEME=$(grep 'OBOX_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
MMENU_ICONS_THEME=$(grep 'icon_theme =' $HOME/.config/jgmenu/jgmenurc | cut -c 14-)
MMENU_ICONS_LIGHT_THEME=$(grep 'MAINMENU_ICONS_LIGHT_THEME=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
MMENU_ICONS_DARK_THEME=$(grep 'MAINMENU_ICONS_DARK_THEME=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)

MODE_TEST=$(grep SIDEMENU_RC $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)

	if [ $MODE_TEST = "sidemenu-dark-rc" ]; then

		sed -i "4s/$GTK_DARK_THEME/$GTK_THEME/" $HOME/.config/darkmode-settings/config
		sed -i "6s/$QT_DARK_THEME/$QT_THEME/" $HOME/.config/darkmode-settings/config
		sed -i "8s/$ICONS_DARK_THEME/$ICONS_THEME/" $HOME/.config/darkmode-settings/config
		sed -i "10s/$ROFI_DARK_THEME/$ROFI_THEME/" $HOME/.config/darkmode-settings/config
		sed -i "12s/$OBOX_DARK_THEME/$OBOX_THEME/" $HOME/.config/darkmode-settings/config
		sed -i "18s/$MMENU_ICONS_DARK_THEME/$MMENU_ICONS_THEME/" $HOME/.config/darkmode-settings/config

		sleep 0.5; exec rb-dark-mode-settings.sh

	elif [ $MODE_TEST = "sidemenu-light-rc" ]; then

		sed -i "3s/$GTK_LIGHT_THEME/$GTK_THEME/" $HOME/.config/darkmode-settings/config
		sed -i "5s/$QT_LIGHT_THEME/$QT_THEME/" $HOME/.config/darkmode-settings/config
		sed -i "7s/$ICONS_LIGHT_THEME/$ICONS_THEME/" $HOME/.config/darkmode-settings/config
		sed -i "9s/$ROFI_LIGHT_THEME/$ROFI_THEME/" $HOME/.config/darkmode-settings/config
		sed -i "11s/$OBOX_LIGHT_THEME/$OBOX_THEME/" $HOME/.config/darkmode-settings/config
		sed -i "17s/$MMENU_ICONS_LIGHT_THEME/$MMENU_ICONS_THEME/" $HOME/.config/darkmode-settings/config

		sleep 0.5; exec rb-dark-mode-settings.sh

fi
