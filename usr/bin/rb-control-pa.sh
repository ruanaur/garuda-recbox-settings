#!/usr/bin/sh

# VERSION=1.8

export RED='\033[0;31m'
export GREEN='\033[0;32m'
export YELLOW='\033[0;33m'
export WHITE='\033[0;37m'
export RESETCOLOR='\033[1;00m'

#  Credits:
#  https://stackoverflow.com/questions/48164937/notify-send-volume-popup-replacing-itself#48171366
#  https://unix.stackexchange.com/questions/257490/writing-output-of-xbacklight-get-to-a-variable-in-a-bash-script
#  https://konradstrack.ninja/blog/volume-change-notifications-in-openbox/

vol_box() {

SCALEBOX_TEST=$(ps -x | grep 'ScaleBox' | grep -v 'grep' | awk '{print $12}' | cut -d '=' -f 2)

dunstctl close-all

TITLE="Volume ScaleBox"
TEXT="<b>Volume Level</b>"
MIC="Mic"
BOX="zenity --scale --cancel-label="Close" --ok-label="Apply""
VOL=$(amixer -D pulse sget Master | grep 'Left:' | awk -F'[][]' '{ print $2 }')
VOL=${VOL::-1}

	if [ "$SCALEBOX_TEST" = "ScaleBox" ]; then

		kill 15 $(ps -x | grep ScaleBox | grep -v grep | awk '{print $1}')

			else

				VOL=$($BOX --text="$TEXT" --title="$TITLE" --extra-button="$MIC" --value="${VOL}" --step="1"); pactl set-sink-volume 0 ${VOL}%

					[ "$VOL" = "$MIC" ] && exec rb-control.sh mic_box

	fi 2>/dev/null
}

vol_up() {

	pamixer -i 6 && notify-send "Volume Level " -t 1500 -h int:value:$(printf %s $(amixer -D pulse sget 'Master' | grep 'Left:' | awk -F'[][]' '{ print $2 }')) -i /usr/share/icons/Flat-Remix-Green/panel/audio-volume-medium.svg

}

vol_down() {

	pamixer -d 6 && notify-send "Volume Level " -t 1500 -h int:value:$(printf %s $(amixer -D pulse sget 'Master' | grep 'Left:' | awk -F'[][]' '{ print $2 }')) -i /usr/share/icons/Flat-Remix-Green/panel/audio-volume-medium.svg

}

vol_mute() {

	pactl set-sink-mute 0 toggle; notify-send -t 2300 "Volume mute toggle" -i /usr/share/icons/Flat-Remix-Green/panel/audio-volume-muted.svg

    if ! echo "on" | amixer scontents | grep Mono | grep off; then

    	notify-send -t 2300 "Volume unmuted" -i /usr/share/icons/Flat-Remix-Green/panel/audio-volume-high.svg

		    else

				notify-send -t 2300 "Volume muted" -i /usr/share/icons/Flat-Remix-Green/panel/audio-volume-low.svg

    fi

}

mic_box() {

TITLE="Microphone ScaleBox"
TEXT="<b>Microphone Level</b>"
MIC_TEST=$(amixer -D pulse sget 'Capture' | grep 'Left:' | awk -F'[][]' '{ print $4 }' | sed -e "s/\b\(.\)/\u\1/")
MENU="zenity --scale --cancel-label="Close" --ok-label="Apply""
MIC=$(amixer -D pulse sget 'Capture' | grep 'Left:' | awk -F'[][]' '{ print $2 }')
MIC=${MIC::-1}

EXTRA_BUTTON=$(

	if [[ $MIC_TEST = "on" ]]; then

		echo "Turn Off"

			else

				echo "Turn On"

	fi

)

	MIC=$($MENU --text="$TEXT" --title="$TITLE" --extra-button="$EXTRA_BUTTON" --value="${MIC}" --step="1" 2>/dev/null); pactl set-source-volume 1 ${MIC}%

		[ "$MIC" = "$EXTRA_BUTTON" ] && pactl set-source-mute 1 toggle

}

micro_mute() {

	pactl set-source-mute 1 toggle | notify-send -t 2300 "Microphone mute toggle" -i /usr/share/icons/Flat-Remix-Green/panel/audio-input-microphone-muted.svg

		if ! echo "on" | amixer scontents | grep Capture | grep -v 'channels' | grep -v 'Items' | grep -v 'Limits' | grep -v 'mixer' | grep off; then

			notify-send -t 1500 "Microphone unmuted" -i /usr/share/icons/Flat-Remix-Green/panel/audio-input-microphone-symbolic.svg

				else

					notify-send -t 1500 "Microphone muted" -i /usr/share/icons/Flat-Remix-Green/panel/audio-input-microphone-muted.svg

		fi

}

backlight_box() {

SCALEBOX_TEST=$(ps -x | grep 'ScaleBox' | grep -v 'grep' | awk '{print $12}' | cut -d '=' -f 2)

dunstctl close-all

TITLE="Brightness ScaleBox"
TEXT="<b>Brightness Level</b>"
EXTRA_BUTTON="Turn Off"
BOX="zenity --scale --cancel-label="Close" --ok-label="Apply""
BRGH=$(xbacklight -get)

	if [ "$SCALEBOX_TEST" = "ScaleBox" ]; then

		kill 15 $(ps -x | grep ScaleBox | grep -v grep | awk '{print $1}')

			else

				BRGH_BOX=$($BOX --text="$TEXT" --title="$TITLE" --extra-button="$EXTRA_BUTTON" --value="${BRGH%.*}" --step="1" --min-value="1"); xbacklight -set $BRGH_BOX

					[ "$BRGH_BOX" = "$EXTRA_BUTTON" ] && xdotool key super+u && sleep 2 && xset dpms force off

	fi  2>/dev/null

}

backlight_up() {

	xbacklight +6; brgh=$(xbacklight -get) && notify-send "Brightness Level " -t 1500 -h int:value:${brgh%.*} -i /usr/share/icons/Flat-Remix-Green/panel/xfpm-brightness-lcd.svg

}

backlight_down() {

	xbacklight -6; brgh=$(xbacklight -get) && notify-send "Brightness Level " -t 1500 -h int:value:${brgh%.*} -i /usr/share/icons/Flat-Remix-Green/panel/xfpm-brightness-lcd.svg

}

backlight_toggle() {

	if [ $(xbacklight -get |  cut -d '.' -f 1) -ge 50 ]; then

		xbacklight -set 5

	elif [ $(xbacklight -get |  cut -d '.' -f 1) -lt 50 ]; then

		xbacklight -set 81

	fi

}

#  Credits:
#  https://askubuntu.com/questions/69556/how-do-i-check-the-batterys-status-via-the-terminal
#  https://www.ostechnix.com/how-to-check-laptop-battery-status-in-terminal-in-linux/
#  https://askubuntu.com/questions/518928/how-to-write-a-script-to-listen-to-battery-status-and-alert-me-when-its-above

power_notification() {

	notify-send -t 5000 "$(upower -i $(upower -e | grep BAT) | grep --color=never -E "tate|to\ full|to\ empty|percentage")"

}

power_info() {

	echo -e ":: Battery Info\n"; upower -i $(upower -e | grep BAT);	echo ""; read -p ":: Press enter to exit"

}

battery_measure_test() {

	if [ "battery_measure_on" = $(ps -x | grep battery_measure_on | grep -v grep | awk '{print $7}') ] ; then

		killall -q rb-control.sh battery_measure_on & notify-send -t 2300 "Battery monitoring disabled"

			else

				exec rb-control.sh battery_measure_on

	fi 2>/dev/null

}

battery_measure_on() {

	notify-send -t 2300 "Starting battery monitor." && sleep 3

	while true

		do
			BATTERY_LEVEL=$(acpi -b | grep -P -o '[0-9]+(?=%)')

				if [ $BATTERY_LEVEL -ge 60 ]; then
	
					notify-send -t 2300 "Battery level: ${BATTERY_LEVEL}%" -i /usr/share/icons/Flat-Remix-Green/panel/battery-060.svg
	
				elif [ $BATTERY_LEVEL -le 40 ]; then
	
					notify-send -t 2300 "Battery level: ${BATTERY_LEVEL}%" -i /usr/share/icons/Flat-Remix-Green/panel/battery-040.svg
	
				elif [ $BATTERY_LEVEL -le 7 ]; then
	
					notify-send "Battery almost discharged ${BATTERY_LEVEL}%" "System will be suspended after 30 seconds." -i /usr/share/icons/Flat-Remix-Green/panel/battery-caution.svg && sleep 30
	
					[[ $battery_level -le 7 ]] && killall -q rb-control.sh battery_measure_on & betterlockscreen -s
	
				fi

		sleep 300

	done
}

date_notify() {

	notify-send -t 3000 $(date +'%A,%d-%m-%Y')

}

ver() {

	echo -e "\n    ${WHITE}Version 1.8${RESETCOLOR}"

}

version() {

	echo -e "\n    ${WHITE}Version 1.8${RESETCOLOR}"

}

case "$1" in
	vol_box) vol_box;;
	vol_up) vol_up;;
	vol_down) vol_down;;
	vol_mute) vol_mute;;
	mic_box) mic_box;;
	micro_mute) micro_mute;;
	backlight_box) backlight_box;;
	backlight_up) backlight_up;;
	backlight_down) backlight_down;;
	backlight_toggle) backlight_toggle;;
	power_notification) power_notification;;
	power_info) power_info;;
	battery_measure_test) battery_measure_test;;
	battery_measure_on) battery_measure_on;;
	date_notify) date_notify;;
	ver) ver;;
	version) version;;
	*)

echo -e "
${GREEN}\n	--------------------------------------------------------------------------
					RecBox Control
	--------------------------------------------------------------------------\n
	Made by Projekt:Root projekt.root@tuta.io
	for RecBox and Garuda OpenBox.
	If you want to use script on other setup 
	probably you'll need to modify it.\n
	--------------------------------------------------------------------------${RESETCOLOR}

	Usage:
	rb-control.sh ${YELLOW}[${RED} OPTION${YELLOW} ]

${GREEN}	Options:

${YELLOW}	>${RED} vol_box ${YELLOW}              -${RESETCOLOR} execute zenity widget
${YELLOW}	>${RED} vol_up ${YELLOW}               -${RESETCOLOR} increase volume
${YELLOW}	>${RED} vol_down ${YELLOW}             -${RESETCOLOR} decrease volume
${YELLOW}	>${RED} mic_box ${YELLOW}              -${RESETCOLOR} execute zenity widget
${YELLOW}	>${RED} vol_mute ${YELLOW}             -${RESETCOLOR} mute volume toggle
${YELLOW}	>${RED} micro_mute ${YELLOW}           -${RESETCOLOR} microphone mute
${YELLOW}	>${RED} backlight_box ${YELLOW}        -${RESETCOLOR} execute zenity widget
${YELLOW}	>${RED} backlight_up ${YELLOW}         -${RESETCOLOR} increase brightness
${YELLOW}	>${RED} backlight_down ${YELLOW}       -${RESETCOLOR} decrease brightness
${YELLOW}	>${RED} backlight_toggle ${YELLOW}     -${RESETCOLOR} brightness toggle
${YELLOW}	>${RED} power_notification ${YELLOW}   -${RESETCOLOR} execute battery notification
${YELLOW}	>${RED} power_info ${YELLOW}           -${RESETCOLOR} execute rofi with power info
${YELLOW}	>${RED} battery_measure_on ${YELLOW}   -${RESETCOLOR} enable battery monitoring 
${YELLOW}	>${RED} battery_measure_test ${YELLOW} -${RESETCOLOR} enable/disable battery monitoring 
${YELLOW}	>${RED} date_notify ${YELLOW}          -${RESETCOLOR} execute date notitication

${YELLOW}	>${RED} ver, version${YELLOW}          -${RESETCOLOR} show script version
" >&2
exit 1
;;
esac

echo -e $RESETCOLOR
exit 0
