#!/usr/bin/sh

# VERSION=2.6

export RED='\033[0;31m'
export GREEN='\033[0;32m'
export YELLOW='\033[0;33m'
export BLUE='\033[0;34m'
export WHITE='\033[0;37m'
export RESETCOLOR='\033[1;00m'

echo ":: Please wait..."

DAILY_CUSTOM_JACK_CHECK=$(grep "DAILY_JACK_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_CADENCE_CHECK=$(grep "DAILY_CADENCE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_ARANDR_CHECK=$(grep "DAILY_ARANDR_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_XFSETTINGSD_CHECK=$(grep "DAILY_XFSETTINGSD_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_COMPOSITOR_CHECK=$(grep "DAILY_COMPOSITOR_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_THUNAR_DAEMON_CHECK=$(grep "DAILY_THUNAR_DAEMON_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_PCMANFM_CHECK=$(grep 'DAILY_PCMANFM_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_NITROGEN_CHECK=$(grep "DAILY_NITROGEN_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_TINT2_PANEL_CHECK=$(grep "DAILY_TINT2_PANEL_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_POLYBAR_PANEL_CHECK=$(grep "DAILY_POLYBAR_PANEL_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_CONKY_CHECK=$(grep "DAILY_CONKY_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_VOLUMEICON_CHECK=$(grep "DAILY_VOLUMEICON_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_SKIPPY_DAEMON_CHECK=$(grep "DAILY_SKIPPY_DAEMON_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_SUPERKEY_CHECK=$(grep 'DAILY_SUPERKEY_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_POLICYKIT_CHECK=$(grep "DAILY_POLICYKIT_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_SIDE_SHORTCUTS_CHECK=$(grep "DAILY_SIDE_SHORTCUTS_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_NUMLOCK_CHECK=$(grep "DAILY_NUMLOCK_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_OBANIMATIONS_CHECK=$(grep "DAILY_OBANIMATIONS_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_COMPOSITOR_SHADOW_CHECK=$(grep "DAILY_COMPOSITOR_SHADOW_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_ALSA_CONFIGS_CHECK=$(grep "DAILY_ALSA_CONFIGS_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_CPUPOWER_CONFIGS_CHECK=$(grep "DAILY_CPUPOWER_CONFIGS_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_TLP_CONFIGS_CHECK=$(grep "DAILY_TLP_CONFIGS_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_CPU_SERVICE_CHECK=$(grep "DAILY_CPU_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_RTC_SERVICE_CHECK=$(grep "DAILY_RTC_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_HPET_SERVICE_CHECK=$(grep "DAILY_HPET_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_UFW_SERVICE_CHECK=$(grep "DAILY_UFW_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_FIREWALLD_SERVICE_CHECK=$(grep "DAILY_FIREWALLD_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_GRUB_CONFIGS_CHECK=$(grep "DAILY_GRUB_CONFIGS_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)

STUDIO_CUSTOM_JACK_CHECK=$(grep "STUDIO_JACK_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_CADENCE_CHECK=$(grep 'STUDIO_CADENCE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_ARANDR_CHECK=$(grep 'STUDIO_ARANDR_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_XFSETTINGSD_CHECK=$(grep 'STUDIO_XFSETTINGSD_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_COMPOSITOR_CHECK=$(grep 'STUDIO_COMPOSITOR_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_THUNAR_DAEMON_CHECK=$(grep 'STUDIO_THUNAR_DAEMON_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_PCMANFM_CHECK=$(grep 'STUDIO_PCMANFM_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_NITROGEN_CHECK=$(grep 'STUDIO_NITROGEN_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_TINT2_PANEL_CHECK=$(grep 'STUDIO_TINT2_PANEL_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_POLYBAR_PANEL_CHECK=$(grep 'STUDIO_POLYBAR_PANEL_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_CONKY_CHECK=$(grep 'STUDIO_CONKY_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_VOLUMEICON_CHECK=$(grep 'STUDIO_VOLUMEICON_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_SKIPPY_DAEMON_CHECK=$(grep 'STUDIO_SKIPPY_DAEMON_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_SUPERKEY_CHECK=$(grep 'STUDIO_SUPERKEY_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_POLICYKIT_CHECK=$(grep 'STUDIO_POLICYKIT_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_SIDE_SHORTCUTS_CHECK=$(grep 'STUDIO_SIDE_SHORTCUTS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_NUMLOCK_CHECK=$(grep 'STUDIO_NUMLOCK_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_OBANIMATIONS_CHECK=$(grep 'STUDIO_OBANIMATIONS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_COMPOSITOR_SHADOW_CHECK=$(grep 'STUDIO_COMPOSITOR_SHADOW_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_ALSA_CONFIGS_CHECK=$(grep 'STUDIO_ALSA_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_CPUPOWER_CONFIGS_CHECK=$(grep 'STUDIO_CPUPOWER_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_TLP_CONFIGS_CHECK=$(grep 'STUDIO_TLP_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_CPU_SERVICE_CHECK=$(grep 'STUDIO_CPU_SERVICE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_RTC_SERVICE_CHECK=$(grep 'STUDIO_RTC_SERVICE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_HPET_SERVICE_CHECK=$(grep 'STUDIO_HPET_SERVICE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_UFW_SERVICE_CHECK=$(grep "STUDIO_UFW_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_FIREWALLD_SERVICE_CHECK=$(grep "STUDIO_FIREWALLD_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_GRUB_CONFIGS_CHECK=$(grep 'STUDIO_GRUB_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)

ALTERNATIVE_CUSTOM_JACK_CHECK=$(grep "ALTERNATIVE_JACK_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_CADENCE_CHECK=$(grep 'ALTERNATIVE_CADENCE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_ARANDR_CHECK=$(grep 'ALTERNATIVE_ARANDR_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_XFSETTINGSD_CHECK=$(grep 'ALTERNATIVE_XFSETTINGSD_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_COMPOSITOR_CHECK=$(grep 'ALTERNATIVE_COMPOSITOR_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_THUNAR_DAEMON_CHECK=$(grep 'ALTERNATIVE_THUNAR_DAEMON_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_PCMANFM_CHECK=$(grep 'ALTERNATIVE_PCMANFM_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_NITROGEN_CHECK=$(grep 'ALTERNATIVE_NITROGEN_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_TINT2_PANEL_CHECK=$(grep 'ALTERNATIVE_TINT2_PANEL_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_POLYBAR_PANEL_CHECK=$(grep 'ALTERNATIVE_POLYBAR_PANEL_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_CONKY_CHECK=$(grep 'ALTERNATIVE_CONKY_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_VOLUMEICON_CHECK=$(grep 'ALTERNATIVE_VOLUMEICON_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_SKIPPY_DAEMON_CHECK=$(grep 'ALTERNATIVE_SKIPPY_DAEMON_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_SUPERKEY_CHECK=$(grep 'ALTERNATIVE_SUPERKEY_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_POLICYKIT_CHECK=$(grep 'ALTERNATIVE_POLICYKIT_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_SIDE_SHORTCUTS_CHECK=$(grep 'ALTERNATIVE_SIDE_SHORTCUTS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_NUMLOCK_CHECK=$(grep 'ALTERNATIVE_NUMLOCK_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_OBANIMATIONS_CHECK=$(grep 'ALTERNATIVE_OBANIMATIONS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_COMPOSITOR_SHADOW_CHECK=$(grep 'ALTERNATIVE_COMPOSITOR_SHADOW_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_ALSA_CONFIGS_CHECK=$(grep 'ALTERNATIVE_ALSA_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_CPUPOWER_CONFIGS_CHECK=$(grep 'ALTERNATIVE_CPUPOWER_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_TLP_CONFIGS_CHECK=$(grep 'ALTERNATIVE_TLP_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_CPU_SERVICE_CHECK=$(grep 'ALTERNATIVE_CPU_SERVICE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_RTC_SERVICE_CHECK=$(grep 'ALTERNATIVE_RTC_SERVICE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_HPET_SERVICE_CHECK=$(grep 'ALTERNATIVE_HPET_SERVICE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_UFW_SERVICE_CHECK=$(grep "ALTERNATIVE_UFW_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_FIREWALLD_SERVICE_CHECK=$(grep "ALTERNATIVE_FIREWALLD_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_GRUB_CONFIGS_CHECK=$(grep 'ALTERNATIVE_GRUB_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)

TLP_DAILY_CONFIG_FILE=$(ls $HOME/.config/openbox/obconfigs/ | grep -wo "tlp.daily")
CPUPOWER_DAILY_CONFIG_FILE=$(ls $HOME/.config/openbox/obconfigs/ | grep -wo "cpupower.daily")
GRUB_DAILY_CONFIG_FILE=$(ls $HOME/.config/openbox/obconfigs/ | grep -wo "grub.daily")

TLP_STUDIO_CONFIG_FILE=$(ls $HOME/.config/openbox/obconfigs/ | grep -wo "tlp.studio")
CPUPOWER_STUDIO_CONFIG_FILE=$(ls $HOME/.config/openbox/obconfigs/ | grep -wo "cpupower.studio")
GRUB_STUDIO_CONFIG_FILE=$(ls $HOME/.config/openbox/obconfigs/ | grep -wo "grub.studio")

CPUPOWER_SERVICE=$(pamac search cpupower -i | grep -wo "cpupower")
TLP_SERVICE=$(pamac search tlp -i | grep -wo "tlp")
FIREWALLD_SERVICE=$(pamac search firewalld -i | grep -wo "firewalld")
UFW_SERVICE=$(pamac search ufw -i | grep -wo "ufw")

CPUPOWER_SERVICE_STATUS=$(systemctl status cpupower.service 2>/dev/null | grep Active | awk '{print $2}')
RTC_SERVICE_STATUS=$(head -1 /sys/class/rtc/rtc0/max_user_freq)
HPET_SERVICE_STATUS=$(head -1 /proc/sys/dev/hpet/max-user-freq)
UFW_SERVICE_STATUS=$(systemctl status ufw.service 2>/dev/null | grep Active | awk '{print $2}')
FIREWALLD_SERVICE_STATUS=$(systemctl status firewalld.service 2>/dev/null | grep Active | awk '{print $2}')

MODE_STATUS=$(grep DARK_MODE_STATUS $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)

autostart_default_config() {

cat <<'EOF' > $HOME/.config/openbox/autostart
### To lern about all options here, go to How RecBox works.pdf

#start_jack.sh &

#cadence-session-start -s &

#~/.screenlayout/dual-head.sh &

sleep 0.5; xfsettingsd &

sleep 1; garuda-compositor --start &

thunar --daemon &

#pcmanfm --desktop &

sleep 1; nitrogen --restore &

sleep 2; garuda-tint2-session &

#sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &

#garuda-conky-session &

#sleep 1; volumeicon &

skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &

#xcape -e 'Super_L=Alt_L|F1' &

/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

#xdg-user-dirs-gtk-update &

#numlockx &

EOF

}

hperf() {
	echo -e "\n${BLUE}[${YELLOW} i${BLUE} ]${YELLOW} I'm closing the unnecessary software.\n"
	sleep 1
	killall -q redshift
	echo -e "${YELLOW}> RedShift$RED         Turned off"
	sleep 1
	killall -q compton
	echo -e "${YELLOW}> Compton$RED          Turned off"
	sleep 1
	killall -q /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
	echo -e "${YELLOW}> PolicyKit$RED        Turned off"
	sleep 1
	killall -q xfsettingsd
	echo -e "${YELLOW}> XFCE settings$RED    Turned off"
	sleep 1
	killall -q thunar
	echo -e "${YELLOW}> Thunar daemon$RED    Turned off"
	sleep 1
	killall -q skippy-xd 
	echo -e "${YELLOW}> skippy-xd daemon$RED Turned off"
	sleep 1
	echo -e "$BLUE \nDone!"
	sleep 4
}

daily() {

	if [ $MODE_STATUS = "OFF" ]; then

		cp ~/.config/tint2/recbox-daily-light.tint2rc ~/.config/tint2/tint2rc; garuda-tint2restart

	elif [ $MODE_STATUS = "ON" ]; then

		cp ~/.config/tint2/recbox-daily-dark.tint2rc ~/.config/tint2/tint2rc; garuda-tint2restart

	fi  2>/dev/null

	sed -i '5s/Studio/Daily/' ~/.config/workflow-settings/session-test-config

	echo -e "\n${WHITE}Setting up ${GREEN}Daily Mode${RESETCOLOR}.\n"

	echo -e "${GREEN}==>${RESETCOLOR} checking autostart file content\n"

		if echo "start_jack.sh &" | grep -q "start_jack.sh &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] start_jack.sh"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] start_jack.sh"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "cadence-session-start -s &" | grep -q "cadence-session-start -s &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] cadence-session-start -s"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] cadence-session-start -s"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "~/.screenlayout/dual-head.sh &" | grep -q "~/.screenlayout/dual-head.sh &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] ~/.screenlayout/dual-head.sh"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] ~/.screenlayout/dual-head.sh"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 0.5; xfsettingsd &" | grep -q "sleep 0.5; xfsettingsd &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] xfsettingsd"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] xfsettingsd"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 1; garuda-compositor --start &" | grep -q "sleep 1; garuda-compositor --start &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] garuda-compositor --start"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] garuda-compositor --start"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "thunar --daemon &" | grep -q "thunar --daemon &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] thunar --daemon"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] thunar --daemon"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "pcmanfm --desktop &" | grep -q "pcmanfm --desktop &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] pcmanfm --desktop"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] pcmanfm --desktop"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 1; nitrogen --restore &" | grep -q "sleep 1; nitrogen --restore &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] nitrogen --restore"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] nitrogen --restore"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 2; garuda-tint2-session &" | grep -q "sleep 2; garuda-tint2-session &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] garuda-tint2-session"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] garuda-tint2-session"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &" | grep -q 'sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &' $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] polybar -c $HOME/.config/polybar/config openbox-bar"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] polybar -c $HOME/.config/polybar/config openbox-bar"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "garuda-conky-session &" | grep -q "garuda-conky-session &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] garuda-conky-session"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] garuda-conky-session"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 1; volumeicon &" | grep -q "sleep 1; volumeicon &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] volumeicon"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] volumeicon"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &" | grep -q "skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "xcape -e 'Super_L=Alt_L|F1' &" | grep -q "xcape -e 'Super_L=Alt_L|F1' &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] xcape -e 'Super_L=Alt_L|F1'"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] xcape -e 'Super_L=Alt_L|F1'"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1" | grep -q '/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1' $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &" | grep -q "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] xdg-user-dirs-gtk-update"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] xdg-user-dirs-gtk-update"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "numlockx &" | grep -q "numlockx &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] numlockx"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] numlockx"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

	echo -e "\n${GREEN}==>${RESETCOLOR} autostart file configuration\n"

	if [ "$DAILY_CUSTOM_JACK_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling custom jack script"

			if echo "start_jack.sh" | grep -q '#start_jack.sh' $HOME/.config/openbox/autostart; then

				sed -i 's/#start_jack.sh/start_jack.sh/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} start_jack.sh &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} custom jack script already enabled\n"

			fi

	elif [ "$DAILY_CUSTOM_JACK_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling custom jack script"

			if echo "#start_jack.sh" | grep -q '#start_jack.sh' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} custom jack script already disabled\n"
	
					else

						sed -i 's/start_jack.sh/#start_jack.sh/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #start_jack.sh &\n"

			fi

	fi

	if [ "$DAILY_CADENCE_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling cadence-session-start"

			if echo "cadence-session-start" | grep -q '#cadence-session-start' $HOME/.config/openbox/autostart; then

				sed -i 's/#cadence-session-start/cadence-session-start/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} cadence-session-start -s &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} cadence-session-start already enabled\n"

			fi

	elif [ "$DAILY_CADENCE_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling cadence-session-start"

			if echo "#cadence-session-start" | grep -q '#cadence-session-start' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} cadence-session-start already disabled\n"
	
					else

						sed -i 's/cadence-session-start/#cadence-session-start/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #cadence-session-start -s &\n"

			fi

	fi

	if [ "$DAILY_ARANDR_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling dual-head script (required when xfsettingsd not working)"

			if echo "~/.screenlayout/dual-head.sh" | grep -q '#~/.screenlayout/dual-head.sh' $HOME/.config/openbox/autostart; then

				sed -i 's|#~/.screenlayout|~/.screenlayout|' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} ~/.screenlayout/dual-head.sh &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} dual-head script already enabled\n"

			fi

	elif [ "$DAILY_ARANDR_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling dual-head script (required when xfsettingsd not working)"

			if echo "#~/.screenlayout/dual-head.sh" | grep -q '#~/.screenlayout/dual-head.sh' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} dual-head script already disabled\n"
	
					else

						sed -i 's|~/.screenlayout|#~/.screenlayout|' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #~/.screenlayout/dual-head.sh &\n"

			fi

	fi

	if [ "$DAILY_XFSETTINGSD_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling xfsettingsd (required for xfce settings to work)"

			if echo "sleep 0.5; xfsettingsd" | grep -q '#sleep 0.5; xfsettingsd' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 0.5; xfsettingsd/sleep 0.5; xfsettingsd/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 0.5; xfsettingsd &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} xfsettingsd already enabled\n"

			fi

	elif [ "$DAILY_XFSETTINGSD_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling xfsettingsd (required for xfce settings to work)"

			if echo "#sleep 0.5; xfsettingsd" | grep -q '#sleep 0.5; xfsettingsd' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} xfsettingsd already disabled\n"
	
					else

						sed -i 's/sleep 0.5; xfsettingsd/#sleep 0.5; xfsettingsd/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 0.5; xfsettingsd &\n"

			fi

	fi

	if [ "$DAILY_COMPOSITOR_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Garuda compositor"

			if echo "sleep 1; garuda-compositor" | grep -q '#sleep 1; garuda-compositor' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 1; garuda-compositor/sleep 1; garuda-compositor/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 1; garuda-compositor --start &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Garuda compositor already enabled\n"

			fi

	elif [ "$DAILY_COMPOSITOR_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Garuda compositor"

			if echo "#sleep 1; garuda-compositor" | grep -q '#sleep 1; garuda-compositor' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Garuda compositor already disabled\n"
	
					else

						sed -i 's/sleep 1; garuda-compositor/#sleep 1; garuda-compositor/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 1; garuda-compositor --start &\n"

			fi

	fi

	if [ "$DAILY_THUNAR_DAEMON_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Thunar in daemon mode"

			if echo "thunar --daemon" | grep -q '#thunar --daemon' $HOME/.config/openbox/autostart; then

				sed -i 's/#thunar --daemon/thunar --daemon/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} thunar --daemon &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Thunar daemon already enabled\n"

			fi

	elif [ "$DAILY_THUNAR_DAEMON_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Thunar daemon"

			if echo "#thunar --daemon" | grep -q '#thunar --daemon' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Thunar daemon already disabled\n"
	
					else

						sed -i 's/thunar --daemon/#thunar --daemon/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #thunar --daemon &\n"

			fi

	fi

	if [ "$DAILY_PCMANFM_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling PCManFM as wallpaper manager"

			if echo "pcmanfm --desktop" | grep -q '#pcmanfm --desktop' $HOME/.config/openbox/autostart; then

				sed -i 's/#pcmanfm --desktop/pcmanfm --desktop/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} pcmanfm --desktop &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} PCManFM already enabled\n"

			fi

	elif [ "$DAILY_PCMANFM_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling PCManFM as wallpaper manager"

			if echo "#pcmanfm --desktop" | grep -q '#pcmanfm --desktop' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} PCManFM already disabled\n"
	
					else

						sed -i 's/pcmanfm --desktop/#pcmanfm --desktop/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #pcmanfm --desktop &\n"

			fi

	fi

	if [ "$DAILY_NITROGEN_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Nitrogen as wallpaper manager"

			if echo "sleep 1; nitrogen --restore" | grep -q '#sleep 1; nitrogen --restore' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 1; nitrogen --restore/sleep 1; nitrogen --restore/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 1; nitrogen --restore &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Nitrogen already enabled\n"

			fi

	elif [ "$DAILY_NITROGEN_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Nitrogen as wallpaper manager"

			if echo "#sleep 1; nitrogen --restore" | grep -q '#sleep 1; nitrogen --restore' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Nitrogen already disabled\n"
	
					else

						sed -i 's/sleep 1; nitrogen --restore/#sleep 1; nitrogen --restore/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 1; nitrogen --restore &\n"

			fi

	fi

	if [ "$DAILY_TINT2_PANEL_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Garuda tint2 session"

			if echo "sleep 2; garuda-tint2-session" | grep -q '#sleep 2; garuda-tint2-session' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 2; garuda-tint2-session/sleep 2; garuda-tint2-session/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 2; garuda-tint2-session &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Garuda tint2 session already enabled\n"

			fi

	elif [ "$DAILY_TINT2_PANEL_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Garuda tint2 session"

			if echo "#sleep 2; garuda-tint2-session" | grep -q '#sleep 2; garuda-tint2-session' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Garuda tint2 session already disabled\n"
	
					else

						sed -i 's/sleep 2; garuda-tint2-session/#sleep 2; garuda-tint2-session/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 2; garuda-tint2-session &\n"

			fi

	fi

	if [ "$DAILY_POLYBAR_PANEL_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Garuda polybar session"

			if echo "sleep 2; polybar -c" | grep -q '#sleep 2; polybar -c' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 2; polybar -c/sleep 2; polybar -c/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Garuda polybar session already enabled\n"

			fi

	elif [ "$DAILY_POLYBAR_PANEL_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Garuda polybar session"

			if echo "#sleep 2; polybar -c" | grep -q '#sleep 2; polybar -c' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Garuda polybar session already disabled\n"
	
					else

						sed -i 's/sleep 2; polybar -c/#sleep 2; polybar -c/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &\n"

			fi

	fi

	if [ "$DAILY_CONKY_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Garuda Conky session"

			if echo "garuda-conky-session" | grep -q '#garuda-conky-session' $HOME/.config/openbox/autostart; then

				sed -i 's/#garuda-conky-session/garuda-conky-session/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} garuda-conky-session &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Garuda Conky session already enabled\n"

			fi

	elif [ "$DAILY_CONKY_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Garuda Conky session"

			if echo "#garuda-conky-session" | grep -q '#garuda-conky-session' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Garuda Conky session already disabled\n"
	
					else

						sed -i 's/garuda-conky-session/#garuda-conky-session/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #garuda-conky-session &\n"

			fi

	fi

	if [ "$DAILY_VOLUMEICON_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Volumeicon"

			if echo "sleep 1; volumeicon" | grep -q '#sleep 1; volumeicon' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 1; volumeicon/sleep 1; volumeicon/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 1; volumeicon &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Volumeicon already enabled\n"

			fi

	elif [ "$DAILY_VOLUMEICON_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Volumeicon"

			if echo "#sleep 1; volumeicon" | grep -q '#sleep 1; volumeicon' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Volumeicon already disabled\n"
	
					else

						sed -i 's/sleep 1; volumeicon/#sleep 1; volumeicon/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 1; volumeicon &\n"

			fi

	fi

	if [ "$DAILY_SKIPPY_DAEMON_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Skippy-XD daemon"

			if echo "skippy-xd --config" | grep -q '#skippy-xd --config' $HOME/.config/openbox/autostart; then

				sed -i 's/#skippy-xd --config/skippy-xd --config/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Skippy-XD daemon already enabled\n"

			fi

	elif [ "$DAILY_SKIPPY_DAEMON_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Skippy-XD daemon"

			if echo "#skippy-xd --config" | grep -q '#skippy-xd --config' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Skippy-XD daemon already disabled\n"
	
					else

						sed -i 's/skippy-xd --config/#skippy-xd --config/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &\n"

			fi

	fi

	if [ "$DAILY_SUPERKEY_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Super key for menu"

			if echo "xcape -e 'Super_L=Alt_L|F1'" | grep -q "#xcape -e 'Super_L=Alt_L|F1'" $HOME/.config/openbox/autostart; then

				sed -i "s/#xcape -e 'Super_L=Alt_L|F1'/xcape -e 'Super_L=Alt_L|F1'/" $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} xcape -e 'Super_L=Alt_L|F1' &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Super key already enabled\n"

			fi

	elif [ "$DAILY_SUPERKEY_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Super key for menu"

			if echo "#xcape -e 'Super_L=Alt_L|F1'" | grep -q "#xcape -e 'Super_L=Alt_L|F1'" $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Super key already disabled\n"
	
					else

						sed -i "s/xcape -e 'Super_L=Alt_L|F1'/#xcape -e 'Super_L=Alt_L|F1'/" $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #xcape -e 'Super_L=Alt_L|F1' &\n"

			fi

	fi

	if [ "$DAILY_POLICYKIT_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling GNOME PolicyKit"

			if echo "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1" | grep -q '#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1' $HOME/.config/openbox/autostart; then

				sed -i 's|#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1|/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1|' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} GNOME PolicyKit already enabled\n"

			fi

	elif [ "$DAILY_POLICYKIT_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling GNOME PolicyKit"

			if echo "#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1" | grep -q '#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} GNOME PolicyKit already disabled\n"
	
					else

						sed -i 's|/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1|#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1|' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &\n"

			fi

	fi

	if [ "$DAILY_SIDE_SHORTCUTS_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Thunar side shortcuts update"

			if echo "xdg-user-dirs-gtk-update" | grep -q '#xdg-user-dirs-gtk-update' $HOME/.config/openbox/autostart; then

					sed -i 's/#xdg-user-dirs-gtk-update/xdg-user-dirs-gtk-update/' $HOME/.config/openbox/autostart
					echo -e "${RED}  ->${RESETCOLOR} xdg-user-dirs-gtk-update &\n"

						else

							echo -e "${RED}  ->${RESETCOLOR} Thunar side shortcuts update already enabled\n"

			fi

	elif [ "$DAILY_SIDE_SHORTCUTS_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Thunar side shortcuts update"

			if echo "#xdg-user-dirs-gtk-update" | grep -q '#xdg-user-dirs-gtk-update' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Thunar side shortcuts update already disabled\n"
	
					else

						sed -i 's/xdg-user-dirs-gtk-update/#xdg-user-dirs-gtk-update/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #xdg-user-dirs-gtk-update &\n"

			fi

	fi

	if [ "$DAILY_NUMLOCK_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Num Lock at start"

			if echo "numlockx" | grep -q '#numlockx' $HOME/.config/openbox/autostart; then

				sed -i 's/#numlockx/numlockx/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} numlockx &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Num Lock at start already enabled\n"

			fi

	elif [ "$DAILY_NUMLOCK_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Num Lock at start"

			if echo "#numlockx" | grep -q '#numlockx' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Num Lock at start already disabled\n"
	
					else

						sed -i 's/numlockx/#numlockx/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #numlockx &\n"

			fi

	fi

	echo -e "${GREEN}==>${RESETCOLOR} Openbox configuration\n"

	if [ "$DAILY_OBANIMATIONS_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Openbox animations"

			if echo "<animateIconify>yes</animateIconify>" | grep -q '<animateIconify>no</animateIconify>' $HOME/.config/openbox/rc.xml; then

				sed -i 's|<animateIconify>no</animateIconify>|<animateIconify>yes</animateIconify>|' $HOME/.config/openbox/rc.xml
				echo -e "${RED}  ->${RESETCOLOR} <animateIconify>yes</animateIconify>\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Openbox animations already enabled\n"

			fi

	elif [ "$DAILY_OBANIMATIONS_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Openbox animations"

			if echo "<animateIconify>no</animateIconify>" | grep -q '<animateIconify>no</animateIconify>' $HOME/.config/openbox/rc.xml; then

				echo -e "${RED}  ->${RESETCOLOR} Openbox animations already disabled\n"
	
					else

						sed -i 's|<animateIconify>yes</animateIconify>|<animateIconify>no</animateIconify>|' $HOME/.config/openbox/rc.xml
						echo -e "${RED}  ->${RESETCOLOR} <animateIconify>no</animateIconify>\n"

			fi

	fi

	if [ "$DAILY_COMPOSITOR_SHADOW_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling compositor shadows"

			if echo "shadow = true;" | grep -q "shadow = false;" $HOME/.config/picom/picom.conf | head -1; then

				sed -i '48s/shadow = false;/shadow = true;/' $HOME/.config/picom/picom.conf
				echo -e "${RED}  ->${RESETCOLOR} shadow = true\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} compositor shadows already enabled\n"

			fi

	elif [ "$DAILY_COMPOSITOR_SHADOW_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling compositor shadows"

			if echo "shadow = false;" | grep -q "shadow = false;" $HOME/.config/picom/picom.conf | head -1; then

				echo -e "${RED}  ->${RESETCOLOR} compositor shadows already disabled\n"
	
					else

						sed -i '48s/shadow = true;/shadow = false;/' $HOME/.config/picom/picom.conf
						echo -e "${RED}  ->${RESETCOLOR} shadow = false\n"

			fi

	fi

	echo -e "${GREEN}==>${RESETCOLOR} ALSA configuration\n"

		if [ "$DAILY_ALSA_CONFIGS_CHECK" = "TRUE" ]; then

			echo -e "${RED}-->${RESETCOLOR} replacing ALSA settings with asoundrc.daily configuration"
			cp ~/.config/openbox/obconfigs/asoundrc.daily ~/.asoundrc
			echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/asoundrc.daily ~/.asoundrc\n"

				else

					echo -e "${RED}-->${RESETCOLOR} leaving current settings"
					echo -e "${RED}  ->${RESETCOLOR} no operation\n"

		fi

	echo -e "${GREEN}==>${RESETCOLOR} TLP settings configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for config files"

		if [ -z "$TLP_DAILY_CONFIG_FILE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} Config files not found, skipping process"

				else

					if [ "$DAILY_TLP_CONFIGS_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} replacing TLP settings with tlp.daily configuration"
						sudo cp ~/.config/openbox/obconfigs/tlp.daily /etc/default/tlp
						sudo cp ~/.config/openbox/obconfigs/tlp.daily /etc/tlp.conf
						echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/tlp.daily /etc/default/tlp"
						echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/tlp.daily /etc/tlp.conf\n"

							else

								echo -e "${RED}-->${RESETCOLOR} restoring default configuration"
								sudo cp ~/.config/openbox/obconfigs/tlp.default /etc/default/tlp
								sudo cp ~/.config/openbox/obconfigs/tlp.default /etc/tlp.conf
								echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/tlp.default /etc/default/tlp"
								echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/tlp.default /etc/tlp.conf\n"

					fi

		fi

	echo -e "${GREEN}==>${RESETCOLOR} CPU Power settings configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for config files"

		if [ -z "$CPUPOWER_DAILY_CONFIG_FILE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} Config files not found, skipping process"

				else

					if [ "$DAILY_CPUPOWER_CONFIGS_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} replacing CPU Power settings with cpupower.daily configuration"
						sudo cp ~/.config/openbox/obconfigs/cpupower.daily /etc/default/cpupower
						echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/cpupower.daily /etc/default/cpupower\n"

							else

								echo -e "${RED}-->${RESETCOLOR} restoring default configuration"
								sudo cp ~/.config/openbox/obconfigs/cpupower.default /etc/default/cpupower
								echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/cpupower.default /etc/default/cpupower\n"

					fi

		fi

	echo -e "${GREEN}==>${RESETCOLOR} CPU Power service configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for cpupower"

		if [ -z "$CPUPOWER_SERVICE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} cpupower not installed, skipping process\n"

				else

					if [ "$DAILY_CPU_SERVICE_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} enabling CPU Power service"

							if [ "$CPUPOWER_SERVICE_STATUS" = "active" ]; then

								echo -e "${RED}  ->${RESETCOLOR} CPU Power service already enabled\n"

									else

										sudo systemctl enable cpupower.service
										echo -e "${RED}  ->${RESETCOLOR} systemctl enable cpupower.service\n"

							fi

						else

							echo -e "${RED}-->${RESETCOLOR} disabling CPU Power service"

								if [ "$CPUPOWER_SERVICE_STATUS" = "inactive" ]; then

									echo -e "${RED}  ->${RESETCOLOR} CPU Power service already disabled\n"

										else

											sudo systemctl disable cpupower.service
											echo -e "${RED}  ->${RESETCOLOR} systemctl disable cpupower.service\n"		

								fi

				fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} RTC service configuration\n"

		if [ "$DAILY_RTC_SERVICE_CHECK" = "TRUE" ]; then

			echo -e "${RED}-->${RESETCOLOR} enabling RTC service"

				if [ $RTC_SERVICE_STATUS -gt 64 ]; then

					echo -e "${RED}  ->${RESETCOLOR} CPU Power service already enabled\n"

						else

							sudo systemctl enable rtc.service
							echo -e "${RED}  ->${RESETCOLOR} systemctl enable rtc.service\n"

				fi

			else

				echo -e "${RED}-->${RESETCOLOR} disabling RTC service"

					if [ $RTC_SERVICE_STATUS -eq 64 ]; then

						echo -e "${RED}  ->${RESETCOLOR} RTC service already disabled\n"

							else

								sudo systemctl disable rtc.service
								echo -e "${RED}  ->${RESETCOLOR} systemctl disable rtc.service\n"		

					fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} HPET service configuration\n"

		if [ "$DAILY_HPET_SERVICE_CHECK" = "TRUE" ]; then

			echo -e "${RED}-->${RESETCOLOR} enabling HPET service"

				if [ $HPET_SERVICE_STATUS -gt 64 ]; then

					echo -e "${RED}  ->${RESETCOLOR} HPET service already enabled\n"

						else

							sudo systemctl enable hpet.service
							echo -e "${RED}  ->${RESETCOLOR} systemctl enable hpet.service\n"

				fi

			else

				echo -e "${RED}-->${RESETCOLOR} disabling HPET service"

					if [ $HPET_SERVICE_STATUS -eq 64 ]; then

						echo -e "${RED}  ->${RESETCOLOR} HPET service already disabled\n"

							else

								sudo systemctl disable hpet.service
								echo -e "${RED}  ->${RESETCOLOR} systemctl disable hpet.service\n"		

					fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} UFW service configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for ufw"

		if [ -z "$UFW_SERVICE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} ufw not installed, skipping process\n"

				else

					if [ "$DAILY_UFW_SERVICE_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} enabling UFW service"

							if [ "$UFW_SERVICE_STATUS" = "active" ]; then

								echo -e "${RED}  ->${RESETCOLOR} UFW service already enabled\n"

									else

										sudo systemctl enable ufw.service
										echo -e "${RED}  ->${RESETCOLOR} systemctl enable ufw.service\n"

							fi

						else

							echo -e "${RED}-->${RESETCOLOR} disabling UFW service"

								if [ "$UFW_SERVICE_STATUS" = "inactive" ]; then

									echo -e "${RED}  ->${RESETCOLOR} UFW service already disabled\n"

										else

											sudo systemctl disable ufw.service
											echo -e "${RED}  ->${RESETCOLOR} systemctl disable ufw.service\n"		

								fi

				fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} firewalld service configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for firewalld"

		if [ -z "$FIREWALLD_SERVICE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} firewalld not installed, skipping process\n"

				else

					if [ "$DAILY_FIREWALLD_SERVICE_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} enabling firewalld service"

							if [ "$FIREWALLD_SERVICE_STATUS" = "active" ]; then

								echo -e "${RED}  ->${RESETCOLOR} firewalld service already enabled\n"

									else

										sudo systemctl enable firewalld.service
										echo -e "${RED}  ->${RESETCOLOR} systemctl enable firewalld.service\n"

							fi

						else

							echo -e "${RED}-->${RESETCOLOR} disabling firewalld service"

								if [ "$FIREWALLD_SERVICE_STATUS" = "inactive" ]; then

									echo -e "${RED}  ->${RESETCOLOR} firewalld service already disabled\n"

										else

											sudo systemctl disable firewalld.service
											echo -e "${RED}  ->${RESETCOLOR} systemctl disable firewalld.service\n"		

								fi

				fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} GRUB settings configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for config files"

		if [ -z "$GRUB_DAILY_CONFIG_FILE" ]; then

			echo -e "${RED}-->${RESETCOLOR} Config files not found, skipping process"

				else

					if [ "$DAILY_GRUB_CONFIGS_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} replacing GRUB settings with grub.daily configuration"
						sudo cp ~/.config/openbox/obconfigs/grub.daily /etc/default/grub
						echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/grub.daily /etc/default/grub\n"

						else

							echo -e "${RED}-->${RESETCOLOR} restoring default configuration"
							sudo cp ~/.config/openbox/obconfigs/grub.default /etc/default/grub
							echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/grub.default /etc/default/grub\n"

					fi

			echo -e "${GREEN}==>${RESETCOLOR} Updating GRUB\n"
			sudo update-grub; sleep 1

		fi

	echo -e "\n:: System settings changed to ${GREEN}Daily mode${RESETCOLOR}."
	echo -e ":: Do you want to shutdown [${YELLOW}pP${RESETCOLOR}] reboot system [${YELLOW}rR${RESETCOLOR}] logout [${YELLOW}lL${RESETCOLOR}] or continue [${YELLOW}cC${RESETCOLOR}] ?"

		while :

			do

				read -r -p ":: Answer: " input

					case $input in

					    [rR] | [rR] ) systemctl reboot ;;
					    [cC] | [cC] ) echo "Ok"; exit 0 ;;
						[pP] | [pP] ) systemctl poweroff ;;
						[lL] | [lL] ) openbox --exit ;;
	    				*)

						echo -e ":: ${RED}Invalid input...${RESETCOLOR}"

						;;

					esac

			done

}

studio() {

	if [ $MODE_STATUS = "OFF" ]; then

		cp ~/.config/tint2/recbox-studio-light.tint2rc ~/.config/tint2/tint2rc; garuda-tint2restart

	elif [ $MODE_STATUS = "ON" ]; then

		cp ~/.config/tint2/recbox-studio-dark.tint2rc ~/.config/tint2/tint2rc; garuda-tint2restart

	fi 2>/dev/null

	sed -i '5s/Daily/Studio/' ~/.config/workflow-settings/session-test-config

	echo -e "\n${WHITE}Setting up ${GREEN}Studio Mode${RESETCOLOR}.\n"

	echo -e "${GREEN}==>${RESETCOLOR} checking autostart file content\n"

		if echo "start_jack.sh &" | grep -q "start_jack.sh &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] start_jack.sh"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] start_jack.sh"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "cadence-session-start -s &" | grep -q "cadence-session-start -s &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] cadence-session-start -s"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] cadence-session-start -s"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "~/.screenlayout/dual-head.sh &" | grep -q "~/.screenlayout/dual-head.sh &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] ~/.screenlayout/dual-head.sh"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] ~/.screenlayout/dual-head.sh"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 0.5; xfsettingsd &" | grep -q "sleep 0.5; xfsettingsd &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] xfsettingsd"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] xfsettingsd"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 1; garuda-compositor --start &" | grep -q "sleep 1; garuda-compositor --start &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] garuda-compositor --start"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] garuda-compositor --start"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "thunar --daemon &" | grep -q "thunar --daemon &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] thunar --daemon"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] thunar --daemon"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "pcmanfm --desktop &" | grep -q "pcmanfm --desktop &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] pcmanfm --desktop"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] pcmanfm --desktop"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 1; nitrogen --restore &" | grep -q "sleep 1; nitrogen --restore &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] nitrogen --restore"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] nitrogen --restore"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 2; garuda-tint2-session &" | grep -q "sleep 2; garuda-tint2-session &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] garuda-tint2-session"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] garuda-tint2-session"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &" | grep -q 'sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &' $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] polybar -c $HOME/.config/polybar/config openbox-bar"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] polybar -c $HOME/.config/polybar/config openbox-bar"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "garuda-conky-session &" | grep -q "garuda-conky-session &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] garuda-conky-session"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] garuda-conky-session"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 1; volumeicon &" | grep -q "sleep 1; volumeicon &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] volumeicon"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] volumeicon"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &" | grep -q "skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "xcape -e 'Super_L=Alt_L|F1' &" | grep -q "xcape -e 'Super_L=Alt_L|F1' &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] xcape -e 'Super_L=Alt_L|F1'"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] xcape -e 'Super_L=Alt_L|F1'"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1" | grep -q '/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1' $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &" | grep -q "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] xdg-user-dirs-gtk-update"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] xdg-user-dirs-gtk-update"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "numlockx &" | grep -q "numlockx &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] numlockx"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] numlockx"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

	echo -e "\n${GREEN}==>${RESETCOLOR} autostart file configuration\n"

	if [ "$STUDIO_CUSTOM_JACK_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling custom jack script"

			if echo "start_jack.sh" | grep -q '#start_jack.sh' $HOME/.config/openbox/autostart; then

				sed -i 's/#start_jack.sh/start_jack.sh/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} start_jack.sh &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} custom jack script already enabled\n"

			fi

	elif [ "$STUDIO_CUSTOM_JACK_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling custom jack script"

			if echo "#start_jack.sh" | grep -q '#start_jack.sh' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} custom jack script already disabled\n"
	
					else

						sed -i 's/start_jack.sh/#start_jack.sh/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #start_jack.sh &\n"

			fi

	fi

	if [ "$STUDIO_CADENCE_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling cadence-session-start"

			if echo "cadence-session-start" | grep -q '#cadence-session-start' $HOME/.config/openbox/autostart; then

				sed -i 's/#cadence-session-start/cadence-session-start/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} cadence-session-start -s &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} cadence-session-start already enabled\n"

			fi

	elif [ "$STUDIO_CADENCE_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling cadence-session-start"

			if echo "#cadence-session-start" | grep -q '#cadence-session-start' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} cadence-session-start already disabled\n"
	
					else

						sed -i 's/cadence-session-start/#cadence-session-start/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #cadence-session-start -s &\n"

			fi

	fi

	if [ "$STUDIO_ARANDR_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling dual-head script (required when xfsettingsd not working)"

			if echo "~/.screenlayout/dual-head.sh" | grep -q '#~/.screenlayout/dual-head.sh' $HOME/.config/openbox/autostart; then

				sed -i "s|#~/.screenlayout|~/.screenlayout|" $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} ~/.screenlayout/dual-head.sh &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} dual-head script already enabled\n"

			fi

	elif [ "$STUDIO_ARANDR_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling dual-head script (required when xfsettingsd not working)"

			if echo "#~/.screenlayout/dual-head.sh" | grep -q '#~/.screenlayout/dual-head.sh' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} dual-head script already disabled\n"
	
					else

						sed -i 's|~/.screenlayout|#~/.screenlayout|' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #~/.screenlayout/dual-head.sh &\n"

			fi

	fi

	if [ "$STUDIO_XFSETTINGSD_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling xfsettingsd (required for xfce settings to work)"

			if echo "sleep 0.5; xfsettingsd" | grep -q '#sleep 0.5; xfsettingsd' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 0.5; xfsettingsd/sleep 0.5; xfsettingsd/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 0.5; xfsettingsd &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} xfsettingsd already enabled\n"

			fi

	elif [ "$STUDIO_XFSETTINGSD_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling xfsettingsd (required for xfce settings to work)"

			if echo "#sleep 0.5; xfsettingsd" | grep -q '#sleep 0.5; xfsettingsd' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} xfsettingsd already disabled\n"
	
					else

						sed -i 's/sleep 0.5; xfsettingsd/#sleep 0.5; xfsettingsd/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 0.5; xfsettingsd &\n"

			fi

	fi

	if [ "$STUDIO_COMPOSITOR_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Garuda compositor"

			if echo "sleep 1; garuda-compositor" | grep -q '#sleep 1; garuda-compositor' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 1; garuda-compositor/sleep 1; garuda-compositor/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 1; garuda-compositor --start &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Garuda compositor already enabled\n"

			fi

	elif [ "$STUDIO_COMPOSITOR_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Garuda compositor"

			if echo "#sleep 1; garuda-compositor" | grep -q '#sleep 1; garuda-compositor' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Garuda compositor already disabled\n"
	
					else

						sed -i 's/sleep 1; garuda-compositor/#sleep 1; garuda-compositor/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 1; garuda-compositor --start &\n"

			fi

	fi

	if [ "$STUDIO_THUNAR_DAEMON_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Thunar in daemon mode"

			if echo "thunar --daemon" | grep -q '#thunar --daemon' $HOME/.config/openbox/autostart; then

				sed -i 's/#thunar --daemon/thunar --daemon/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} thunar --daemon &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Thunar daemon already enabled\n"

			fi

	elif [ "$STUDIO_THUNAR_DAEMON_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Thunar daemon"

			if echo "#thunar --daemon" | grep -q '#thunar --daemon' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Thunar daemon already disabled\n"
	
					else

						sed -i 's/thunar --daemon/#thunar --daemon/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #thunar --daemon &\n"

			fi

	fi

	if [ "$STUDIO_PCMANFM_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling PCManFM as wallpaper manager"

			if echo "pcmanfm --desktop" | grep -q '#pcmanfm --desktop' $HOME/.config/openbox/autostart; then

				sed -i 's/#pcmanfm --desktop/pcmanfm --desktop/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} pcmanfm --desktop &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} PCManFM already enabled\n"

			fi

	elif [ "$STUDIO_PCMANFM_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling PCManFM as wallpaper manager"

			if echo "#pcmanfm --desktop" | grep -q '#pcmanfm --desktop' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} PCManFM already disabled\n"
	
					else

						sed -i 's/pcmanfm --desktop/#pcmanfm --desktop/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #pcmanfm --desktop &\n"

			fi

	fi

	if [ "$STUDIO_NITROGEN_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Nitrogen as wallpaper manager"

			if echo "sleep 1; nitrogen --restore" | grep -q '#sleep 1; nitrogen --restore' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 1; nitrogen --restore/sleep 1; nitrogen --restore/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 1; nitrogen --restore &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Nitrogen already enabled\n"

			fi

	elif [ "$STUDIO_NITROGEN_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Nitrogen as wallpaper manager"

			if echo "#sleep 1; nitrogen --restore" | grep -q '#sleep 1; nitrogen --restore' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Nitrogen already disabled\n"
	
					else

						sed -i 's/sleep 1; nitrogen --restore/#sleep 1; nitrogen --restore/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 1; nitrogen --restore &\n"

			fi

	fi

	if [ "$STUDIO_TINT2_PANEL_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Garuda tint2 session"

			if echo "sleep 2; garuda-tint2-session" | grep -q '#sleep 2; garuda-tint2-session' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 2; garuda-tint2-session/sleep 2; garuda-tint2-session/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 2; garuda-tint2-session &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Garuda tint2 session already enabled\n"

			fi

	elif [ "$STUDIO_TINT2_PANEL_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Garuda tint2 session"

			if echo "#sleep 2; garuda-tint2-session" | grep -q '#sleep 2; garuda-tint2-session' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Garuda tint2 session already disabled\n"
	
					else

						sed -i 's/sleep 2; garuda-tint2-session/#sleep 2; garuda-tint2-session/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 2; garuda-tint2-session &\n"

			fi

	fi

	if [ "$STUDIO_POLYBAR_PANEL_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Garuda polybar session"

			if echo "sleep 2; polybar -c" | grep -q '#sleep 2; polybar -c' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 2; polybar -c/sleep 2; polybar -c/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Garuda polybar session already enabled\n"

			fi

	elif [ "$STUDIO_POLYBAR_PANEL_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Garuda polybar session"

			if echo "#sleep 2; polybar -c" | grep -q '#sleep 2; polybar -c' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Garuda polybar session already disabled\n"
	
					else

						sed -i 's/sleep 2; polybar -c/#sleep 2; polybar -c/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &\n"

			fi

	fi

	if [ "$STUDIO_CONKY_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Garuda Conky session"

			if echo "garuda-conky-session" | grep -q '#garuda-conky-session' $HOME/.config/openbox/autostart; then

				sed -i 's/#garuda-conky-session/garuda-conky-session/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} garuda-conky-session &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Garuda Conky session already enabled\n"

			fi

	elif [ "$STUDIO_CONKY_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Garuda Conky session"

			if echo "#garuda-conky-session" | grep -q '#garuda-conky-session' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Garuda Conky session already disabled\n"
	
					else

						sed -i 's/garuda-conky-session/#garuda-conky-session/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #garuda-conky-session &\n"

			fi

	fi

	if [ "$STUDIO_VOLUMEICON_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Volumeicon"

			if echo "sleep 1; volumeicon" | grep -q '#sleep 1; volumeicon' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 1; volumeicon/sleep 1; volumeicon/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 1; volumeicon &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Volumeicon already enabled\n"

			fi

	elif [ "$STUDIO_VOLUMEICON_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Volumeicon"

			if echo "#sleep 1; volumeicon" | grep -q '#sleep 1; volumeicon' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Volumeicon already disabled\n"
	
					else

						sed -i 's/sleep 1; volumeicon/#sleep 1; volumeicon/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 1; volumeicon &\n"

			fi

	fi

	if [ "$STUDIO_SKIPPY_DAEMON_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Skippy-XD daemon"

			if echo "skippy-xd --config" | grep -q '#skippy-xd --config' $HOME/.config/openbox/autostart; then

				sed -i 's/#skippy-xd --config/skippy-xd --config/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Skippy-XD daemon already enabled\n"

			fi

	elif [ "$STUDIO_SKIPPY_DAEMON_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Skippy-XD daemon"

			if echo "#skippy-xd --config" | grep -q '#skippy-xd --config' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Skippy-XD daemon already disabled\n"
	
					else

						sed -i 's/skippy-xd --config/#skippy-xd --config/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &\n"

			fi

	fi

	if [ "$STUDIO_SUPERKEY_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Super key for menu"

			if echo "xcape -e 'Super_L=Alt_L|F1'" | grep -q "#xcape -e 'Super_L=Alt_L|F1'" $HOME/.config/openbox/autostart; then

				sed -i "s/#xcape -e 'Super_L=Alt_L|F1'/xcape -e 'Super_L=Alt_L|F1'/" $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} xcape -e 'Super_L=Alt_L|F1' &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Super key already enabled\n"

			fi

	elif [ "$STUDIO_SUPERKEY_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Super key for menu"

			if echo "#xcape -e 'Super_L=Alt_L|F1'" | grep -q "#xcape -e 'Super_L=Alt_L|F1'" $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Super key already disabled\n"
	
					else

						sed -i "s/xcape -e 'Super_L=Alt_L|F1'/#xcape -e 'Super_L=Alt_L|F1'/" $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #xcape -e 'Super_L=Alt_L|F1' &\n"

			fi

	fi

	if [ "$STUDIO_POLICYKIT_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling GNOME PolicyKit"

			if echo "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1" | grep -q '#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1' $HOME/.config/openbox/autostart; then

				sed -i 's|#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1|/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1|' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} GNOME PolicyKit already enabled\n"

			fi

	elif [ "$STUDIO_POLICYKIT_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling GNOME PolicyKit"

			if echo "#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1" | grep -q '#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} GNOME PolicyKit already disabled\n"
	
					else

						sed -i 's|/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1|#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1|' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &\n"

			fi

	fi

	if [ "$STUDIO_SIDE_SHORTCUTS_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Thunar side shortcuts update"

			if echo "xdg-user-dirs-gtk-update" | grep -q '#xdg-user-dirs-gtk-update' $HOME/.config/openbox/autostart; then

				sed -i 's/#xdg-user-dirs-gtk-update/xdg-user-dirs-gtk-update/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} xdg-user-dirs-gtk-update &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Thunar side shortcuts update already enabled\n"

			fi

	elif [ "$STUDIO_SIDE_SHORTCUTS_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Thunar side shortcuts update"

			if echo "#xdg-user-dirs-gtk-update" | grep -q '#xdg-user-dirs-gtk-update' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Thunar side shortcuts update already disabled\n"
	
					else

						sed -i 's/xdg-user-dirs-gtk-update/#xdg-user-dirs-gtk-update/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #xdg-user-dirs-gtk-update &\n"

			fi

	fi

	if [ "$STUDIO_NUMLOCK_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Num Lock at start"

			if echo "numlockx" | grep -q '#numlockx' $HOME/.config/openbox/autostart; then

				sed -i 's/#numlockx/numlockx/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} numlockx &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Num Lock at start already enabled\n"

			fi

	elif [ "$STUDIO_NUMLOCK_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Num Lock at start"

			if echo "#numlockx" | grep -q '#numlockx' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Num Lock at start already disabled\n"
	
					else

						sed -i 's/numlockx/#numlockx/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #numlockx &\n"

			fi

	fi

	echo -e "${GREEN}==>${RESETCOLOR} Openbox configuration\n"

	if [ "$STUDIO_OBANIMATIONS_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Openbox animations"

			if echo "<animateIconify>yes</animateIconify>" | grep -q '<animateIconify>no</animateIconify>' $HOME/.config/openbox/rc.xml; then

				sed -i 's|<animateIconify>no</animateIconify>|<animateIconify>yes</animateIconify>|' $HOME/.config/openbox/rc.xml
				echo -e "${RED}  ->${RESETCOLOR} <animateIconify>yes</animateIconify>\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Openbox animations already enabled\n"

			fi

	elif [ "$STUDIO_OBANIMATIONS_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Openbox animations"

			if echo "<animateIconify>no</animateIconify>" | grep -q '<animateIconify>no</animateIconify>' $HOME/.config/openbox/rc.xml; then

				echo -e "${RED}  ->${RESETCOLOR} Openbox animations already disabled\n"
	
					else

						sed -i 's|<animateIconify>yes</animateIconify>|<animateIconify>no</animateIconify>|' $HOME/.config/openbox/rc.xml
						echo -e "${RED}  ->${RESETCOLOR} <animateIconify>no</animateIconify>\n"

			fi

	fi

	if [ "$STUDIO_COMPOSITOR_SHADOW_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling compositor shadows"

			if echo "shadow = true;" | grep -q "shadow = false;" $HOME/.config/picom/picom.conf | head -1; then

				sed -i '48s/shadow = false;/shadow = true;/' $HOME/.config/picom/picom.conf
				echo -e "${RED}  ->${RESETCOLOR} shadow = true\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} compositor shadows already enabled\n"

			fi

	elif [ "$STUDIO_COMPOSITOR_SHADOW_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling compositor shadows"

			if echo "shadow = false;" | grep -q "shadow = false;" $HOME/.config/picom/picom.conf | head -1; then

				echo -e "${RED}  ->${RESETCOLOR} compositor shadows already disabled\n"
	
					else

						sed -i '48s/shadow = true;/shadow = false;/' $HOME/.config/picom/picom.conf
						echo -e "${RED}  ->${RESETCOLOR} shadow = false\n"

			fi

	fi

	echo -e "${GREEN}==>${RESETCOLOR} ALSA configuration\n"

		if [ "$STUDIO_ALSA_CONFIGS_CHECK" = "TRUE" ]; then

			echo -e "${RED}-->${RESETCOLOR} replacing ALSA settings with asoundrc.studio configuration"
			cp ~/.config/openbox/obconfigs/asoundrc.studio ~/.asoundrc
			echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/asoundrc.studio ~/.asoundrc\n"

				else

					echo -e "${RED}-->${RESETCOLOR} leaving current settings"
					echo -e "${RED}  ->${RESETCOLOR} no operation\n"

		fi

	echo -e "${GREEN}==>${RESETCOLOR} TLP settings configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for config files"

		if [ -z "$TLP_STUDIO_CONFIG_FILE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} Config files not found, skipping process"

				else

					if [ "$STUDIO_TLP_CONFIGS_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} replacing TLP settings with tlp.studio configuration"
						sudo cp ~/.config/openbox/obconfigs/tlp.studio /etc/default/tlp
						sudo cp ~/.config/openbox/obconfigs/tlp.studio /etc/tlp.conf
						echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/tlp.studio /etc/default/tlp"
						echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/tlp.studio /etc/tlp.conf\n"

							else

								echo -e "${RED}-->${RESETCOLOR} restoring default configuration\n"
								sudo cp ~/.config/openbox/obconfigs/tlp.default /etc/default/tlp
								sudo cp ~/.config/openbox/obconfigs/tlp.default /etc/tlp.conf
								echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/tlp.default /etc/default/tlp"
								echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/tlp.default /etc/tlp.conf\n"

					fi

	fi

	echo -e "${GREEN}==>${RESETCOLOR} CPU Power settings configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for config files"

		if [ -z "$CPUPOWER_STUDIO_CONFIG_FILE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} Config files not found, skipping process"

				else

					if [ "$STUDIO_CPUPOWER_CONFIGS_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} replacing CPU Power settings with cpupower.studio configuration"
						sudo cp ~/.config/openbox/obconfigs/cpupower.studio /etc/default/cpupower
						echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/cpupower.studio /etc/default/cpupower\n"

							else

								echo -e "${RED}-->${RESETCOLOR} restoring default configuration"
								sudo cp ~/.config/openbox/obconfigs/cpupower.default /etc/default/cpupower
								echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/cpupower.default /etc/default/cpupower\n"

					fi

		fi

	echo -e "${GREEN}==>${RESETCOLOR} CPU Power service configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for cpupower"

		if [ -z "$CPUPOWER_SERVICE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} cpupower not installed, skipping process\n"

				else

					if [ "$STUDIO_CPU_SERVICE_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} enabling CPU Power service"

							if [ "$CPUPOWER_SERVICE_STATUS" = "active" ]; then

								echo -e "${RED}  ->${RESETCOLOR} CPU Power service already enabled\n"

									else

										sudo systemctl enable cpupower.service
										echo -e "${RED}  ->${RESETCOLOR} systemctl enable cpupower.service\n"

							fi

						else

							echo -e "${RED}-->${RESETCOLOR} disabling CPU Power service"

								if [ "$CPUPOWER_SERVICE_STATUS" = "inactive" ]; then

									echo -e "${RED}  ->${RESETCOLOR} CPU Power service already disabled\n"

										else

											sudo systemctl disable cpupower.service
											echo -e "${RED}  ->${RESETCOLOR} systemctl disable cpupower.service\n"

								fi

				fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} RTC service configuration\n"

		if [ "$STUDIO_RTC_SERVICE_CHECK" = "TRUE" ]; then

				echo -e "${RED}-->${RESETCOLOR} enabling RTC service"

					if [ $RTC_SERVICE_STATUS -gt 64 ]; then

						echo -e "${RED}  ->${RESETCOLOR} CPU Power service already enabled\n"

							else

								sudo systemctl enable rtc.service
								echo -e "${RED}  ->${RESETCOLOR} systemctl enable rtc.service\n"

					fi

			else

				echo -e "${RED}-->${RESETCOLOR} disabling RTC service"

					if [ $RTC_SERVICE_STATUS -eq 64 ]; then

						echo -e "${RED}  ->${RESETCOLOR} RTC service already disabled\n"

							else

								sudo systemctl disable rtc.service
								echo -e "${RED}  ->${RESETCOLOR} systemctl disable rtc.service\n"		

					fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} HPET service configuration\n"

		if [ "$STUDIO_HPET_SERVICE_CHECK" = "TRUE" ]; then

			echo -e "${RED}-->${RESETCOLOR} enabling HPET service"

				if [ $HPET_SERVICE_STATUS -gt 64 ]; then

					echo -e "${RED}  ->${RESETCOLOR} HPET service already enabled\n"

						else

							sudo systemctl enable hpet.service
							echo -e "${RED}  ->${RESETCOLOR} systemctl enable hpet.service\n"

				fi

			else

				echo -e "${RED}-->${RESETCOLOR} disabling HPET service"

					if [ $HPET_SERVICE_STATUS -eq 64 ]; then

						echo -e "${RED}  ->${RESETCOLOR} HPET service already disabled\n"

							else

								sudo systemctl disable hpet.service
								echo -e "${RED}  ->${RESETCOLOR} systemctl disable hpet.service\n"		

					fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} UFW service configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for ufw"

		if [ -z "$UFW_SERVICE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} ufw not installed, skipping process\n"

				else

					if [ "$STUDIO_UFW_SERVICE_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} enabling UFW service"

							if [ "$UFW_SERVICE_STATUS" = "active" ]; then

								echo -e "${RED}  ->${RESETCOLOR} UFW service already enabled\n"

									else

										sudo systemctl enable ufw.service
										echo -e "${RED}  ->${RESETCOLOR} systemctl enable ufw.service\n"

							fi

						else

							echo -e "${RED}-->${RESETCOLOR} disabling UFW service"

								if [ "$UFW_SERVICE_STATUS" = "inactive" ]; then

									echo -e "${RED}  ->${RESETCOLOR} UFW service already disabled\n"

										else

											sudo systemctl disable ufw.service
											echo -e "${RED}  ->${RESETCOLOR} systemctl disable ufw.service\n"		

								fi

				fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} firewalld service configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for firewalld"

		if [ -z "$FIREWALLD_SERVICE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} firewalld not installed, skipping process\n"

				else

					if [ "$STUDIO_FIREWALLD_SERVICE_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} enabling firewalld service"

							if [ "$FIREWALLD_SERVICE_STATUS" = "active" ]; then

								echo -e "${RED}  ->${RESETCOLOR} firewalld service already enabled\n"

									else

										sudo systemctl enable firewalld.service
										echo -e "${RED}  ->${RESETCOLOR} systemctl enable firewalld.service\n"

							fi

						else

							echo -e "${RED}-->${RESETCOLOR} disabling firewalld service"

								if [ "$FIREWALLD_SERVICE_STATUS" = "inactive" ]; then

									echo -e "${RED}  ->${RESETCOLOR} firewalld service already disabled\n"

										else

											sudo systemctl disable firewalld.service
											echo -e "${RED}  ->${RESETCOLOR} systemctl disable firewalld.service\n"		

								fi

				fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} GRUB settings configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for config files"

		if [ -z "$GRUB_STUDIO_CONFIG_FILE" ]; then

			echo -e "${RED}-->${RESETCOLOR} Config files not found, skipping process"

				else

					if [ "$STUDIO_GRUB_CONFIGS_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} replacing GRUB settings with grub.studio configuration"
						sudo cp ~/.config/openbox/obconfigs/grub.studio /etc/default/grub
						echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/grub.studio /etc/default/grub\n"

							else

								echo -e "${RED}-->${RESETCOLOR} restoring default configuration"
								sudo cp ~/.config/openbox/obconfigs/grub.default /etc/default/grub
								echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/grub.default /etc/default/grub\n"

					fi

			echo -e "${GREEN}==>${RESETCOLOR} Updating GRUB\n"
			sudo update-grub; sleep 1

		fi

	echo -e "\n:: System settings changed to ${GREEN}Studio mode${RESETCOLOR}."
	echo -e ":: Do you want to shutdown [${YELLOW}pP${RESETCOLOR}] reboot system [${YELLOW}rR${RESETCOLOR}] logout [${YELLOW}lL${RESETCOLOR}] or continue [${YELLOW}cC${RESETCOLOR}] ?"

		while :

			do

				read -r -p ":: Answer: " input

					case $input in

					    [rR] | [rR] ) systemctl reboot ;;
					    [cC] | [cC] ) echo "Ok"; exit 0 ;;
						[pP] | [pP] ) systemctl poweroff ;;
						[lL] | [lL] ) openbox --exit ;;
			    		* )

						echo -e ":: ${RED}Invalid input...${RESETCOLOR}"

						;;

					esac

			done

}

altr() {

	cp ~/.config/tint2/recbox-alternarive-dark.tint2rc ~/.config/tint2/tint2rc; garuda-tint2restart 2>/dev/null; garuda-polybar-session 2>/dev/null

	sed -i '5s/Studio/Daily/' ~/.config/workflow-settings/session-test-config

	echo -e "\n${WHITE}Setting up ${GREEN}Alternative Mode${RESETCOLOR}.\n"

	echo -e "${GREEN}==>${RESETCOLOR} checking autostart file content\n"

		if echo "start_jack.sh &" | grep -q "start_jack.sh &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] start_jack.sh"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] start_jack.sh"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "cadence-session-start -s &" | grep -q "cadence-session-start -s &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] cadence-session-start -s"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] cadence-session-start -s"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "~/.screenlayout/dual-head.sh &" | grep -q "~/.screenlayout/dual-head.sh &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] ~/.screenlayout/dual-head.sh"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] ~/.screenlayout/dual-head.sh"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 0.5; xfsettingsd &" | grep -q "sleep 0.5; xfsettingsd &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] xfsettingsd"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] xfsettingsd"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 1; garuda-compositor --start &" | grep -q "sleep 1; garuda-compositor --start &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] garuda-compositor --start"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] garuda-compositor --start"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "thunar --daemon &" | grep -q "thunar --daemon &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] thunar --daemon"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] thunar --daemon"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "pcmanfm --desktop &" | grep -q "pcmanfm --desktop &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] pcmanfm --desktop"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] pcmanfm --desktop"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 1; nitrogen --restore &" | grep -q "sleep 1; nitrogen --restore &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] nitrogen --restore"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] nitrogen --restore"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 2; garuda-tint2-session &" | grep -q "sleep 2; garuda-tint2-session &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] garuda-tint2-session"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] garuda-tint2-session"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &" | grep -q 'sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &' $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] polybar -c $HOME/.config/polybar/config openbox-bar"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] polybar -c $HOME/.config/polybar/config openbox-bar"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "garuda-conky-session &" | grep -q "garuda-conky-session &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] garuda-conky-session"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] garuda-conky-session"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "sleep 1; volumeicon &" | grep -q "sleep 1; volumeicon &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] volumeicon"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] volumeicon"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &" | grep -q "skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "xcape -e 'Super_L=Alt_L|F1' &" | grep -q "xcape -e 'Super_L=Alt_L|F1' &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] xcape -e 'Super_L=Alt_L|F1'"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] xcape -e 'Super_L=Alt_L|F1'"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1" | grep -q '/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1' $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &" | grep -q "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] xdg-user-dirs-gtk-update"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] xdg-user-dirs-gtk-update"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

		if echo "numlockx &" | grep -q "numlockx &" $HOME/.config/openbox/autostart; then

			echo -e "    [ ${GREEN}${RESETCOLOR} ] numlockx"

				else

					echo -e "    [ ${RED}${RESETCOLOR} ] numlockx"
					echo -e "    [ ${GREEN}i${RESETCOLOR} ] restoring default autostart config file"
					autostart_default_config; echo ""; read -r -p ":: Press Enter to continue " input

		fi

	echo -e "\n${GREEN}==>${RESETCOLOR} autostart file configuration\n"

	if [ "$ALTERNATIVE_CUSTOM_JACK_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling custom jack script"

			if echo "start_jack.sh" | grep -q '#start_jack.sh' $HOME/.config/openbox/autostart; then

				sed -i 's/#start_jack.sh/start_jack.sh/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} start_jack.sh &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} custom jack script already enabled\n"

			fi

	elif [ "$ALTERNATIVE_CUSTOM_JACK_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling custom jack script"

			if echo "#start_jack.sh" | grep -q '#start_jack.sh' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} custom jack script already disabled\n"
	
					else

						sed -i 's/start_jack.sh/#start_jack.sh/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #start_jack.sh &\n"

			fi

	fi

	if [ "$ALTERNATIVE_CADENCE_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling cadence-session-start"

			if echo "cadence-session-start" | grep -q '#cadence-session-start' $HOME/.config/openbox/autostart; then

				sed -i 's/#cadence-session-start/cadence-session-start/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} cadence-session-start -s &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} cadence-session-start already enabled\n"

			fi

	elif [ "$ALTERNATIVE_CADENCE_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling cadence-session-start"

			if echo "#cadence-session-start" | grep -q '#cadence-session-start' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} cadence-session-start already disabled\n"
	
					else

						sed -i 's/cadence-session-start/#cadence-session-start/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #cadence-session-start -s &\n"

			fi

	fi

	if [ "$ALTERNATIVE_ARANDR_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling dual-head script (required when xfsettingsd not working)"

			if echo "~/.screenlayout/dual-head.sh" | grep -q '#~/.screenlayout/dual-head.sh' $HOME/.config/openbox/autostart; then

				sed -i 's|#~/.screenlayout|~/.screenlayout|' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} ~/.screenlayout/dual-head.sh &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} dual-head script already enabled\n"

			fi

	elif [ "$ALTERNATIVE_ARANDR_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling dual-head script (required when xfsettingsd not working)"

			if echo "#~/.screenlayout/dual-head.sh" | grep -q '#~/.screenlayout/dual-head.sh' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} dual-head script already disabled\n"
	
					else

						sed -i 's|~/.screenlayout|#~/.screenlayout|' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #~/.screenlayout/dual-head.sh &\n"

			fi

	fi

	if [ "$ALTERNATIVE_XFSETTINGSD_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling xfsettingsd (required for xfce settings to work)"

			if echo "sleep 0.5; xfsettingsd" | grep -q '#sleep 0.5; xfsettingsd' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 0.5; xfsettingsd/sleep 0.5; xfsettingsd/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 0.5; xfsettingsd &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} xfsettingsd already enabled\n"

			fi

	elif [ "$ALTERNATIVE_XFSETTINGSD_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling xfsettingsd (required for xfce settings to work)"

			if echo "#sleep 0.5; xfsettingsd" | grep -q '#sleep 0.5; xfsettingsd' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} xfsettingsd already disabled\n"
	
					else

						sed -i 's/sleep 0.5; xfsettingsd/#sleep 0.5; xfsettingsd/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 0.5; xfsettingsd &\n"

			fi

	fi

	if [ "$ALTERNATIVE_COMPOSITOR_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Garuda compositor"

			if echo "sleep 1; garuda-compositor" | grep -q '#sleep 1; garuda-compositor' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 1; garuda-compositor/sleep 1; garuda-compositor/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 1; garuda-compositor --start &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Garuda compositor already enabled\n"

			fi

	elif [ "$ALTERNATIVE_COMPOSITOR_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Garuda compositor"

			if echo "#sleep 1; garuda-compositor" | grep -q '#sleep 1; garuda-compositor' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Garuda compositor already disabled\n"
	
					else

						sed -i 's/sleep 1; garuda-compositor/#sleep 1; garuda-compositor/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 1; garuda-compositor --start &\n"

			fi

	fi

	if [ "$ALTERNATIVE_THUNAR_DAEMON_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Thunar in daemon mode"

			if echo "thunar --daemon" | grep -q '#thunar --daemon' $HOME/.config/openbox/autostart; then

				sed -i 's/#thunar --daemon/thunar --daemon/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} thunar --daemon &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Thunar daemon already enabled\n"

			fi

	elif [ "$ALTERNATIVE_THUNAR_DAEMON_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Thunar daemon"

			if echo "#thunar --daemon" | grep -q '#thunar --daemon' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Thunar daemon already disabled\n"
	
					else

						sed -i 's/thunar --daemon/#thunar --daemon/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #thunar --daemon &\n"

			fi

	fi

	if [ "$ALTERNATIVE_PCMANFM_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling PCManFM as wallpaper manager"

			if echo "pcmanfm --desktop" | grep -q '#pcmanfm --desktop' $HOME/.config/openbox/autostart; then

				sed -i 's/#pcmanfm --desktop/pcmanfm --desktop/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} pcmanfm --desktop &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} PCManFM already enabled\n"

			fi

	elif [ "$ALTERNATIVE_PCMANFM_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling PCManFM as wallpaper manager"

			if echo "#pcmanfm --desktop" | grep -q '#pcmanfm --desktop' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} PCManFM already disabled\n"
	
					else

						sed -i 's/pcmanfm --desktop/#pcmanfm --desktop/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #pcmanfm --desktop &\n"

			fi

	fi

	if [ "$ALTERNATIVE_NITROGEN_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Nitrogen as wallpaper manager"

			if echo "sleep 1; nitrogen --restore" | grep -q '#sleep 1; nitrogen --restore' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 1; nitrogen --restore/sleep 1; nitrogen --restore/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 1; nitrogen --restore &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Nitrogen already enabled\n"

			fi

	elif [ "$ALTERNATIVE_NITROGEN_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Nitrogen as wallpaper manager"

			if echo "#sleep 1; nitrogen --restore" | grep -q '#sleep 1; nitrogen --restore' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Nitrogen already disabled\n"
	
					else

						sed -i 's/sleep 1; nitrogen --restore/#sleep 1; nitrogen --restore/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 1; nitrogen --restore &\n"

			fi

	fi

	if [ "$ALTERNATIVE_TINT2_PANEL_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Garuda tint2 session"

			if echo "sleep 2; garuda-tint2-session" | grep -q '#sleep 2; garuda-tint2-session' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 2; garuda-tint2-session/sleep 2; garuda-tint2-session/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 2; garuda-tint2-session &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Garuda tint2 session already enabled\n"

			fi

	elif [ "$ALTERNATIVE_TINT2_PANEL_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Garuda tint2 session"

			if echo "#sleep 2; garuda-tint2-session" | grep -q '#sleep 2; garuda-tint2-session' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Garuda tint2 session already disabled\n"
	
					else

						sed -i 's/sleep 2; garuda-tint2-session/#sleep 2; garuda-tint2-session/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 2; garuda-tint2-session &\n"

			fi

	fi

	if [ "$ALTERNATIVE_POLYBAR_PANEL_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Garuda polybar session"

			if echo "sleep 2; polybar -c" | grep -q '#sleep 2; polybar -c' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 2; polybar -c/sleep 2; polybar -c/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Garuda polybar session already enabled\n"

			fi

	elif [ "$ALTERNATIVE_POLYBAR_PANEL_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Garuda polybar session"

			if echo "#sleep 2; polybar -c" | grep -q '#sleep 2; polybar -c' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Garuda polybar session already disabled\n"
	
					else

						sed -i 's/sleep 2; polybar -c/#sleep 2; polybar -c/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 2; polybar -c $HOME/.config/polybar/config openbox-bar &\n"

			fi

	fi

	if [ "$ALTERNATIVE_CONKY_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Garuda Conky session"

			if echo "garuda-conky-session" | grep -q '#garuda-conky-session' $HOME/.config/openbox/autostart; then

				sed -i 's/#garuda-conky-session/garuda-conky-session/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} garuda-conky-session &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Garuda Conky session already enabled\n"

			fi

	elif [ "$ALTERNATIVE_CONKY_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Garuda Conky session"

			if echo "#garuda-conky-session" | grep -q '#garuda-conky-session' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Garuda Conky session already disabled\n"
	
					else

						sed -i 's/garuda-conky-session/#garuda-conky-session/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #garuda-conky-session &\n"

			fi

	fi

	if [ "$ALTERNATIVE_VOLUMEICON_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Volumeicon"

			if echo "sleep 1; volumeicon" | grep -q '#sleep 1; volumeicon' $HOME/.config/openbox/autostart; then

				sed -i 's/#sleep 1; volumeicon/sleep 1; volumeicon/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} sleep 1; volumeicon &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Volumeicon already enabled\n"

			fi

	elif [ "$ALTERNATIVE_VOLUMEICON_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Volumeicon"

			if echo "#sleep 1; volumeicon" | grep -q '#sleep 1; volumeicon' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Volumeicon already disabled\n"
	
					else

						sed -i 's/sleep 1; volumeicon/#sleep 1; volumeicon/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #sleep 1; volumeicon &\n"

			fi

	fi

	if [ "$ALTERNATIVE_SKIPPY_DAEMON_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Skippy-XD daemon"

			if echo "skippy-xd --config" | grep -q '#skippy-xd --config' $HOME/.config/openbox/autostart; then

				sed -i 's/#skippy-xd --config/skippy-xd --config/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Skippy-XD daemon already enabled\n"

			fi

	elif [ "$ALTERNATIVE_SKIPPY_DAEMON_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Skippy-XD daemon"

			if echo "#skippy-xd --config" | grep -q '#skippy-xd --config' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Skippy-XD daemon already disabled\n"
	
					else

						sed -i 's/skippy-xd --config/#skippy-xd --config/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon &\n"

			fi

	fi

	if [ "$ALTERNATIVE_SUPERKEY_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Super key for menu"

			if echo "xcape -e 'Super_L=Alt_L|F1'" | grep -q "#xcape -e 'Super_L=Alt_L|F1'" $HOME/.config/openbox/autostart; then

				sed -i "s/#xcape -e 'Super_L=Alt_L|F1'/xcape -e 'Super_L=Alt_L|F1'/" $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} xcape -e 'Super_L=Alt_L|F1' &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Super key already enabled\n"

			fi

	elif [ "$ALTERNATIVE_SUPERKEY_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Super key for menu"

			if echo "#xcape -e 'Super_L=Alt_L|F1'" | grep -q "#xcape -e 'Super_L=Alt_L|F1'" $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Super key already disabled\n"
	
					else

						sed -i "s/xcape -e 'Super_L=Alt_L|F1'/#xcape -e 'Super_L=Alt_L|F1'/" $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #xcape -e 'Super_L=Alt_L|F1' &\n"

			fi

	fi

	if [ "$ALTERNATIVE_POLICYKIT_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling GNOME PolicyKit"

			if echo "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1" | grep -q '#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1' $HOME/.config/openbox/autostart; then

				sed -i 's|#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1|/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1|' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} GNOME PolicyKit already enabled\n"

			fi

	elif [ "$ALTERNATIVE_POLICYKIT_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling GNOME PolicyKit"

			if echo "#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1" | grep -q '#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} GNOME PolicyKit already disabled\n"
	
					else

						sed -i 's|/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1|#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1|' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &\n"

			fi

	fi

	if [ "$ALTERNATIVE_SIDE_SHORTCUTS_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Thunar side shortcuts update"

			if echo "xdg-user-dirs-gtk-update" | grep -q '#xdg-user-dirs-gtk-update' $HOME/.config/openbox/autostart; then

				sed -i 's/#xdg-user-dirs-gtk-update/xdg-user-dirs-gtk-update/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} xdg-user-dirs-gtk-update &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Thunar side shortcuts update already enabled\n"

			fi

	elif [ "$ALTERNATIVE_SIDE_SHORTCUTS_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Thunar side shortcuts update"

			if echo "#xdg-user-dirs-gtk-update" | grep -q '#xdg-user-dirs-gtk-update' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Thunar side shortcuts update already disabled\n"
	
					else

						sed -i 's/xdg-user-dirs-gtk-update/#xdg-user-dirs-gtk-update/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #xdg-user-dirs-gtk-update &\n"

			fi

	fi

	if [ "$ALTERNATIVE_NUMLOCK_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Num Lock at start"

			if echo "numlockx" | grep -q '#numlockx' $HOME/.config/openbox/autostart; then

				sed -i 's/#numlockx/numlockx/' $HOME/.config/openbox/autostart
				echo -e "${RED}  ->${RESETCOLOR} numlockx &\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Num Lock at start already enabled\n"

			fi

	elif [ "$ALTERNATIVE_NUMLOCK_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Num Lock at start"

			if echo "#numlockx" | grep -q '#numlockx' $HOME/.config/openbox/autostart; then

				echo -e "${RED}  ->${RESETCOLOR} Num Lock at start already disabled\n"
	
					else

						sed -i 's/numlockx/#numlockx/' $HOME/.config/openbox/autostart
						echo -e "${RED}  ->${RESETCOLOR} #numlockx &\n"

			fi

	fi

	echo -e "${GREEN}==>${RESETCOLOR} Openbox configuration\n"

	if [ "$ALTERNATIVE_OBANIMATIONS_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling Openbox animations"

			if echo "<animateIconify>yes</animateIconify>" | grep -q '<animateIconify>no</animateIconify>' $HOME/.config/openbox/rc.xml; then

				sed -i 's|<animateIconify>no</animateIconify>|<animateIconify>yes</animateIconify>|' $HOME/.config/openbox/rc.xml
				echo -e "${RED}  ->${RESETCOLOR} <animateIconify>yes</animateIconify>\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} Openbox animations already enabled\n"

			fi

	elif [ "$ALTERNATIVE_OBANIMATIONS_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling Openbox animations"

			if echo "<animateIconify>no</animateIconify>" | grep -q '<animateIconify>no</animateIconify>' $HOME/.config/openbox/rc.xml; then

				echo -e "${RED}  ->${RESETCOLOR} Openbox animations already disabled\n"
	
					else

						sed -i 's|<animateIconify>yes</animateIconify>|<animateIconify>no</animateIconify>|' $HOME/.config/openbox/rc.xml
						echo -e "${RED}  ->${RESETCOLOR} <animateIconify>no</animateIconify>\n"

			fi

	fi

	if [ "$ALTERNATIVE_COMPOSITOR_SHADOW_CHECK" = "TRUE" ]; then

		echo -e "${RED}-->${RESETCOLOR} enabling compositor shadows"

			if echo "shadow = true;" | grep -q "shadow = false;" $HOME/.config/picom/picom.conf | head -1; then

				sed -i '48s/shadow = false;/shadow = true;/' $HOME/.config/picom/picom.conf
				echo -e "${RED}  ->${RESETCOLOR} shadow = true\n"

					else

						echo -e "${RED}  ->${RESETCOLOR} compositor shadows already enabled\n"

			fi

	elif [ "$ALTERNATIVE_COMPOSITOR_SHADOW_CHECK" = "FALSE" ]; then

		echo -e "${RED}-->${RESETCOLOR} disabling compositor shadows"

			if echo "shadow = false;" | grep -q "shadow = false;" $HOME/.config/picom/picom.conf | head -1; then

				echo -e "${RED}  ->${RESETCOLOR} compositor shadows already disabled\n"
	
					else

						sed -i '48s/shadow = true;/shadow = false;/' $HOME/.config/picom/picom.conf
						echo -e "${RED}  ->${RESETCOLOR} shadow = false\n"

			fi

	fi

	echo -e "${GREEN}==>${RESETCOLOR} ALSA configuration\n"

		if [ "$ALTERNATIVE_ALSA_CONFIGS_CHECK" = "TRUE" ]; then

			echo -e "${RED}-->${RESETCOLOR} replacing ALSA settings with asoundrc.daily configuration"
			cp ~/.config/openbox/obconfigs/asoundrc.daily ~/.asoundrc
			echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/asoundrc.daily ~/.asoundrc\n"

				else

					echo -e "${RED}-->${RESETCOLOR} leaving current settings"
					echo -e "${RED}  ->${RESETCOLOR} no operation\n"

		fi

	echo -e "${GREEN}==>${RESETCOLOR} TLP settings configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for config files"

		if [ -z "$TLP_DAILY_CONFIG_FILE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} Config files not found, skipping process"

				else

					if [ "$ALTERNATIVE_TLP_CONFIGS_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} replacing TLP settings with tlp.daily configuration"
						sudo cp ~/.config/openbox/obconfigs/tlp.daily /etc/default/tlp
						sudo cp ~/.config/openbox/obconfigs/tlp.daily /etc/tlp.conf
						echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/tlp.daily /etc/default/tlp"
						echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/tlp.daily /etc/tlp.conf\n"

							else

								echo -e "${RED}-->${RESETCOLOR} restoring default configuration"
								sudo cp ~/.config/openbox/obconfigs/tlp.default /etc/default/tlp
								sudo cp ~/.config/openbox/obconfigs/tlp.default /etc/tlp.conf
								echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/tlp.default /etc/default/tlp"
								echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/tlp.default /etc/tlp.conf\n"

					fi

		fi

	echo -e "${GREEN}==>${RESETCOLOR} CPU Power settings configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for config files"

		if [ -z "$CPUPOWER_DAILY_CONFIG_FILE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} Config files not found, skipping process"

				else

					if [ "$ALTERNATIVE_CPUPOWER_CONFIGS_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} replacing CPU Power settings with cpupower.daily configuration"
						sudo cp ~/.config/openbox/obconfigs/cpupower.daily /etc/default/cpupower
						echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/cpupower.daily /etc/default/cpupower\n"

							else

								echo -e "${RED}-->${RESETCOLOR} restoring default configuration"
								sudo cp ~/.config/openbox/obconfigs/cpupower.default /etc/default/cpupower
								echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/cpupower.default /etc/default/cpupower\n"

					fi

		fi

	echo -e "${GREEN}==>${RESETCOLOR} CPU Power service configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for cpupower"

		if [ -z "$CPUPOWER_SERVICE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} cpupower not installed, skipping process\n"

				else

					if [ "$ALTERNATIVE_CPU_SERVICE_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} enabling CPU Power service"

							if [ "$CPUPOWER_SERVICE_STATUS" = "active" ]; then

								echo -e "${RED}  ->${RESETCOLOR} CPU Power service already enabled\n"

									else

										sudo systemctl enable cpupower.service
										echo -e "${RED}  ->${RESETCOLOR} systemctl enable cpupower.service\n"

							fi

						else

							echo -e "${RED}-->${RESETCOLOR} disabling CPU Power service"

								if [ "$CPUPOWER_SERVICE_STATUS" = "inactive" ]; then

									echo -e "${RED}  ->${RESETCOLOR} CPU Power service already disabled\n"

										else

											sudo systemctl disable cpupower.service
											echo -e "${RED}  ->${RESETCOLOR} systemctl disable cpupower.service\n"		

								fi

				fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} RTC service configuration\n"

		if [ "$ALTERNATIVE_RTC_SERVICE_CHECK" = "TRUE" ]; then

			echo -e "${RED}-->${RESETCOLOR} enabling RTC service"

				if [ $RTC_SERVICE_STATUS -gt 64 ]; then

					echo -e "${RED}  ->${RESETCOLOR} CPU Power service already enabled\n"

						else

							sudo systemctl enable rtc.service
							echo -e "${RED}  ->${RESETCOLOR} systemctl enable rtc.service\n"

				fi

			else

				echo -e "${RED}-->${RESETCOLOR} disabling RTC service"

					if [ $RTC_SERVICE_STATUS -eq 64 ]; then

						echo -e "${RED}  ->${RESETCOLOR} RTC service already disabled\n"

							else

								sudo systemctl disable rtc.service
								echo -e "${RED}  ->${RESETCOLOR} systemctl disable rtc.service\n"		

					fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} HPET service configuration\n"

		if [ "$ALTERNATIVE_HPET_SERVICE_CHECK" = "TRUE" ]; then

			echo -e "${RED}-->${RESETCOLOR} enabling HPET service"

				if [ $HPET_SERVICE_STATUS -gt 64 ]; then

					echo -e "${RED}  ->${RESETCOLOR} HPET service already enabled\n"

						else

							sudo systemctl enable hpet.service
							echo -e "${RED}  ->${RESETCOLOR} systemctl enable hpet.service\n"

				fi

			else

				echo -e "${RED}-->${RESETCOLOR} disabling HPET service"

					if [ $HPET_SERVICE_STATUS -eq 64 ]; then

						echo -e "${RED}  ->${RESETCOLOR} HPET service already disabled\n"

							else

								sudo systemctl disable hpet.service
								echo -e "${RED}  ->${RESETCOLOR} systemctl disable hpet.service\n"		

					fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} UFW service configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for ufw"

		if [ -z "$UFW_SERVICE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} ufw not installed, skipping process\n"

				else

					if [ "$ALTERNATIVE_UFW_SERVICE_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} enabling UFW service"

							if [ "$UFW_SERVICE_STATUS" = "active" ]; then

								echo -e "${RED}  ->${RESETCOLOR} UFW service already enabled\n"

									else

										sudo systemctl enable ufw.service
										echo -e "${RED}  ->${RESETCOLOR} systemctl enable ufw.service\n"

							fi

						else

							echo -e "${RED}-->${RESETCOLOR} disabling UFW service"

								if [ "$UFW_SERVICE_STATUS" = "inactive" ]; then

									echo -e "${RED}  ->${RESETCOLOR} UFW service already disabled\n"

										else

											sudo systemctl disable ufw.service
											echo -e "${RED}  ->${RESETCOLOR} systemctl disable ufw.service\n"		

								fi

				fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} firewalld service configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for firewalld"

		if [ -z "$FIREWALLD_SERVICE" ]; then

			echo -e "${RED}  ->${RESETCOLOR} firewalld not installed, skipping process\n"

				else

					if [ "$ALTERNATIVE_FIREWALLD_SERVICE_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} enabling firewalld service"

							if [ "$FIREWALLD_SERVICE_STATUS" = "active" ]; then

								echo -e "${RED}  ->${RESETCOLOR} firewalld service already enabled\n"

									else

										sudo systemctl enable firewalld.service
										echo -e "${RED}  ->${RESETCOLOR} systemctl enable firewalld.service\n"

							fi

						else

							echo -e "${RED}-->${RESETCOLOR} disabling firewalld service"

								if [ "$FIREWALLD_SERVICE_STATUS" = "inactive" ]; then

									echo -e "${RED}  ->${RESETCOLOR} firewalld service already disabled\n"

										else

											sudo systemctl disable firewalld.service
											echo -e "${RED}  ->${RESETCOLOR} systemctl disable firewalld.service\n"		

								fi

				fi

		fi 2>/dev/null

	echo -e "${GREEN}==>${RESETCOLOR} GRUB settings configuration\n"
	echo -e "${RED}-->${RESETCOLOR} Looking for config files"

		if [ -z "$GRUB_DAILY_CONFIG_FILE" ]; then

			echo -e "${RED}-->${RESETCOLOR} Config files not found, skipping process"

				else

					if [ "$ALTERNATIVE_GRUB_CONFIGS_CHECK" = "TRUE" ]; then

						echo -e "${RED}-->${RESETCOLOR} replacing GRUB settings with grub.daily configuration"
						sudo cp ~/.config/openbox/obconfigs/grub.daily /etc/default/grub
						echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/grub.daily /etc/default/grub\n"

						else

							echo -e "${RED}-->${RESETCOLOR} restoring default configuration"
							sudo cp ~/.config/openbox/obconfigs/grub.default /etc/default/grub
							echo -e "${RED}  ->${RESETCOLOR} cp ~/.config/openbox/obconfigs/grub.default /etc/default/grub\n"

					fi

			echo -e "${GREEN}==>${RESETCOLOR} Updating GRUB\n"
			sudo update-grub; sleep 1

		fi

	echo -e "\n:: System settings changed to ${GREEN}Alternative mode${RESETCOLOR}."
	echo -e ":: Do you want to shutdown [${YELLOW}pP${RESETCOLOR}] reboot system [${YELLOW}rR${RESETCOLOR}] logout [${YELLOW}lL${RESETCOLOR}] or continue [${YELLOW}cC${RESETCOLOR}] ?"

		while :

			do

				read -r -p ":: Answer: " input

					case $input in

					    [rR] | [rR] ) systemctl reboot ;;
					    [cC] | [cC] ) echo "Ok"; exit 0 ;;
						[pP] | [pP] ) systemctl poweroff ;;
						[lL] | [lL] ) openbox --exit ;;
				   		*)

						echo -e ":: ${RED}Invalid input...${RESETCOLOR}"

						;;

					esac

			done

}

ver() {

	echo -e "\n    ${WHITE}Version 2.5${RESETCOLOR}"

}

version() {

	echo -e "\n    ${WHITE}Version 2.5${RESETCOLOR}"

}

case "$1" in

	hperf) hperf ;;
	daily) daily ;;
	studio) studio ;;
	altr) altr ;;
	ver) ver ;;
	version) version ;;
	*)

	echo -e "
${GREEN}	RecBox Script

	Usage:
${RESETCOLOR}	rb-workflow.sh${YELLOW} [${RED} OPTION${YELLOW} ]

${GREEN}	Options:

${YELLOW}	>${RED} hperf ${YELLOW}       -${RESETCOLOR} disabling redshift, compton,
			 power-manager,polkit agent,
			 xfsettingsd, thunar daemon,
			 skippy-xd daemon\n
${YELLOW}	>${RED} daily ${YELLOW}       -${RESETCOLOR} changing settings to Daily
${YELLOW}	>${RED} studio ${YELLOW}      -${RESETCOLOR} changing settings to Studio
${YELLOW}	>${RED} altr ${YELLOW}        -${RESETCOLOR} changing settings to Alternative

${YELLOW}	>${RED} ver, version${YELLOW} -${RESETCOLOR} show script version
" >&2
exit 1
;;
esac

echo -e $RESETCOLOR
exit 0
