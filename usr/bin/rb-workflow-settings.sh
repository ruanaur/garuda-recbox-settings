#!/usr/bin/sh

# VERSION=1.3

MAIN_WINDOW_TITLE="RecBox Workflow Settings"
MAIN_WINDOW_TEXT="Select which mode you want to configure.\n"
DAILY_WINDOW_TITLE="Workflow Settings - Daily"
DAILY_WINDOW_TEXT="Choose which options you want to use in Daily mode.\n"
STUDIO_WINDOW_TITLE="Workflow Settings - Studio"
STUDIO_WINDOW_TEXT="Choose which options you want to use in Studio mode.\n"
ALTERNATIVE_WINDOW_TITLE="Workflow Settings - Alternative"
ALTERNATIVE_WINDOW_TEXT="Choose which options you want to use in Alternative mode.\n"
PICK_COLUMN_TITLE="Pick"
MODE_COLUMN_TITLE="Mode"
OPTIONS_COLUMN_TITLE="Options"
DESCRIPTION_COLUMN_TITLE="Description"
OK_BUTTON="Apply"
CANCEL_BUTTON="Close"
EXTRA_BUTTON_BACK="Back"
DAILY_EXTRA_BUTTON_DEFAULT="Default"
STUDIO_EXTRA_BUTTON_DEFAULT="Default"
ALTERNATIVE_EXTRA_BUTTON_DEFAULT="Default"
ZEN_MAIN_MENU="zenity --list --radiolist --width=560 --height=172 --hide-header"
ZEN_SETTINGS_MENU="zenity --list --checklist --width=940 --height=409 --hide-header"

DAILY_JACK_CHECK=$(grep "DAILY_JACK_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_CADENCE_CHECK=$(grep "DAILY_CADENCE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_ARANDR_CHECK=$(grep "DAILY_ARANDR_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_XFSETTINGSD_CHECK=$(grep "DAILY_XFSETTINGSD_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_COMPOSITOR_CHECK=$(grep "DAILY_COMPOSITOR_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_THUNAR_DAEMON_CHECK=$(grep "DAILY_THUNAR_DAEMON_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_PCMANFM_CHECK=$(grep 'DAILY_PCMANFM_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_NITROGEN_CHECK=$(grep "DAILY_NITROGEN_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_TINT2_PANEL_CHECK=$(grep "DAILY_TINT2_PANEL_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_POLYBAR_PANEL_CHECK=$(grep "DAILY_POLYBAR_PANEL_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_CONKY_CHECK=$(grep "DAILY_CONKY_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_VOLUMEICON_CHECK=$(grep "DAILY_VOLUMEICON_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_SKIPPY_DAEMON_CHECK=$(grep "DAILY_SKIPPY_DAEMON_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_SUPERKEY_CHECK=$(grep 'DAILY_SUPERKEY_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_POLICYKIT_CHECK=$(grep "DAILY_POLICYKIT_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_SIDE_SHORTCUTS_CHECK=$(grep "DAILY_SIDE_SHORTCUTS_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_NUMLOCK_CHECK=$(grep "DAILY_NUMLOCK_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_OBANIMATIONS_CHECK=$(grep "DAILY_OBANIMATIONS_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_COMPOSITOR_SHADOW_CHECK=$(grep "DAILY_COMPOSITOR_SHADOW_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_ALSA_CONFIGS_CHECK=$(grep "DAILY_ALSA_CONFIGS_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_CPUPOWER_CONFIGS_CHECK=$(grep "DAILY_CPUPOWER_CONFIGS_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_TLP_CONFIGS_CHECK=$(grep "DAILY_TLP_CONFIGS_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_CPU_SERVICE_CHECK=$(grep "DAILY_CPU_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_RTC_SERVICE_CHECK=$(grep "DAILY_RTC_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_HPET_SERVICE_CHECK=$(grep "DAILY_HPET_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_UFW_SERVICE_CHECK=$(grep "DAILY_UFW_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_FIREWALLD_SERVICE_CHECK=$(grep "DAILY_FIREWALLD_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
DAILY_GRUB_CONFIGS_CHECK=$(grep "DAILY_GRUB_CONFIGS_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)

STUDIO_JACK_CHECK=$(grep "STUDIO_JACK_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_CADENCE_CHECK=$(grep 'STUDIO_CADENCE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_ARANDR_CHECK=$(grep 'STUDIO_ARANDR_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_XFSETTINGSD_CHECK=$(grep 'STUDIO_XFSETTINGSD_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_COMPOSITOR_CHECK=$(grep 'STUDIO_COMPOSITOR_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_THUNAR_DAEMON_CHECK=$(grep 'STUDIO_THUNAR_DAEMON_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_PCMANFM_CHECK=$(grep 'STUDIO_PCMANFM_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_NITROGEN_CHECK=$(grep 'STUDIO_NITROGEN_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_TINT2_PANEL_CHECK=$(grep 'STUDIO_TINT2_PANEL_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_POLYBAR_PANEL_CHECK=$(grep 'STUDIO_POLYBAR_PANEL_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_CONKY_CHECK=$(grep 'STUDIO_CONKY_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_VOLUMEICON_CHECK=$(grep 'STUDIO_VOLUMEICON_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_SKIPPY_DAEMON_CHECK=$(grep 'STUDIO_SKIPPY_DAEMON_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_SUPERKEY_CHECK=$(grep 'STUDIO_SUPERKEY_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_POLICYKIT_CHECK=$(grep 'STUDIO_POLICYKIT_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_SIDE_SHORTCUTS_CHECK=$(grep 'STUDIO_SIDE_SHORTCUTS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_NUMLOCK_CHECK=$(grep 'STUDIO_NUMLOCK_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_OBANIMATIONS_CHECK=$(grep 'STUDIO_OBANIMATIONS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_COMPOSITOR_SHADOW_CHECK=$(grep 'STUDIO_COMPOSITOR_SHADOW_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_ALSA_CONFIGS_CHECK=$(grep 'STUDIO_ALSA_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_CPUPOWER_CONFIGS_CHECK=$(grep 'STUDIO_CPUPOWER_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_TLP_CONFIGS_CHECK=$(grep 'STUDIO_TLP_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_CPU_SERVICE_CHECK=$(grep 'STUDIO_CPU_SERVICE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_RTC_SERVICE_CHECK=$(grep 'STUDIO_RTC_SERVICE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_HPET_SERVICE_CHECK=$(grep 'STUDIO_HPET_SERVICE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_UFW_SERVICE_CHECK=$(grep "STUDIO_UFW_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_FIREWALLD_SERVICE_CHECK=$(grep "STUDIO_FIREWALLD_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
STUDIO_GRUB_CONFIGS_CHECK=$(grep 'STUDIO_GRUB_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)

ALTERNATIVE_JACK_CHECK=$(grep "ALTERNATIVE_JACK_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_CADENCE_CHECK=$(grep 'ALTERNATIVE_CADENCE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_ARANDR_CHECK=$(grep 'ALTERNATIVE_ARANDR_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_XFSETTINGSD_CHECK=$(grep 'ALTERNATIVE_XFSETTINGSD_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_COMPOSITOR_CHECK=$(grep 'ALTERNATIVE_COMPOSITOR_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_THUNAR_DAEMON_CHECK=$(grep 'ALTERNATIVE_THUNAR_DAEMON_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_PCMANFM_CHECK=$(grep 'ALTERNATIVE_PCMANFM_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_NITROGEN_CHECK=$(grep 'ALTERNATIVE_NITROGEN_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_TINT2_PANEL_CHECK=$(grep 'ALTERNATIVE_TINT2_PANEL_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_POLYBAR_PANEL_CHECK=$(grep 'ALTERNATIVE_POLYBAR_PANEL_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_CONKY_CHECK=$(grep 'ALTERNATIVE_CONKY_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_VOLUMEICON_CHECK=$(grep 'ALTERNATIVE_VOLUMEICON_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_SKIPPY_DAEMON_CHECK=$(grep 'ALTERNATIVE_SKIPPY_DAEMON_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_SUPERKEY_CHECK=$(grep 'ALTERNATIVE_SUPERKEY_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_POLICYKIT_CHECK=$(grep 'ALTERNATIVE_POLICYKIT_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_SIDE_SHORTCUTS_CHECK=$(grep 'ALTERNATIVE_SIDE_SHORTCUTS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_NUMLOCK_CHECK=$(grep 'ALTERNATIVE_NUMLOCK_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_OBANIMATIONS_CHECK=$(grep 'ALTERNATIVE_OBANIMATIONS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_COMPOSITOR_SHADOW_CHECK=$(grep 'ALTERNATIVE_COMPOSITOR_SHADOW_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_ALSA_CONFIGS_CHECK=$(grep 'ALTERNATIVE_ALSA_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_CPUPOWER_CONFIGS_CHECK=$(grep 'ALTERNATIVE_CPUPOWER_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_TLP_CONFIGS_CHECK=$(grep 'ALTERNATIVE_TLP_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_CPU_SERVICE_CHECK=$(grep 'ALTERNATIVE_CPU_SERVICE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_RTC_SERVICE_CHECK=$(grep 'ALTERNATIVE_RTC_SERVICE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_HPET_SERVICE_CHECK=$(grep 'ALTERNATIVE_HPET_SERVICE_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_UFW_SERVICE_CHECK=$(grep "ALTERNATIVE_UFW_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_FIREWALLD_SERVICE_CHECK=$(grep "ALTERNATIVE_FIREWALLD_SERVICE_CHECK" $HOME/.config/workflow-settings/config | cut -d '"' -f2)
ALTERNATIVE_GRUB_CONFIGS_CHECK=$(grep 'ALTERNATIVE_GRUB_CONFIGS_CHECK' $HOME/.config/workflow-settings/config | cut -d '"' -f2)

JACK_SESSION="User custom JACK script"
CADENCE_SESSION="Enable Cadence Session on start"
ARANDR="Screen setup with ARandR (XFCE alternative)"
XFSETTINGSD="Enabele XFCE settings (xfsettingsd)"
COMPOSITOR="Enable Garuda Compositor (Compton)"
THUNAR_DAEMON="Launch Thunar in daemon mode for faster startup"
PCMANFM="Use PCManFM as wallpaper manager (desktop icons)"
NITROGEN="Use Nitrogen as wallpaper manager (no desktop icons)"
TINT2_PANEL="Launch tint2 panel"
POLYBAR_PANEL="Launch polybar panel"
CONKY="Launch Conky"
VOLUMEICON="Use Volumeicon"
SKIPPY_DAEMON="Launch Skippy window switcher daemon"
SUPERKEY="Use Super key for menu"
POLICYKIT="Enable PolicyKit authentication"
SIDE_SHORTCUTS="Side shortcuts for file managers"
NUMLOCK="Enable Numlock"
OBANIMATIONS="Enable Openbox animations"
COMPOSITOR_SHADOW="Enable Desktop shadows (picom)"
ALSA_CONFIGS="ALSA config files swap"
CPUPOWER_CONFIGS="CPU Power config files swap"
TLP_CONFIGS="TLP config files swap"
CPU_SERVICE="CPU Power service"
RTC_SERVICE="RTC service"
HPET_SERVICE="HPET service"
UFW_SERVICE="UFW service"
FIREWALLD_SERVICE="firewalld service"
GRUB_CONFIGS="GRUB config files swap"

JACK_SESSION_INFO="~/.config/openbox/autostart [ start_jack.sh ]"
CADENCE_SESSION_INFO="~/.config/openbox/autostart  [ cadence-session-start -s ]"
ARANDR_INFO="~/.config/openbox/autostart  [ ~/.screenlayout/dual-head.sh ]"
XFSETTINGSD_INFO="~/.config/openbox/autostart  [ xfsettingsd ]"
COMPOSITOR_INFO="~/.config/openbox/autostart  [ garuda-compositor --start ]"
THUNAR_DAEMON_INFO="~/.config/openbox/autostart  [ thunar --daemon ]"
PCMANFM_INFO="~/.config/openbox/autostart  [pcmanfm --desktop]"
NITROGEN_INFO="~/.config/openbox/autostart  [ nitrogen --restore ]"
TINT2_PANEL_INFO="~/.config/openbox/autostart  [ garuda-tint2-session ]"
POLYBAR_PANEL_INFO="~/.config/openbox/autostart  [ garuda-polybar-session ]"
CONKY_INFO="~/.config/openbox/autostart  [ garuda-conky-session ]"
VOLUMEICON_INFO="~/.config/openbox/autostart  [ volumeicon ]"
SKIPPY_DAEMON_INFO="~/.config/ob-autostart/config  [ skippy-xd --config ~/.config/skippy-xd/skippy-xd.rc --start-daemon ]"
SUPERKEY_INFO="~/.config/openbox/autostart  [ xcape -e 'Super_L=Alt_L|F1' ]"
POLICYKIT_INFO="~/.config/openbox/autostart  [ polkit-gnome-authentication-agent-1 ]"
SIDE_SHORTCUTS_INFO="~/.config/openbox/autostart  [ xdg-user-dirs-gtk-update ]"
NUMLOCK_INFO="~/.config/openbox/autostart  [ numlockx ]"
OBANIMATIONS_INFO="~/.config/openbox/rc.xml  [ <animateIconify>no</animateIconify> ]"
COMPOSITOR_SHADOW_INFO="~/.config/picom/picom.conf  [ shadow = true ]"
ALSA_CONFIGS_INFO="~/.asoundrc  [ cp ~/.config/openbox/obconfigs/asoundrc.studio ~/.asoundrc ]"
CPUPOWER_CONFIGS_INFO="/etc/default/cpupower  [ cp ~/.config/openbox/obconfigs/cpupower.studio /etc/default/cpupower ]"
TLP_CONFIGS_INFO="/etc/default/tlp [ cp ~/.config/openbox/obconfigs/tlp.studio /etc/default/tlp ]"
CPU_SERVICE_INFO="enable cpupower.service  [ systemctl enable cpupower.service ]"
RTC_SERVICE_INFO="enable rtc.service  [ systemctl enable rtc.service ]"
HPET_SERVICE_INFO="enable hpet.service  [ systemctl enable hpet.service ]"
UFW_SERVICE_INFO="enable ufw.service  [ systemctl enable ufw.service ]"
FIREWALLD_SERVICE_INFO="enable firewalld.service  [ systemctl enable firewalld.service ]"
GRUB_CONFIGS_INFO="/etc/default/grub  [ cp ~/.config/openbox/obconfigs/grub.mode /etc/default/grub ]"

	BOX=$($ZEN_MAIN_MENU --title="$MAIN_WINDOW_TITLE" --text="$MAIN_WINDOW_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
		--column="$PICK_COLUMN_TITLE" --column="$MODE_COLUMN_TITLE" --column="$DESCRIPTION_COLUMN_TITLE" \
		TRUE "Daily Mode" "This mode is for day to day use." \
		FALSE "Studio Mode" "This mode is set for recording, mixing and mastering purposes." \
		FALSE "Alternative Mode" "This mode is can be used for in both Daily and Studio Mode." \
		2>/dev/null)

	case $? in

		0 )

			if [ "$BOX" = "Daily Mode" ]; then

				BOX=$($ZEN_SETTINGS_MENU --title="$DAILY_WINDOW_TITLE" --text="$DAILY_WINDOW_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --extra-button="$DAILY_EXTRA_BUTTON_DEFAULT" \
					--column="$PICK_COLUMN_TITLE" --column="$OPTIONS_COLUMN_TITLE" --column="$DESCRIPTION_COLUMN_TITLE" --separator=" " \
					"$DAILY_JACK_CHECK" "$JACK_SESSION" "$JACK_SESSION_INFO" \
					"$DAILY_CADENCE_CHECK" "$CADENCE_SESSION" "$CADENCE_SESSION_INFO" \
					"$DAILY_ARANDR_CHECK" "$ARANDR" "$ARANDR_INFO" \
					"$DAILY_XFSETTINGSD_CHECK" "$XFSETTINGSD" "$XFSETTINGSD_INFO" \
					"$DAILY_COMPOSITOR_CHECK" "$COMPOSITOR" "$COMPOSITOR_INFO" \
					"$DAILY_THUNAR_DAEMON_CHECK" "$THUNAR_DAEMON" "$THUNAR_DAEMON_INFO" \
					"$DAILY_PCMANFM_CHECK" "$PCMANFM" "$PCMANFM_INFO" \
					"$DAILY_NITROGEN_CHECK" "$NITROGEN" "$NITROGEN_INFO" \
					"$DAILY_TINT2_PANEL_CHECK" "$TINT2_PANEL" "$TINT2_PANEL_INFO" \
					"$DAILY_POLYBAR_PANEL_CHECK" "$POLYBAR_PANEL" "$POLYBAR_PANEL_INFO" \
					"$DAILY_CONKY_CHECK" "$CONKY" "$CONKY_INFO" \
					"$DAILY_VOLUMEICON_CHECK" "$VOLUMEICON" "$VOLUMEICON_INFO" \
					"$DAILY_SKIPPY_DAEMON_CHECK" "$SKIPPY_DAEMON" "$SKIPPY_DAEMON_INFO" \
					"$DAILY_SUPERKEY_CHECK" "$SUPERKEY" "$SUPERKEY_INFO" \
					"$DAILY_POLICYKIT_CHECK" "$POLICYKIT" "$POLICYKIT_INFO" \
					"$DAILY_SIDE_SHORTCUTS_CHECK" "$SIDE_SHORTCUTS" "$SIDE_SHORTCUTS_INFO" \
					"$DAILY_NUMLOCK_CHECK" "$NUMLOCK" "$NUMLOCK_INFO" \
					"$DAILY_OBANIMATIONS_CHECK" "$OBANIMATIONS" "$OBANIMATIONS_INFO" \
					"$DAILY_COMPOSITOR_SHADOW_CHECK" "$COMPOSITOR_SHADOW" "$COMPOSITOR_SHADOW_INFO" \
					"$DAILY_ALSA_CONFIGS_CHECK" "$ALSA_CONFIGS" "$ALSA_CONFIGS_INFO" \
					"$DAILY_CPUPOWER_CONFIGS_CHECK" "$CPUPOWER_CONFIGS" "$CPUPOWER_CONFIGS_INFO" \
					"$DAILY_TLP_CONFIGS_CHECK" "$TLP_CONFIGS" "$TLP_CONFIGS_INFO" \
					"$DAILY_CPU_SERVICE_CHECK" "$CPU_SERVICE" "$CPU_SERVICE_INFO" \
					"$DAILY_RTC_SERVICE_CHECK" "$RTC_SERVICE" "$RTC_SERVICE_INFO" \
					"$DAILY_HPET_SERVICE_CHECK" "$HPET_SERVICE" "$HPET_SERVICE_INFO" \
					"$DAILY_UFW_SERVICE_CHECK" "$UFW_SERVICE" "$UFW_SERVICE_INFO" \
					"$DAILY_FIREWALLD_SERVICE_CHECK" "$FIREWALLD_SERVICE" "$FIREWALLD_SERVICE_INFO" \
					"$DAILY_GRUB_CONFIGS_CHECK" "$GRUB_CONFIGS" "$GRUB_CONFIGS_INFO" \
					--multiple 2>/dev/null)

					case $? in

						0 )

							mkdir -p /tmp/workflow-settings; echo $BOX > /tmp/workflow-settings/.cache							

							if ! echo "$JACK_SESSION" | grep -q "$JACK_SESSION" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_JACK_CHECK="TRUE"/DAILY_JACK_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_JACK_CHECK="FALSE"/DAILY_JACK_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$CADENCE_SESSION" | grep -q "$CADENCE_SESSION" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_CADENCE_CHECK="TRUE"/DAILY_CADENCE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_CADENCE_CHECK="FALSE"/DAILY_CADENCE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$ARANDR" | grep -q "$ARANDR" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_ARANDR_CHECK="TRUE"/DAILY_ARANDR_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_ARANDR_CHECK="FALSE"/DAILY_ARANDR_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$XFSETTINGSD" | grep -q "$XFSETTINGSD" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_XFSETTINGSD_CHECK="TRUE"/DAILY_XFSETTINGSD_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_XFSETTINGSD_CHECK="FALSE"/DAILY_XFSETTINGSD_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$COMPOSITOR" | grep -q "$COMPOSITOR" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_COMPOSITOR_CHECK="TRUE"/DAILY_COMPOSITOR_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_COMPOSITOR_CHECK="FALSE"/DAILY_COMPOSITOR_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$THUNAR_DAEMON" | grep -q "$THUNAR_DAEMON" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_THUNAR_DAEMON_CHECK="TRUE"/DAILY_THUNAR_DAEMON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_THUNAR_DAEMON_CHECK="FALSE"/DAILY_THUNAR_DAEMON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$PCMANFM" | grep -q "$PCMANFM" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_PCMANFM_CHECK="TRUE"/DAILY_PCMANFM_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_PCMANFM_CHECK="FALSE"/DAILY_PCMANFM_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$NITROGEN" | grep -q "$NITROGEN" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_NITROGEN_CHECK="TRUE"/DAILY_NITROGEN_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
							else

								sed -i 's/DAILY_NITROGEN_CHECK="FALSE"/DAILY_NITROGEN_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$TINT2_PANEL" | grep -q "$TINT2_PANEL" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_TINT2_PANEL_CHECK="TRUE"/DAILY_TINT2_PANEL_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_TINT2_PANEL_CHECK="FALSE"/DAILY_TINT2_PANEL_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$POLYBAR_PANEL" | grep -q "$POLYBAR_PANEL" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_POLYBAR_PANEL_CHECK="TRUE"/DAILY_POLYBAR_PANEL_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_POLYBAR_PANEL_CHECK="FALSE"/DAILY_POLYBAR_PANEL_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$CONKY" | grep -q "$CONKY" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_CONKY_CHECK="TRUE"/DAILY_CONKY_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_CONKY_CHECK="FALSE"/DAILY_CONKY_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$VOLUMEICON" | grep -q "$VOLUMEICON" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_VOLUMEICON_CHECK="TRUE"/DAILY_VOLUMEICON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
							else

								sed -i 's/DAILY_VOLUMEICON_CHECK="FALSE"/DAILY_VOLUMEICON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$SKIPPY_DAEMON" | grep -q "$SKIPPY_DAEMON" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_SKIPPY_DAEMON_CHECK="TRUE"/DAILY_SKIPPY_DAEMON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_SKIPPY_DAEMON_CHECK="FALSE"/DAILY_SKIPPY_DAEMON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$SUPERKEY" | grep -q "$SUPERKEY" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_SUPERKEY_CHECK="TRUE"/DAILY_SUPERKEY_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_SUPERKEY_CHECK="FALSE"/DAILY_SUPERKEY_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$POLICYKIT" | grep -q "$POLICYKIT" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_POLICYKIT_CHECK="TRUE"/DAILY_POLICYKIT_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_POLICYKIT_CHECK="FALSE"/DAILY_POLICYKIT_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$SIDE_SHORTCUTS" | grep -q "$SIDE_SHORTCUTS" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_SIDE_SHORTCUTS_CHECK="TRUE"/DAILY_SIDE_SHORTCUTS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_SIDE_SHORTCUTS_CHECK="FALSE"/DAILY_SIDE_SHORTCUTS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$NUMLOCK" | grep -q "$NUMLOCK" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_NUMLOCK_CHECK="TRUE"/DAILY_NUMLOCK_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_NUMLOCK_CHECK="FALSE"/DAILY_NUMLOCK_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$OBANIMATIONS" | grep -q "$OBANIMATIONS" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_OBANIMATIONS_CHECK="TRUE"/DAILY_OBANIMATIONS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_OBANIMATIONS_CHECK="FALSE"/DAILY_OBANIMATIONS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$COMPOSITOR_SHADOW" | grep -q "$COMPOSITOR_SHADOW" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_COMPOSITOR_SHADOW_CHECK="TRUE"/DAILY_COMPOSITOR_SHADOW_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_COMPOSITOR_SHADOW_CHECK="FALSE"/DAILY_COMPOSITOR_SHADOW_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
							fi

							if ! echo "$ALSA_CONFIGS" | grep -q "$ALSA_CONFIGS" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_ALSA_CONFIGS_CHECK="TRUE"/DAILY_ALSA_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_ALSA_CONFIGS_CHECK="FALSE"/DAILY_ALSA_CONFIGS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
							fi

							if ! echo "$CPUPOWER_CONFIGS" | grep -q "$CPUPOWER_CONFIGS" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_CPUPOWER_CONFIGS_CHECK="TRUE"/DAILY_CPUPOWER_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_CPUPOWER_CONFIGS_CHECK="FALSE"/DAILY_CPUPOWER_CONFIGS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$TLP_CONFIGS" | grep -q "$TLP_CONFIGS" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_TLP_CONFIGS_CHECK="TRUE"/DAILY_TLP_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_TLP_CONFIGS_CHECK="FALSE"/DAILY_TLP_CONFIGS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$CPU_SERVICE" | grep -q "$CPU_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_CPU_SERVICE_CHECK="TRUE"/DAILY_CPU_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_CPU_SERVICE_CHECK="FALSE"/DAILY_CPU_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$RTC_SERVICE" | grep -q "$RTC_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_RTC_SERVICE_CHECK="TRUE"/DAILY_RTC_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_RTC_SERVICE_CHECK="FALSE"/DAILY_RTC_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$HPET_SERVICE" | grep -q "$HPET_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_HPET_SERVICE_CHECK="TRUE"/DAILY_HPET_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_HPET_SERVICE_CHECK="FALSE"/DAILY_HPET_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$UFW_SERVICE" | grep -q "$UFW_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_UFW_SERVICE_CHECK="TRUE"/DAILY_UFW_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_UFW_SERVICE_CHECK="FALSE"/DAILY_UFW_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$FIREWALLD_SERVICE" | grep -q "$FIREWALLD_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_FIREWALLD_SERVICE_CHECK="TRUE"/DAILY_FIREWALLD_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_FIREWALLD_SERVICE_CHECK="FALSE"/DAILY_FIREWALLD_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$GRUB_CONFIGS" | grep -q "$GRUB_CONFIGS" /tmp/workflow-settings/.cache; then

								sed -i 's/DAILY_GRUB_CONFIGS_CHECK="TRUE"/DAILY_GRUB_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/DAILY_GRUB_CONFIGS_CHECK="FALSE"/DAILY_GRUB_CONFIGS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							exec rb-workflow-settings.sh

						;;

						1 )

							if [ "$BOX" = $EXTRA_BUTTON_BACK ]; then

								exec rb-workflow-settings.sh

							elif [ "$BOX" = $DAILY_EXTRA_BUTTON_DEFAULT ]; then

								sed -i 's/DAILY_JACK_CHECK="TRUE"/DAILY_JACK_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_CADENCE_CHECK="TRUE"/DAILY_CADENCE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_ARANDR_CHECK="TRUE"/DAILY_ARANDR_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_XFSETTINGSD_CHECK="FALSE"/DAILY_XFSETTINGSD_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_COMPOSITOR_CHECK="FALSE"/DAILY_COMPOSITOR_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_THUNAR_DAEMON_CHECK="FALSE"/DAILY_THUNAR_DAEMON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_PCMANFM_CHECK="TRUE"/DAILY_PCMANFM_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_NITROGEN_CHECK="FALSE"/DAILY_NITROGEN_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_TINT2_PANEL_CHECK="FALSE"/DAILY_TINT2_PANEL_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_POLYBAR_PANEL_CHECK="TRUE"/DAILY_POLYBAR_PANEL_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_CONKY_CHECK="TRUE"/DAILY_CONKY_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_VOLUMEICON_CHECK="TRUE"/DAILY_VOLUMEICON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_SKIPPY_DAEMON_CHECK="FALSE"/DAILY_SKIPPY_DAEMON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_SUPERKEY_CHECK="TRUE"/DAILY_SUPERKEY_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_POLICYKIT_CHECK="FALSE"/DAILY_POLICYKIT_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_SIDE_SHORTCUTS_CHECK="FALSE"/DAILY_SIDE_SHORTCUTS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_NUMLOCK_CHECK="TRUE"/DAILY_NUMLOCK_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_OBANIMATIONS_CHECK="FALSE"/DAILY_OBANIMATIONS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_COMPOSITOR_SHADOW_CHECK="FALSE"/DAILY_COMPOSITOR_SHADOW_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_ALSA_CONFIGS_CHECK="TRUE"/DAILY_ALSA_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_CPUPOWER_CONFIGS_CHECK="TRUE"/DAILY_CPUPOWER_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_TLP_CONFIGS_CHECK="TRUE"/DAILY_TLP_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_CPU_SERVICE_CHECK="TRUE"/DAILY_CPU_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_RTC_SERVICE_CHECK="TRUE"/DAILY_RTC_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_HPET_SERVICE_CHECK="TRUE"/DAILY_HPET_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_UFW_SERVICE_CHECK="TRUE"/DAILY_UFW_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_FIREWALLD_SERVICE_CHECK="TRUE"/DAILY_FIREWALLD_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/DAILY_GRUB_CONFIGS_CHECK="FALSE"/DAILY_GRUB_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sleep 0.5; zenity --info --text="Daily session restored to default." --width="260"; exec rb-workflow-settings.sh

							fi

						;;

						* )

					esac

			elif [ "$BOX" = "Studio Mode" ]; then

				BOX=$($ZEN_SETTINGS_MENU --title="$STUDIO_WINDOW_TITLE" --text="$STUDIO_WINDOW_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --extra-button="$STUDIO_EXTRA_BUTTON_DEFAULT" \
					--column="$PICK_COLUMN_TITLE" --column="$OPTIONS_COLUMN_TITLE" --column="$DESCRIPTION_COLUMN_TITLE" --separator=" " \
					"$STUDIO_JACK_CHECK" "$JACK_SESSION" "$JACK_SESSION_INFO" \
					"$STUDIO_CADENCE_CHECK" "$CADENCE_SESSION" "$CADENCE_SESSION_INFO" \
					"$STUDIO_ARANDR_CHECK" "$ARANDR" "$ARANDR_INFO" \
					"$STUDIO_XFSETTINGSD_CHECK" "$XFSETTINGSD" "$XFSETTINGSD_INFO" \
					"$STUDIO_COMPOSITOR_CHECK" "$COMPOSITOR" "$COMPOSITOR_INFO" \
					"$STUDIO_THUNAR_DAEMON_CHECK" "$THUNAR_DAEMON" "$THUNAR_DAEMON_INFO" \
					"$STUDIO_PCMANFM_CHECK" "$PCMANFM" "$PCMANFM_INFO" \
					"$STUDIO_NITROGEN_CHECK" "$NITROGEN" "$NITROGEN_INFO" \
					"$STUDIO_TINT2_PANEL_CHECK" "$TINT2_PANEL" "$TINT2_PANEL_INFO" \
					"$STUDIO_POLYBAR_PANEL_CHECK" "$POLYBAR_PANEL" "$POLYBAR_PANEL_INFO" \
					"$STUDIO_CONKY_CHECK" "$CONKY" "$CONKY_INFO" \
					"$STUDIO_VOLUMEICON_CHECK" "$VOLUMEICON" "$VOLUMEICON_INFO" \
					"$STUDIO_SKIPPY_DAEMON_CHECK" "$SKIPPY_DAEMON" "$SKIPPY_DAEMON_INFO" \
					"$STUDIO_SUPERKEY_CHECK" "$SUPERKEY" "$SUPERKEY_INFO" \
					"$STUDIO_POLICYKIT_CHECK" "$POLICYKIT" "$POLICYKIT_INFO" \
					"$STUDIO_SIDE_SHORTCUTS_CHECK" "$SIDE_SHORTCUTS" "$SIDE_SHORTCUTS_INFO" \
					"$STUDIO_NUMLOCK_CHECK" "$NUMLOCK" "$NUMLOCK_INFO" \
					"$STUDIO_OBANIMATIONS_CHECK" "$OBANIMATIONS" "$OBANIMATIONS_INFO" \
					"$STUDIO_COMPOSITOR_SHADOW_CHECK" "$COMPOSITOR_SHADOW" "$COMPOSITOR_SHADOW_INFO" \
					"$STUDIO_ALSA_CONFIGS_CHECK" "$ALSA_CONFIGS" "$ALSA_CONFIGS_INFO" \
					"$STUDIO_CPUPOWER_CONFIGS_CHECK" "$CPUPOWER_CONFIGS" "$CPUPOWER_CONFIGS_INFO" \
					"$STUDIO_TLP_CONFIGS_CHECK" "$TLP_CONFIGS" "$TLP_CONFIGS_INFO" \
					"$STUDIO_CPU_SERVICE_CHECK" "$CPU_SERVICE" "$CPU_SERVICE_INFO" \
					"$STUDIO_RTC_SERVICE_CHECK" "$RTC_SERVICE" "$RTC_SERVICE_INFO" \
					"$STUDIO_HPET_SERVICE_CHECK" "$HPET_SERVICE" "$HPET_SERVICE_INFO" \
					"$STUDIO_UFW_SERVICE_CHECK" "$UFW_SERVICE" "$UFW_SERVICE_INFO" \
					"$STUDIO_FIREWALLD_SERVICE_CHECK" "$FIREWALLD_SERVICE" "$FIREWALLD_SERVICE_INFO" \
					"$STUDIO_GRUB_CONFIGS_CHECK" "$GRUB_CONFIGS" "$GRUB_CONFIGS_INFO" \
					--multiple 2>/dev/null)

					case $? in

						0 )

							mkdir -p /tmp/workflow-settings; echo $BOX > /tmp/workflow-settings/.cache							

							if ! echo "$JACK_SESSION" | grep -q "$JACK_SESSION" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_JACK_CHECK="TRUE"/STUDIO_JACK_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_JACK_CHECK="FALSE"/STUDIO_JACK_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$CADENCE_SESSION" | grep -q "$CADENCE_SESSION" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_CADENCE_CHECK="TRUE"/STUDIO_CADENCE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_CADENCE_CHECK="FALSE"/STUDIO_CADENCE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$ARANDR" | grep -q "$ARANDR" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_ARANDR_CHECK="TRUE"/STUDIO_ARANDR_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_ARANDR_CHECK="FALSE"/STUDIO_ARANDR_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$XFSETTINGSD" | grep -q "$XFSETTINGSD" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_XFSETTINGSD_CHECK="TRUE"/STUDIO_XFSETTINGSD_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_XFSETTINGSD_CHECK="FALSE"/STUDIO_XFSETTINGSD_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$COMPOSITOR" | grep -q "$COMPOSITOR" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_COMPOSITOR_CHECK="TRUE"/STUDIO_COMPOSITOR_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_COMPOSITOR_CHECK="FALSE"/STUDIO_COMPOSITOR_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$THUNAR_DAEMON" | grep -q "$THUNAR_DAEMON" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_THUNAR_DAEMON_CHECK="TRUE"/STUDIO_THUNAR_DAEMON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_THUNAR_DAEMON_CHECK="FALSE"/STUDIO_THUNAR_DAEMON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$PCMANFM" | grep -q "$PCMANFM" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_PCMANFM_CHECK="TRUE"/STUDIO_PCMANFM_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_PCMANFM_CHECK="FALSE"/STUDIO_PCMANFM_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$NITROGEN" | grep -q "$NITROGEN" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_NITROGEN_CHECK="TRUE"/STUDIO_NITROGEN_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_NITROGEN_CHECK="FALSE"/STUDIO_NITROGEN_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$TINT2_PANEL" | grep -q "$TINT2_PANEL" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_TINT2_PANEL_CHECK="TRUE"/STUDIO_TINT2_PANEL_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_TINT2_PANEL_CHECK="FALSE"/STUDIO_TINT2_PANEL_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$POLYBAR_PANEL" | grep -q "$POLYBAR_PANEL" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_POLYBAR_PANEL_CHECK="TRUE"/STUDIO_POLYBAR_PANEL_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_POLYBAR_PANEL_CHECK="FALSE"/STUDIO_POLYBAR_PANEL_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$CONKY" | grep -q "$CONKY" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_CONKY_CHECK="TRUE"/STUDIO_CONKY_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_CONKY_CHECK="FALSE"/STUDIO_CONKY_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$VOLUMEICON" | grep -q "$VOLUMEICON" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_VOLUMEICON_CHECK="TRUE"/STUDIO_VOLUMEICON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_VOLUMEICON_CHECK="FALSE"/STUDIO_VOLUMEICON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$SKIPPY_DAEMON" | grep -q "$SKIPPY_DAEMON" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_SKIPPY_DAEMON_CHECK="TRUE"/STUDIO_SKIPPY_DAEMON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_SKIPPY_DAEMON_CHECK="FALSE"/STUDIO_SKIPPY_DAEMON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$SUPERKEY" | grep -q "$SUPERKEY" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_SUPERKEY_CHECK="TRUE"/STUDIO_SUPERKEY_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_SUPERKEY_CHECK="FALSE"/STUDIO_SUPERKEY_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$POLICYKIT" | grep -q "$POLICYKIT" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_POLICYKIT_CHECK="TRUE"/STUDIO_POLICYKIT_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_POLICYKIT_CHECK="FALSE"/STUDIO_POLICYKIT_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$SIDE_SHORTCUTS" | grep -q "$SIDE_SHORTCUTS" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_SIDE_SHORTCUTS_CHECK="TRUE"/STUDIO_SIDE_SHORTCUTS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_SIDE_SHORTCUTS_CHECK="FALSE"/STUDIO_SIDE_SHORTCUTS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$NUMLOCK" | grep -q "$NUMLOCK" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_NUMLOCK_CHECK="TRUE"/STUDIO_NUMLOCK_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_NUMLOCK_CHECK="FALSE"/STUDIO_NUMLOCK_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$OBANIMATIONS" | grep -q "$OBANIMATIONS" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_OBANIMATIONS_CHECK="TRUE"/STUDIO_OBANIMATIONS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_OBANIMATIONS_CHECK="FALSE"/STUDIO_OBANIMATIONS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$COMPOSITOR_SHADOW" | grep -q "$COMPOSITOR_SHADOW" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_COMPOSITOR_SHADOW_CHECK="TRUE"/STUDIO_COMPOSITOR_SHADOW_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_COMPOSITOR_SHADOW_CHECK="FALSE"/STUDIO_COMPOSITOR_SHADOW_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$ALSA_CONFIGS" | grep -q "$ALSA_CONFIGS" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_ALSA_CONFIGS_CHECK="TRUE"/STUDIO_ALSA_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_ALSA_CONFIGS_CHECK="FALSE"/STUDIO_ALSA_CONFIGS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$CPUPOWER_CONFIGS" | grep -q "$CPUPOWER_CONFIGS" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_CPUPOWER_CONFIGS_CHECK="TRUE"/STUDIO_CPUPOWER_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_CPUPOWER_CONFIGS_CHECK="FALSE"/STUDIO_CPUPOWER_CONFIGS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$TLP_CONFIGS" | grep -q "$TLP_CONFIGS" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_TLP_CONFIGS_CHECK="TRUE"/STUDIO_TLP_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_TLP_CONFIGS_CHECK="FALSE"/STUDIO_TLP_CONFIGS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$CPU_SERVICE" | grep -q "$CPU_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_CPU_SERVICE_CHECK="TRUE"/STUDIO_CPU_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_CPU_SERVICE_CHECK="FALSE"/STUDIO_CPU_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$RTC_SERVICE" | grep -q "$RTC_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_RTC_SERVICE_CHECK="TRUE"/STUDIO_RTC_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_RTC_SERVICE_CHECK="FALSE"/STUDIO_RTC_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$HPET_SERVICE" | grep -q "$HPET_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_HPET_SERVICE_CHECK="TRUE"/STUDIO_HPET_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_HPET_SERVICE_CHECK="FALSE"/STUDIO_HPET_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$UFW_SERVICE" | grep -q "$UFW_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_UFW_SERVICE_CHECK="TRUE"/STUDIO_UFW_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_UFW_SERVICE_CHECK="FALSE"/STUDIO_UFW_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$FIREWALLD_SERVICE" | grep -q "$FIREWALLD_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_FIREWALLD_SERVICE_CHECK="TRUE"/STUDIO_FIREWALLD_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_FIREWALLD_SERVICE_CHECK="FALSE"/STUDIO_FIREWALLD_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$GRUB_CONFIGS" | grep -q "$GRUB_CONFIGS" /tmp/workflow-settings/.cache; then

								sed -i 's/STUDIO_GRUB_CONFIGS_CHECK="TRUE"/STUDIO_GRUB_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/STUDIO_GRUB_CONFIGS_CHECK="FALSE"/STUDIO_GRUB_CONFIGS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							exec rb-workflow-settings.sh

						;;

						1 )

							if [ "$BOX" = $EXTRA_BUTTON_BACK ]; then

								exec rb-workflow-settings.sh

							elif [ "$BOX" = $STUDIO_EXTRA_BUTTON_DEFAULT ]; then

								sed -i 's/STUDIO_JACK_CHECK="TRUE"/STUDIO_JACK_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_CADENCE_CHECK="TRUE"/STUDIO_CADENCE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_ARANDR_CHECK="FALSE"/STUDIO_ARANDR_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_XFSETTINGSD_CHECK="TRUE"/STUDIO_XFSETTINGSD_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_COMPOSITOR_CHECK="TRUE"/STUDIO_COMPOSITOR_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_THUNAR_DAEMON_CHECK="TRUE"/STUDIO_THUNAR_DAEMON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_PCMANFM_CHECK="TRUE"/STUDIO_PCMANFM_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_NITROGEN_CHECK="FALSE"/STUDIO_NITROGEN_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_TINT2_PANEL_CHECK="FALSE"/STUDIO_TINT2_PANEL_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_POLYBAR_PANEL_CHECK="TRUE"/STUDIO_POLYBAR_PANEL_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_CONKY_CHECK="TRUE"/STUDIO_CONKY_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_VOLUMEICON_CHECK="TRUE"/STUDIO_VOLUMEICON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_SKIPPY_DAEMON_CHECK="TRUE"/STUDIO_SKIPPY_DAEMON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_SUPERKEY_CHECK="TRUE"/STUDIO_SUPERKEY_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_POLICYKIT_CHECK="TRUE"/STUDIO_POLICYKIT_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_SIDE_SHORTCUTS_CHECK="FALSE"/STUDIO_SIDE_SHORTCUTS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_NUMLOCK_CHECK="TRUE"/STUDIO_NUMLOCK_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_OBANIMATIONS_CHECK="TRUE"/STUDIO_OBANIMATIONS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_COMPOSITOR_SHADOW_CHECK="TRUE"/STUDIO_COMPOSITOR_SHADOW_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_ALSA_CONFIGS_CHECK="TRUE"/STUDIO_ALSA_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_CPUPOWER_CONFIGS_CHECK="TRUE"/STUDIO_CPUPOWER_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_TLP_CONFIGS_CHECK="TRUE"/STUDIO_TLP_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_CPU_SERVICE_CHECK="TRUE"/STUDIO_CPU_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_RTC_SERVICE_CHECK="TRUE"/STUDIO_RTC_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_HPET_SERVICE_CHECK="TRUE"/STUDIO_HPET_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_UFW_SERVICE_CHECK="TRUE"/STUDIO_UFW_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_FIREWALLD_SERVICE_CHECK="TRUE"/STUDIO_FIREWALLD_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/STUDIO_GRUB_CONFIGS_CHECK="FALSE"/STUDIO_GRUB_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sleep 0.5; zenity --info --text="Studio session restored to default." --width="260"; exec rb-workflow-settings.sh

							fi

						;;

						* )

					esac

			elif [ "$BOX" = "Alternative Mode" ]; then

				BOX=$($ZEN_SETTINGS_MENU --title="$ALTERNATIVE_WINDOW_TITLE" --text="$ALTERNATIVE_WINDOW_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_BACK" --extra-button="$ALTERNATIVE_EXTRA_BUTTON_DEFAULT" \
					--column="$PICK_COLUMN_TITLE" --column="$OPTIONS_COLUMN_TITLE" --column="$DESCRIPTION_COLUMN_TITLE" --separator=" " \
					"$ALTERNATIVE_JACK_CHECK" "$JACK_SESSION" "$JACK_SESSION_INFO" \
					"$ALTERNATIVE_CADENCE_CHECK" "$CADENCE_SESSION" "$CADENCE_SESSION_INFO" \
					"$ALTERNATIVE_ARANDR_CHECK" "$ARANDR" "$ARANDR_INFO" \
					"$ALTERNATIVE_XFSETTINGSD_CHECK" "$XFSETTINGSD" "$XFSETTINGSD_INFO" \
					"$ALTERNATIVE_COMPOSITOR_CHECK" "$COMPOSITOR" "$COMPOSITOR_INFO" \
					"$ALTERNATIVE_THUNAR_DAEMON_CHECK" "$THUNAR_DAEMON" "$THUNAR_DAEMON_INFO" \
					"$ALTERNATIVE_PCMANFM_CHECK" "$PCMANFM" "$PCMANFM_INFO" \
					"$ALTERNATIVE_NITROGEN_CHECK" "$NITROGEN" "$NITROGEN_INFO" \
					"$ALTERNATIVE_TINT2_PANEL_CHECK" "$TINT2_PANEL" "$TINT2_PANEL_INFO" \
					"$ALTERNATIVE_POLYBAR_PANEL_CHECK" "$POLYBAR_PANEL" "$POLYBAR_PANEL_INFO" \
					"$ALTERNATIVE_CONKY_CHECK" "$CONKY" "$CONKY_INFO" \
					"$ALTERNATIVE_VOLUMEICON_CHECK" "$VOLUMEICON" "$VOLUMEICON_INFO" \
					"$ALTERNATIVE_SKIPPY_DAEMON_CHECK" "$SKIPPY_DAEMON" "$SKIPPY_DAEMON_INFO" \
					"$ALTERNATIVE_SUPERKEY_CHECK" "$SUPERKEY" "$SUPERKEY_INFO" \
					"$ALTERNATIVE_POLICYKIT_CHECK" "$POLICYKIT" "$POLICYKIT_INFO" \
					"$ALTERNATIVE_SIDE_SHORTCUTS_CHECK" "$SIDE_SHORTCUTS" "$SIDE_SHORTCUTS_INFO" \
					"$ALTERNATIVE_NUMLOCK_CHECK" "$NUMLOCK" "$NUMLOCK_INFO" \
					"$ALTERNATIVE_OBANIMATIONS_CHECK" "$OBANIMATIONS" "$OBANIMATIONS_INFO" \
					"$ALTERNATIVE_COMPOSITOR_SHADOW_CHECK" "$COMPOSITOR_SHADOW" "$COMPOSITOR_SHADOW_INFO" \
					"$ALTERNATIVE_ALSA_CONFIGS_CHECK" "$ALSA_CONFIGS" "$ALSA_CONFIGS_INFO" \
					"$ALTERNATIVE_CPUPOWER_CONFIGS_CHECK" "$CPUPOWER_CONFIGS" "$CPUPOWER_CONFIGS_INFO" \
					"$ALTERNATIVE_TLP_CONFIGS_CHECK" "$TLP_CONFIGS" "$TLP_CONFIGS_INFO" \
					"$ALTERNATIVE_CPU_SERVICE_CHECK" "$CPU_SERVICE" "$CPU_SERVICE_INFO" \
					"$ALTERNATIVE_RTC_SERVICE_CHECK" "$RTC_SERVICE" "$RTC_SERVICE_INFO" \
					"$ALTERNATIVE_HPET_SERVICE_CHECK" "$HPET_SERVICE" "$HPET_SERVICE_INFO" \
					"$ALTERNATIVE_UFW_SERVICE_CHECK" "$UFW_SERVICE" "$UFW_SERVICE_INFO" \
					"$ALTERNATIVE_FIREWALLD_SERVICE_CHECK" "$FIREWALLD_SERVICE" "$FIREWALLD_SERVICE_INFO" \
					"$ALTERNATIVE_GRUB_CONFIGS_CHECK" "$GRUB_CONFIGS" "$GRUB_CONFIGS_INFO" \
					--multiple 2>/dev/null)

					case $? in

						0 )

							mkdir -p /tmp/workflow-settings; echo $BOX > /tmp/workflow-settings/.cache							

							if ! echo "$JACK_SESSION" | grep -q "$JACK_SESSION" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_JACK_CHECK="TRUE"/ALTERNATIVE_JACK_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_JACK_CHECK="FALSE"/ALTERNATIVE_JACK_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$CADENCE_SESSION" | grep -q "$CADENCE_SESSION" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_CADENCE_CHECK="TRUE"/ALTERNATIVE_CADENCE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_CADENCE_CHECK="FALSE"/ALTERNATIVE_CADENCE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$ARANDR" | grep -q "$ARANDR" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_ARANDR_CHECK="TRUE"/ALTERNATIVE_ARANDR_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_ARANDR_CHECK="FALSE"/ALTERNATIVE_ARANDR_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$XFSETTINGSD" | grep -q "$XFSETTINGSD" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_XFSETTINGSD_CHECK="TRUE"/ALTERNATIVE_XFSETTINGSD_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_XFSETTINGSD_CHECK="FALSE"/ALTERNATIVE_XFSETTINGSD_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$COMPOSITOR" | grep -q "$COMPOSITOR" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_COMPOSITOR_CHECK="TRUE"/ALTERNATIVE_COMPOSITOR_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_COMPOSITOR_CHECK="FALSE"/ALTERNATIVE_COMPOSITOR_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$THUNAR_DAEMON" | grep -q "$THUNAR_DAEMON" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_THUNAR_DAEMON_CHECK="TRUE"/ALTERNATIVE_THUNAR_DAEMON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_THUNAR_DAEMON_CHECK="FALSE"/ALTERNATIVE_THUNAR_DAEMON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$PCMANFM" | grep -q "$PCMANFM" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_PCMANFM_CHECK="TRUE"/ALTERNATIVE_PCMANFM_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_PCMANFM_CHECK="FALSE"/ALTERNATIVE_PCMANFM_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$NITROGEN" | grep -q "$NITROGEN" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_NITROGEN_CHECK="TRUE"/ALTERNATIVE_NITROGEN_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_NITROGEN_CHECK="FALSE"/ALTERNATIVE_NITROGEN_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$TINT2_PANEL" | grep -q "$TINT2_PANEL" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_TINT2_PANEL_CHECK="TRUE"/ALTERNATIVE_TINT2_PANEL_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_TINT2_PANEL_CHECK="FALSE"/ALTERNATIVE_TINT2_PANEL_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$POLYBAR_PANEL" | grep -q "$POLYBAR_PANEL" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_POLYBAR_PANEL_CHECK="TRUE"/ALTERNATIVE_POLYBAR_PANEL_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_POLYBAR_PANEL_CHECK="FALSE"/ALTERNATIVE_POLYBAR_PANEL_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$CONKY" | grep -q "$CONKY" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_CONKY_CHECK="TRUE"/ALTERNATIVE_CONKY_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_CONKY_CHECK="FALSE"/ALTERNATIVE_CONKY_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$VOLUMEICON" | grep -q "$VOLUMEICON" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_VOLUMEICON_CHECK="TRUE"/ALTERNATIVE_VOLUMEICON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_VOLUMEICON_CHECK="FALSE"/ALTERNATIVE_VOLUMEICON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$SKIPPY_DAEMON" | grep -q "$SKIPPY_DAEMON" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_SKIPPY_DAEMON_CHECK="TRUE"/ALTERNATIVE_SKIPPY_DAEMON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_SKIPPY_DAEMON_CHECK="FALSE"/ALTERNATIVE_SKIPPY_DAEMON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$SUPERKEY" | grep -q "$SUPERKEY" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_SUPERKEY_CHECK="TRUE"/ALTERNATIVE_SUPERKEY_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_SUPERKEY_CHECK="FALSE"/ALTERNATIVE_SUPERKEY_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$POLICYKIT" | grep -q "$POLICYKIT" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_POLICYKIT_CHECK="TRUE"/ALTERNATIVE_POLICYKIT_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_POLICYKIT_CHECK="FALSE"/ALTERNATIVE_POLICYKIT_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$SIDE_SHORTCUTS" | grep -q "$SIDE_SHORTCUTS" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_SIDE_SHORTCUTS_CHECK="TRUE"/ALTERNATIVE_SIDE_SHORTCUTS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_SIDE_SHORTCUTS_CHECK="FALSE"/ALTERNATIVE_SIDE_SHORTCUTS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$NUMLOCK" | grep -q "$NUMLOCK" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_NUMLOCK_CHECK="TRUE"/ALTERNATIVE_NUMLOCK_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_NUMLOCK_CHECK="FALSE"/ALTERNATIVE_NUMLOCK_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$OBANIMATIONS" | grep -q "$OBANIMATIONS" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_OBANIMATIONS_CHECK="TRUE"/ALTERNATIVE_OBANIMATIONS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_OBANIMATIONS_CHECK="FALSE"/ALTERNATIVE_OBANIMATIONS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$COMPOSITOR_SHADOW" | grep -q "$COMPOSITOR_SHADOW" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_COMPOSITOR_SHADOW_CHECK="TRUE"/ALTERNATIVE_COMPOSITOR_SHADOW_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_COMPOSITOR_SHADOW_CHECK="FALSE"/ALTERNATIVE_COMPOSITOR_SHADOW_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$ALSA_CONFIGS" | grep -q "$ALSA_CONFIGS" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_ALSA_CONFIGS_CHECK="TRUE"/ALTERNATIVE_ALSA_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_ALSA_CONFIGS_CHECK="FALSE"/ALTERNATIVE_ALSA_CONFIGS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$CPUPOWER_CONFIGS" | grep -q "$CPUPOWER_CONFIGS" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_CPUPOWER_CONFIGS_CHECK="TRUE"/ALTERNATIVE_CPUPOWER_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_CPUPOWER_CONFIGS_CHECK="FALSE"/ALTERNATIVE_CPUPOWER_CONFIGS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$TLP_CONFIGS" | grep -q "$TLP_CONFIGS" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_TLP_CONFIGS_CHECK="TRUE"/ALTERNATIVE_TLP_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_TLP_CONFIGS_CHECK="FALSE"/ALTERNATIVE_TLP_CONFIGS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$CPU_SERVICE" | grep -q "$CPU_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_CPU_SERVICE_CHECK="TRUE"/ALTERNATIVE_CPU_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_CPU_SERVICE_CHECK="FALSE"/ALTERNATIVE_CPU_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$RTC_SERVICE" | grep -q "$RTC_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_RTC_SERVICE_CHECK="TRUE"/ALTERNATIVE_RTC_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_RTC_SERVICE_CHECK="FALSE"/ALTERNATIVE_RTC_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$HPET_SERVICE" | grep -q "$HPET_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_HPET_SERVICE_CHECK="TRUE"/ALTERNATIVE_HPET_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_HPET_SERVICE_CHECK="FALSE"/ALTERNATIVE_HPET_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$UFW_SERVICE" | grep -q "$UFW_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_UFW_SERVICE_CHECK="TRUE"/ALTERNATIVE_UFW_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_UFW_SERVICE_CHECK="FALSE"/ALTERNATIVE_UFW_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$FIREWALLD_SERVICE" | grep -q "$FIREWALLD_SERVICE" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_FIREWALLD_SERVICE_CHECK="TRUE"/ALTERNATIVE_FIREWALLD_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_FIREWALLD_SERVICE_CHECK="FALSE"/ALTERNATIVE_FIREWALLD_SERVICE_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							if ! echo "$GRUB_CONFIGS" | grep -q "$GRUB_CONFIGS" /tmp/workflow-settings/.cache; then

								sed -i 's/ALTERNATIVE_GRUB_CONFIGS_CHECK="TRUE"/ALTERNATIVE_GRUB_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config

							else

								sed -i 's/ALTERNATIVE_GRUB_CONFIGS_CHECK="FALSE"/ALTERNATIVE_GRUB_CONFIGS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config

							fi

							exec rb-workflow-settings.sh

						;;

						1 )

							if [ "$BOX" = $EXTRA_BUTTON_BACK ]; then

								exec rb-workflow-settings.sh

							elif [ "$BOX" = $ALTERNATIVE_EXTRA_BUTTON_DEFAULT ]; then

								sed -i 's/ALTERNATIVE_JACK_CHECK="TRUE"/ALTERNATIVE_JACK_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_CADENCE_CHECK="TRUE"/ALTERNATIVE_CADENCE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_ARANDR_CHECK="TRUE"/ALTERNATIVE_ARANDR_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_XFSETTINGSD_CHECK="FALSE"/ALTERNATIVE_XFSETTINGSD_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_COMPOSITOR_CHECK="FALSE"/ALTERNATIVE_COMPOSITOR_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_THUNAR_DAEMON_CHECK="FALSE"/ALTERNATIVE_THUNAR_DAEMON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_PCMANFM_CHECK="TRUE"/ALTERNATIVE_PCMANFM_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_NITROGEN_CHECK="FALSE"/ALTERNATIVE_NITROGEN_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_TINT2_PANEL_CHECK="FALSE"/ALTERNATIVE_TINT2_PANEL_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_POLYBAR_PANEL_CHECK="FALSE"/ALTERNATIVE_POLYBAR_PANEL_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_CONKY_CHECK="TRUE"/ALTERNATIVE_CONKY_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_VOLUMEICON_CHECK="TRUE"/ALTERNATIVE_VOLUMEICON_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_SKIPPY_DAEMON_CHECK="FALSE"/ALTERNATIVE_SKIPPY_DAEMON_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_SUPERKEY_CHECK="TRUE"/ALTERNATIVE_SUPERKEY_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_POLICYKIT_CHECK="FALSE"/ALTERNATIVE_POLICYKIT_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_SIDE_SHORTCUTS_CHECK="FALSE"/ALTERNATIVE_SIDE_SHORTCUTS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_NUMLOCK_CHECK="TRUE"/ALTERNATIVE_NUMLOCK_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_OBANIMATIONS_CHECK="FALSE"/ALTERNATIVE_OBANIMATIONS_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_COMPOSITOR_SHADOW_CHECK="FALSE"/ALTERNATIVE_COMPOSITOR_SHADOW_CHECK="TRUE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_ALSA_CONFIGS_CHECK="TRUE"/ALTERNATIVE_ALSA_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_CPUPOWER_CONFIGS_CHECK="TRUE"/ALTERNATIVE_CPUPOWER_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_TLP_CONFIGS_CHECK="TRUE"/ALTERNATIVE_TLP_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_CPU_SERVICE_CHECK="TRUE"/ALTERNATIVE_CPU_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_RTC_SERVICE_CHECK="TRUE"/ALTERNATIVE_RTC_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_HPET_SERVICE_CHECK="TRUE"/ALTERNATIVE_HPET_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_UFW_SERVICE_CHECK="TRUE"/ALTERNATIVE_UFW_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_FIREWALLD_SERVICE_CHECK="TRUE"/ALTERNATIVE_FIREWALLD_SERVICE_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sed -i 's/ALTERNATIVE_GRUB_CONFIGS_CHECK="FALSE"/ALTERNATIVE_GRUB_CONFIGS_CHECK="FALSE"/' $HOME/.config/workflow-settings/config
								sleep 0.5; zenity --info --text="Alternative session restored to default." --width="260"; exec rb-workflow-settings.sh

							fi

						;;

						* )

					esac

			fi

		;;

		* )

		esac
