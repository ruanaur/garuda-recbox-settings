#!/usr/bin/sh

TITLE="RecBox - Night Light"
TEXT="Which one you want to use for Night Light?"
OK_BUTTON="Apply"
CANCEL_BUTTON="Close"
EXTRA_BUTTON="Settings"
COLUMN_1="Pick"
COLUMN_2="Option"
COLUMN_3="Description"
OPTION_1="Redshift"
OPTION_1_DESCRIPTION="Redshift is not required to Xrandr script work."
OPTION_2="Xrandr"
OPTION_2_DESCRIPTION="Uses script to manipulate Xrandr settings."
INFO_BOX="zenity --info --width=280 --height=80"
INFO_TITLE="RecBox - Night Light"
RED_INFO_TEXT="Redshift is set as default Night Light."
RED_INFO_TEXT_ALT="There is no redshift config file, add redshift.conf to ~/.config/ directory."
XR_INFO_TEXT="Xrandr script is set as default Night Light."
GAMMA_SCALE_TITLE_DAY="Gamma - Day"
GAMMA_SCALE_TITLE_NIGHT="Gamma - Night"
GAMMA_SCALE_TEXT="Adjust gamma value."
ENTR_TITLE="Xrandr Settings"
G_ENTR_TEXT="Xrandr sets display colors via RGB values.\n\n1:1:1 is default value for display colors\n1.1:0.8:0.6 is set as default Night Light value\n\nAdd values for Gamma here:"
B_SCALE_TEXT="Adjust Brightness value"
RED_TEST=$(grep "REDSHIFT=" $HOME/.config/night-light-settings/config | cut -d "'" -f 2)
XR_TEST=$(grep "XRANDR=" $HOME/.config/night-light-settings/config | cut -d "'" -f 2)
SETTINGS_TEST=$(grep "NIGHT_LIGHT=" $HOME/.config/night-light-settings/config | cut -d "'" -f 2)
DEF_USER_GAMMA=$(grep "XRANDR_USER_GAMMA=" $HOME/.config/night-light-settings/config | cut -d "'" -f 2)
DEF_USER_BRGHTNS=$(grep "XRANDR_USER_BRGHTNS=" $HOME/.config/night-light-settings/config | cut -d "'" -f 2)
DEF_USER_BRGHTNS_SCALE=$(grep "XRANDR_USER_BRGHTNS=" $HOME/.config/night-light-settings/config | cut -d "'" -f 2 | cut -d "." -f 2)
RED_GAMMA_DAY=$(grep "temp-day=" $HOME/.config/redshift.conf | cut -d '=' -f 2)
RED_GAMMA_NIGHT=$(grep "temp-night=" $HOME/.config/redshift.conf | cut -d '=' -f 2)
REDSHIFT_TEST=$(find $HOME/.config/ -maxdepth 1 -name "redshift.conf" | cut -d '/' -f 5)

ZEN_MENU="zenity --list --radiolist --width=400 --height=80 --hide-header"
ENTR_BOX="zenity --entry --width=360"
SCALE_BOX="zenity --scale --min-value=0 --max-value=6500 --step=10"
XSCALE_BOX="zenity --scale --min-value=1 --max-value=10 --step=1"

	BOX=$($ZEN_MENU --title="$TITLE" --text="$TEXT" \
	--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON" \
	--column="$COLUMN_1" --column "$COLUMN_2" --column="$COLUMN_3" "$RED_TEST" "$OPTION_1" "$OPTION_1_DESCRIPTION" "$XR_TEST" "$OPTION_2" "$OPTION_2_DESCRIPTION")

		case $? in

			0 )

				if [ "$BOX" = "$OPTION_1" ]; then

					if [ $REDSHIFT_TEST = "redshift.conf" ]; then

						sed -i '4s/Xrandr/Redshift/' $HOME/.config/night-light-settings/config
						sed -i "s/REDSHIFT='FALSE'/REDSHIFT='TRUE'/" $HOME/.config/night-light-settings/config
						sed -i "s/XRANDR='TRUE'/XRANDR='FALSE'/" $HOME/.config/night-light-settings/config
						RBOX=$($INFO_BOX --title="$INFO_TITLE" --text="$RED_INFO_TEXT"); exec rb-night-light-settings.sh

					else

						sed -i '4s/Redshift/Xrandr/' $HOME/.config/night-light-settings/config
						sed -i "s/REDSHIFT='TRUE'/REDSHIFT='FALSE'/" $HOME/.config/night-light-settings/config
						sed -i "s/XRANDR='FALSE'/XRANDR='TRUE'/" $HOME/.config/night-light-settings/config
						RBOX=$($INFO_BOX --title="$INFO_TITLE" --text="$RED_INFO_TEXT_ALT") & exec rb-night-light-settings.sh

					fi


				elif [ "$BOX" = "$OPTION_2" ]; then

					sed -i '4s/Redshift/Xrandr/' $HOME/.config/night-light-settings/config
					sed -i "s/REDSHIFT='TRUE'/REDSHIFT='FALSE'/" $HOME/.config/night-light-settings/config
					sed -i "s/XRANDR='FALSE'/XRANDR='TRUE'/" $HOME/.config/night-light-settings/config
					XRBOX=$($INFO_BOX --title="$INFO_TITLE" --text="$XR_INFO_TEXT"); exec rb-night-light-settings.sh

				fi

			;;

			1 )

				if [ "$BOX" = $EXTRA_BUTTON ]; then

					if [ $SETTINGS_TEST = "Redshift" ]; then

						if [ $REDSHIFT_TEST = "redshift.conf" ]; then

							GSCALE_BOX_DAY=$($SCALE_BOX --title="$GAMMA_SCALE_TITLE_DAY" --text="$GAMMA_SCALE_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --value="$RED_GAMMA_DAY")

								if [ -z $GSCALE_BOX_DAY ]; then

									echo "exit"

								elif [ $GSCALE_BOX_DAY = $GSCALE_BOX_DAY ]; then

									sed -i "s/$RED_GAMMA_DAY/$GSCALE_BOX_DAY/" $HOME/.config/redshift.conf

									GSCALE_BOX_NIGHT=$($SCALE_BOX --title="$GAMMA_SCALE_TITLE_NIGHT" --text="$GAMMA_SCALE_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --value="$RED_GAMMA_NIGHT")

										if [ -z $GSCALE_BOX_NIGHT ]; then

											echo "exit"

										elif [ $GSCALE_BOX_NIGHT = $GSCALE_BOX_NIGHT ]; then

											sed -i "s/$RED_GAMMA_NIGHT/$GSCALE_BOX_NIGHT/" $HOME/.config/redshift.conf

										fi

								fi

					else

						sed -i '4s/Redshift/Xrandr/' $HOME/.config/night-light-settings/config
						sed -i "s/REDSHIFT='TRUE'/REDSHIFT='FALSE'/" $HOME/.config/night-light-settings/config
						sed -i "s/XRANDR='FALSE'/XRANDR='TRUE'/" $HOME/.config/night-light-settings/config
						RBOX=$($INFO_BOX --title="$INFO_TITLE" --text="$RED_INFO_TEXT_ALT"); exec rb-night-light-settings.sh
					fi

					elif [ $SETTINGS_TEST = "Xrandr" ]; then

						EG_BOX=$($ENTR_BOX --title="$ENTR_TITLE" --text="$G_ENTR_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --entry-text="$DEF_USER_GAMMA")

							if [ -z $EG_BOX ]; then

								echo "exit"

							elif [ $EG_BOX = $EG_BOX ]; then

								sed -i "s/$DEF_USER_GAMMA/$EG_BOX/" $HOME/.config/night-light-settings/config

								if [ $DEF_USER_BRGHTNS_SCALE -eq 0 ]; then

									DEF_USER_BRGHTNS_SCALE="10"

								fi

								EB_BOX=$($XSCALE_BOX --title="$ENTR_TITLE" --text="$B_SCALE_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --value="$DEF_USER_BRGHTNS_SCALE")

									if [ -z $EB_BOX ]; then

										echo "exit"

									elif [ $EB_BOX = $EB_BOX ]; then

										sed -i "s/$DEF_USER_GAMMA/$EG_BOX/" $HOME/.config/night-light-settings/config

										if [ "$EB_BOX" -eq 1 ]; then
								
											sed -i "8s/$DEF_USER_BRGHTNS/0.$EB_BOX/" $HOME/.config/night-light-settings/config; exec rb-night-light-settings.sh
								
										elif [ "$EB_BOX" -eq 2 ]; then
								
											sed -i "8s/$DEF_USER_BRGHTNS/0.$EB_BOX/" $HOME/.config/night-light-settings/config; exec rb-night-light-settings.sh
								
										elif [ "$EB_BOX" -eq 3 ]; then
								
											sed -i "8s/$DEF_USER_BRGHTNS/0.$EB_BOX/" $HOME/.config/night-light-settings/config; exec rb-night-light-settings.sh
								
										elif [ "$EB_BOX" -eq 4 ]; then
								
											sed -i "8s/$DEF_USER_BRGHTNS/0.$EB_BOX/" $HOME/.config/night-light-settings/config; exec rb-night-light-settings.sh
								
										elif [ "$EB_BOX" -eq 5 ]; then
								
											sed -i "8s/$DEF_USER_BRGHTNS/0.$EB_BOX/" $HOME/.config/night-light-settings/config; exec rb-night-light-settings.sh
								
										elif [ "$EB_BOX" -eq 6 ]; then
								
											sed -i "8s/$DEF_USER_BRGHTNS/0.$EB_BOX/" $HOME/.config/night-light-settings/config; exec rb-night-light-settings.sh
								
										elif [ "$EB_BOX" -eq 7 ]; then
								
											sed -i "8s/$DEF_USER_BRGHTNS/0.$EB_BOX/" $HOME/.config/night-light-settings/config; exec rb-night-light-settings.sh
								
										elif [ "$EB_BOX" -eq 8 ]; then
								
											sed -i "8s/$DEF_USER_BRGHTNS/0.$EB_BOX/" $HOME/.config/night-light-settings/config; exec rb-night-light-settings.sh
								
										elif [ "$EB_BOX" -eq 9 ]; then
								
											sed -i "8s/$DEF_USER_BRGHTNS/0.$EB_BOX/" $HOME/.config/night-light-settings/config; exec rb-night-light-settings.sh
								
										elif [ "$EB_BOX" -eq 10 ]; then
								
											if [ $EB_BOX -eq 10 ]; then

												EB_BOX="1.0"

											fi

											sed -i "8s/$DEF_USER_BRGHTNS/$EB_BOX/" $HOME/.config/night-light-settings/config; exec rb-night-light-settings.sh

										fi

									fi

						fi

					fi

				fi

			;;

			* )

		esac
