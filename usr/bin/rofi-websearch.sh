#!/usr/bin/sh

MENU=$(echo "DuckDuckGo|Swisscows|Ardour Forum|Ardour Manual|LBRY|You Tube|Garuda Forum|Garuda Wiki|Arch Wiki|Wikipedia" | \
rofi -sep "|" -dmenu -i -p ' ' "")

	case $MENU in

		"DuckDuckGo" )

			DDG=$(echo "Type here..." | rofi -sep "|" -dmenu -i -p ' ' "")

			if [ "$DDG" = "Type here..." ]; then

				break

			elif [ -z $DDG ]; then

				break

			else

				exo-open https://duckduckgo.com/?q="$DDG"

			fi
			
		;;

		"Swisscows" )

			SC=$(echo "Type here..." | rofi -sep "|" -dmenu -i -p ' ' "")

			if [ "$SC" = "Type here..." ]; then

				break

			elif [ -z $SC ]; then

				break

			else

				exo-open https://swisscows.com/web?query="$SC"

			fi

		;;

		"Ardour Forum" )

			AF=$(echo "Type here..." | rofi -sep "|" -dmenu -i -p ' ' "")

			if [ "$AF" = "Type here..." ]; then

				break

			elif [ -z $AF ]; then

				break

			else

				exo-open https://discourse.ardour.org/search?q="$AF"

			fi

		;;

		"Ardour Manual" )

			ARDOUR_MENU=$(echo "Table of Contents|Introduction to Ardour|Ardour Configuration|Ardour's Interface|Sessions & Tracks|Playback & Recording|Importing and Exporting|Editing|MIDI|Arranging|Mixing|Video|Control Surfaces|Scripting|Appendix" | \
			rofi -sep "|" -dmenu -i -p ' ' "")

				case $ARDOUR_MENU in

					"Table of Contents" )

						exo-open https://manual.ardour.org/toc/

					;;

					"Introduction to Ardour" )

						exo-open https://manual.ardour.org/introduction-to-ardour/

					;;

					"Ardour Configuration" )

						exo-open https://manual.ardour.org/ardour-configuration/

					;;

					"Ardour's Interface" )

						exo-open https://manual.ardour.org/ardours-interface/

					;;

					"Sessions & Tracks" )

						exo-open https://manual.ardour.org/sessions-tracks/

					;;

					"Playback & Recording" )

						exo-open https://manual.ardour.org/recording/

					;;

					"Importing and Exporting" )

						exo-open https://manual.ardour.org/importing-and-exporting/

					;;

					"Editing" )

						exo-open https://manual.ardour.org/editing-and-arranging/

					;;

					"MIDI" )

						exo-open https://manual.ardour.org/midi/

					;;

					"Arranging" )

						exo-open https://manual.ardour.org/arranging/

					;;

					"Mixing" )

						exo-open https://manual.ardour.org/mixing/

					;;

					"Video" )

						exo-open https://manual.ardour.org/video/

					;;

					"Control Surfaces" )

						exo-open https://manual.ardour.org/using-control-surfaces/

					;;

					"Scripting" )

						exo-open https://manual.ardour.org/scripting/

					;;

					"Appendix" )

						exo-open https://manual.ardour.org/appendix/

					;;

					* )

				esac

		;;

		"LBRY" )

			LBRY=$(echo "Type here..." | rofi -sep "|" -dmenu -i -p ' ' "")

			if [ "$LBRY" = "Type here..." ]; then

				break

			elif [ -z $LBRY ]; then

				break

			else

				exo-open https://lbry.tv/$/search?q="$LBRY"

			fi

		;;

		"You Tube" )

			YT=$(echo "Type here..." | rofi -sep "|" -dmenu -i -p ' ' "")

			if [ "$YT" = "Type here..." ]; then

				break

			elif [ -z $YT ]; then

				break

			else

				exo-open https://www.youtube.com/results?search_query="$YT"

			fi

		;;

		"Garuda Forum" )

			GF=$(echo "Type here..." | rofi -sep "|" -dmenu -i -p ' ' "")

			if [ "$GF" = "Type here..." ]; then

				break

			elif [ -z $GF ]; then

				break

			else

				exo-open https://forum.garuda.in/search?q="$GF"

			fi

		;;

		"Garuda Wiki" )

			GW=$(echo "Type here..." | rofi -sep "|" -dmenu -i -p ' ' "")

			if [ "$GW" == "Type here..." ]; then

				break

			elif [ -z $GW ]; then

				break

			else

				exo-open https://wiki.garuda.in/index.php?search="$GW"

			fi

			;;

		"Arch Wiki" )

			AW=$(echo "Type here..." | rofi -sep "|" -dmenu -i -p ' ' "")

			if [ "$AW" = "Type here..." ]; then

				break

			elif [ -z $AW ]; then

				break

			else

				exo-open https://wiki.archlinux.org/index.php?search="$AW"

			fi

			;;

		"Wikipedia" )

			WP=$(echo "Type here..." | rofi -sep "|" -dmenu -i -p ' ' "")

			if [ "$WP" = "Type here..." ]; then

				break

			elif [ -z $WP ]; then

				break

			else

				exo-open https://en.wikipedia.org/wiki/"$WP"

			fi

		;;

		*)

	esac
