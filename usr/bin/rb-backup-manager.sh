#!/usr/bin/sh

# VERSION=1.0

export RED='\033[0;31m'
export GREEN='\033[0;32m'
export YELLOW='\033[0;33m'
export WHITE='\033[0;37m'
export RESETCOLOR='\033[1;00m'

studio_session() {

DATE=$(date +%Y-%m-%d-%S_%H:%M:%S)

FILE_CHOICE_TITLE="Choose files to back up"
DIRECTORY_CHOICE_TITLE="Choose where to store backup files"
PROGRESS_BOX="zenity --progress --pulsate --no-cancel --width=360 --height=120"

SESSION_START_DIRECTORY=$(grep 'STUDIO_SESSION_DIRECTORY' $HOME/.config/backup-settings/config | cut -d '"' -f 2)

	case $? in

		0 )
			cd $SESSION_START_DIRECTORY

			FILE_CHOICE_ENTRY=$(zenity --title="$FILE_CHOICE_TITLE" --file-selection --directory --separator=" ")
	
				if [[ $? == 1 ]]; then

					exit 0

				else

					FILE_PATH=$(echo "$FILE_CHOICE_ENTRY" | rev | cut -d "/" -f1 | rev)
					cd $HOME/

					DIRECTORY_CHOICE_ENTRY=$(zenity --title="$DIRECTORY_CHOICE_TITLE" --file-selection --directory --separator=" ")

						if [[ $? == 1 ]]; then
							exit 0

						else

							mkdir "$DIRECTORY_CHOICE_ENTRY"/Session_"$DATE"
							cp -r "$FILE_CHOICE_ENTRY" "$DIRECTORY_CHOICE_ENTRY"/Session_"$DATE"/"$FILE_PATH" | $($PROGRESS_BOX --title="Work in progress..." --text="\n Copy $(echo $FILE_CHOICE_ENTRY)/...")
						fi

				fi
		;;
		* )

	esac

}

daw_config() {

DATE=$(date +%Y-%m-%d-%S_%H:%M:%S)

FILE_CHOICE_TITLE="Choose files to back up"
DIRECTORY_CHOICE_TITLE="Choose where to store DAW config files"
PROGRESS_BOX="zenity --progress --pulsate --no-cancel --width=360 --height=120"

SESSION_START_DIRECTORY=$(grep 'DAW_DIRECTORY' $HOME/.config/backup-settings/config | cut -d '"' -f 2)

	case $? in

		0 )
			cd $SESSION_START_DIRECTORY

			FILE_CHOICE_ENTRY=$(zenity --title="$FILE_CHOICE_TITLE" --file-selection --directory --separator=" ")
	
				if [[ $? == 1 ]]; then

					exit 0

				else

					FILE_PATH=$(echo "$FILE_CHOICE_ENTRY" | rev | cut -d "/" -f1 | rev)
					cd $HOME/

					DIRECTORY_CHOICE_ENTRY=$(zenity --title="$DIRECTORY_CHOICE_TITLE" --file-selection --directory --separator=" ")

						if [[ $? == 1 ]]; then
							exit 0
						else

							mkdir "$DIRECTORY_CHOICE_ENTRY"/DAW_config_"$DATE"
							cp -r "$FILE_CHOICE_ENTRY" "$DIRECTORY_CHOICE_ENTRY"/DAW_config_"$DATE"/"$FILE_PATH" | $($PROGRESS_BOX --title="Work in progress..." --text="\n Copy $(echo $FILE_CHOICE_ENTRY)/...")

						fi

				fi
		;;
		* )

	esac

}

set_studio() {

START_DIRECTORY="Choose start directory for session backup"

	case $? in

		0 )
			cd $HOME/

			START_DIRECTORY_ENTRY=$(zenity --title="$START_DIRECTORY" --file-selection --directory --separator=" ")
	

				if [[ $? == 1 ]]; then

					exit 0

				else

					sed -i '3d' $HOME/.config/backup-settings/config
					sed -i "2a "$START_DIRECTORY_ENTRY"" $HOME/.config/backup-settings/config
					sed -i '3s/^/STUDIO_SESSION_DIRECTORY="/' $HOME/.config/backup-settings/config
					sed -i '3s/$/"/' $HOME/.config/backup-settings/config

				fi

		;;
		* )

	esac

}

set_daw() {

START_DIRECTORY="Choose start directory for DAW config backup"

	case $? in

		0 )
			cd $HOME/

			START_DIRECTORY_ENTRY=$(zenity --title="$START_DIRECTORY" --file-selection --directory --separator=" ")
	

				if [[ $? == 1 ]]; then

					exit 0

				else

					sed -i '4d' $HOME/.config/backup-settings/config
					sed -i "3a "$START_DIRECTORY_ENTRY"" $HOME/.config/backup-settings/config
					sed -i '4s/^/DAW_DIRECTORY="/' $HOME/.config/backup-settings/config
					sed -i '4s/$/"/' $HOME/.config/backup-settings/config

				fi

		;;
		* )

	esac
	
}

ver() {
	echo -e "\n    ${WHITE}Version 1.0${RESETCOLOR}"
}

version() {
	echo -e "\n    ${WHITE}Version 1.0${RESETCOLOR}"
}

case "$1" in

	studio_session) studio_session;;
	daw_config) daw_config;;
	set_studio) set_studio;;
	set_daw) set_daw;;
	*)

	echo -e "
${GREEN}\n	--------------------------------------------------------------------------
								RecBox Backup Manager
	--------------------------------------------------------------------------\n
			Made by Projekt:Root projekt.root@tuta.io
			for RecBox and Garuda OpenBox.
			If you want to use script on other setup 
			probably you'll need to modify it.\n
	--------------------------------------------------------------------------${RESETCOLOR}

	Usage:\n
	rb-backup-manager.sh ${YELLOW}[$RED OPTION ${YELLOW}]

${GREEN}	Options${RESETCOLOR}:

${YELLOW}	>${RED} studio_session${YELLOW}                  -${RESETCOLOR} execute rofi logout menu
${YELLOW}	>${RED} daw_config${YELLOW}                 -${RESETCOLOR} execute rofi suspend menu
${YELLOW}	>${RED} set_studio${YELLOW}                -${RESETCOLOR} execute zenity backup menu
${YELLOW}	>${RED} set_daw${YELLOW}                -${RESETCOLOR} execute zenity backup menu

${YELLOW}	>${RED} ver, version${YELLOW}                 -${RESETCOLOR} show script version
" >&2
exit 1
;;
esac

echo -e $RESETCOLOR

exit 0