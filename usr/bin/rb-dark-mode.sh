#!/usr/bin/sh

# VERSION=2.2

export RED='\033[0;31m'
export GREEN='\033[0;32m'
export YELLOW='\033[0;33m'
export WHITE='\033[0;37m'
export RESETCOLOR='\033[1;00m'

GTK_LIGHT=$(grep 'GTK_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
GTK_DARK=$(grep 'GTK_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
QT_LIGHT=$(grep 'QT_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
QT_DARK=$(grep 'QT_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
ICONS_LIGHT=$(grep 'ICONS_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
ICONS_DARK=$(grep 'ICONS_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
ROFI_LIGHT=$(grep 'ROFI_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
ROFI_DARK=$(grep 'ROFI_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
OBOX_LIGHT=$(grep 'OBOX_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
OBOX_DARK=$(grep 'OBOX_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
SMENU_LIGHT=$(grep 'SMENU_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
SMENU_DARK=$(grep 'SMENU_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
RCMENU_LIGHT=$(grep 'RCMENU_LIGHT=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
RCMENU_DARK=$(grep 'RCMENU_DARK=' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
MODE_STATUS=$(grep DARK_MODE_STATUS $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)

daily_dark_mode_toggle() {

	if [ $MODE_STATUS = "OFF" ]; then

		xfconf-query -c xsettings -p /Net/ThemeName -s $GTK_DARK
		xfconf-query -c xsettings -p /Net/IconThemeName -s $ICONS_DARK
		cp ~/.config/openbox/jgmenu-theme-configs/dark-jgmenu ~/.config/jgmenu/jgmenurc
		cp ~/.config/openbox/jgmenu-theme-configs/dark-prepend.csv ~/.config/jgmenu/prepend.csv
		cp ~/.config/tint2/recbox-daily-dark.tint2rc ~/.config/tint2/tint2rc
		xdotool key super+u; garuda-tint2restart; sleep 1
#		Rofi theme configuration
		sed -i "s/$ROFI_LIGHT/$ROFI_DARK/" ~/.config/rofi/config
#		OpenBox theme configuration
		sed -i "s/$OBOX_LIGHT/$OBOX_DARK/" ~/.config/openbox/rc.xml
#		GTK2 theme configuration
		sed -i "s/$GTK_LIGHT/$GTK_DARK/" ~/.gtkrc-2.0
		sed -i "s/$ICONS_LIGHT/$ICONS_DARK/" ~/.gtkrc-2.0
#		GTK3 thme configuration
		sed -i "s/$GTK_LIGHT/$GTK_DARK/" ~/.config/gtk-3.0/settings.ini
		sed -i "s/$ICONS_LIGHT/$ICONS_DARK/" ~/.config/gtk-3.0/settings.ini
#		QT5
		sed -i "s/$QT_LIGHT/$QT_DARK/" ~/.config/Kvantum/kvantum.kvconfig
		sed -i "s/$ICONS_LIGHT/$ICONS_DARK/" ~/.config/qt5ct/qt5ct.conf
#		Side Menu theme configuration
		sed -i "22s/$SMENU_LIGHT/$SMENU_DARK/" $HOME/.config/darkmode-settings/config
#		Right Click Menu
		sed -i "26s/$RCMENU_LIGHT/$RCMENU_DARK/" $HOME/.config/darkmode-settings/config
#		Setting up Dark Mode status
		sed -i "30s/$MODE_STATUS/ON/" $HOME/.config/darkmode-settings/config
#		OpenBox Reconfiguration
		openbox --reconfigure; sleep 1;	xdotool key super+u & notify-send -t 2300 "Dark mode On"

	elif [ $MODE_STATUS = "ON" ]; then

		xfconf-query -c xsettings -p /Net/ThemeName -s $GTK_LIGHT
		xfconf-query -c xsettings -p /Net/IconThemeName -s $ICONS_LIGHT
		cp ~/.config/openbox/jgmenu-theme-configs/light-jgmenu ~/.config/jgmenu/jgmenurc
		cp ~/.config/openbox/jgmenu-theme-configs/light-prepend.csv ~/.config/jgmenu/prepend.csv
		cp ~/.config/tint2/recbox-daily-light.tint2rc ~/.config/tint2/tint2rc
		xdotool key super+u; garuda-tint2restart; sleep 1
#		Rofi theme configuration
		sed -i "s/$ROFI_DARK/$ROFI_LIGHT/" ~/.config/rofi/config
#		OpenBox theme configuration
		sed -i "s/$OBOX_DARK/$OBOX_LIGHT/" ~/.config/openbox/rc.xml
#		GTK2 theme configuration
		sed -i "s/$GTK_DARK/$GTK_LIGHT/" ~/.gtkrc-2.0
		sed -i "s/$ICONS_DARK/$ICONS_LIGHT/" ~/.gtkrc-2.0
#		GTK3 thme configuration
		sed -i "s/$GTK_DARK/$GTK_LIGHT/" ~/.config/gtk-3.0/settings.ini
		sed -i "s/$ICONS_DARK/$ICONS_LIGHT/" ~/.config/gtk-3.0/settings.ini
#		QT5
		sed -i "s/$QT_DARK/$QT_LIGHT/" ~/.config/Kvantum/kvantum.kvconfig
		sed -i "s/$ICONS_DARK/$ICONS_LIGHT/" ~/.config/qt5ct/qt5ct.conf
#		Side Menu theme configuration
		sed -i "22s/$SMENU_DARK/$SMENU_LIGHT/" $HOME/.config/darkmode-settings/config
#		Right Click Menu
		sed -i "26s/$RCMENU_DARK/$RCMENU_LIGHT/" $HOME/.config/darkmode-settings/config
#		Setting up Dark Mode status
		sed -i "30s/$MODE_STATUS/OFF/" $HOME/.config/darkmode-settings/config
#		OpenBox Reconfiguration
		openbox --reconfigure; sleep 1; xdotool key super+u & notify-send -t 2300 "Dark mode Off"

	fi  2>/dev/null

}

studio_dark_mode_toggle() {

	if [ $MODE_STATUS = "OFF" ]; then

		xfconf-query -c xsettings -p /Net/ThemeName -s $GTK_DARK
		xfconf-query -c xsettings -p /Net/IconThemeName -s $ICONS_DARK
		xfsettingsd; killall xfsettingsd
		cp ~/.config/openbox/jgmenu-theme-configs/dark-jgmenu ~/.config/jgmenu/jgmenurc
		cp ~/.config/openbox/jgmenu-theme-configs/dark-prepend.csv ~/.config/jgmenu/prepend.csv
		cp ~/.config/tint2/recbox-studio-dark.tint2rc ~/.config/tint2/tint2rc
		xdotool key super+u; garuda-tint2restart; sleep 1
#		Rofi theme configuration
		sed -i "s/$ROFI_LIGHT/$ROFI_DARK/" ~/.config/rofi/config
#		OpenBox theme configuration
		sed -i "s/$OBOX_LIGHT/$OBOX_DARK/" ~/.config/openbox/rc.xml
#		GTK2 theme configuration
		sed -i "s/$GTK_LIGHT/$GTK_DARK/" ~/.gtkrc-2.0
		sed -i "s/$ICONS_LIGHT/$ICONS_DARK/" ~/.gtkrc-2.0
#		GTK3 thme configuration
		sed -i "s/$GTK_LIGHT/$GTK_DARK/" ~/.config/gtk-3.0/settings.ini
		sed -i "s/$ICONS_LIGHT/$ICONS_DARK/" ~/.config/gtk-3.0/settings.ini
#		QT5
		sed -i "s/$QT_LIGHT/$QT_DARK/" ~/.config/Kvantum/kvantum.kvconfig
		sed -i "s/$ICONS_LIGHT/$ICONS_DARK/" ~/.config/qt5ct/qt5ct.conf
#		Side Menu theme configuration
		sed -i "22s/$SMENU_LIGHT/$SMENU_DARK/" $HOME/.config/darkmode-settings/config
#		Right Click Menu
		sed -i "26s/$RCMENU_LIGHT/$RCMENU_DARK/" $HOME/.config/darkmode-settings/config
#		Setting up Dark Mode status
		sed -i "30s/$MODE_STATUS/ON/" $HOME/.config/darkmode-settings/config
#		OpenBox Reconfiguration
		openbox --reconfigure; sleep 1; xdotool key super+u & notify-send -t 2300 "Dark mode On"

	elif [ $MODE_STATUS = "ON" ]; then

		xfconf-query -c xsettings -p /Net/ThemeName -s $GTK_LIGHT
		xfconf-query -c xsettings -p /Net/IconThemeName -s $ICONS_LIGHT
		xfsettingsd; killall xfsettingsd
		cp ~/.config/openbox/jgmenu-theme-configs/light-jgmenu ~/.config/jgmenu/jgmenurc
		cp ~/.config/openbox/jgmenu-theme-configs/light-prepend.csv ~/.config/jgmenu/prepend.csv
		cp ~/.config/tint2/recbox-studio-light.tint2rc ~/.config/tint2/tint2rc
		xdotool key super+u; garuda-tint2restart; sleep 1
#		Rofi theme configuration
		sed -i "s/$ROFI_DARK/$ROFI_LIGHT/" ~/.config/rofi/config
#		OpenBox theme configuration
		sed -i "s/$OBOX_DARK/$OBOX_LIGHT/" ~/.config/openbox/rc.xml
#		GTK2 theme configuration
		sed -i "s/$GTK_DARK/$GTK_LIGHT/" ~/.gtkrc-2.0
		sed -i "s/$ICONS_DARK/$ICONS_LIGHT/" ~/.gtkrc-2.0
#		GTK3 thme configuration
		sed -i "s/$GTK_DARK/$GTK_LIGHT/" ~/.config/gtk-3.0/settings.ini
		sed -i "s/$ICONS_DARK/$ICONS_LIGHT/" ~/.config/gtk-3.0/settings.ini
#		QT5
		sed -i "s/$QT_DARK/$QT_LIGHT/" ~/.config/Kvantum/kvantum.kvconfig
		sed -i "s/$ICONS_DARK/$ICONS_LIGHT/" ~/.config/qt5ct/qt5ct.conf
#		Side Menu theme configuration
		sed -i "22s/$SMENU_DARK/$SMENU_LIGHT/" $HOME/.config/darkmode-settings/config
#		Right Click Menu
		sed -i "26s/$RCMENU_DARK/$RCMENU_LIGHT/" $HOME/.config/darkmode-settings/config
#		Setting up Dark Mode status
		sed -i "30s/$MODE_STATUS/OFF/" $HOME/.config/darkmode-settings/config
#		OpenBox Reconfiguration
		openbox --reconfigure; sleep 1; xdotool key super+u & notify-send -t 2300 "Dark mode Off"

	fi  2>/dev/null

}

ver() {
	echo -e "\n    ${WHITE}Version 2.2${RESETCOLOR}"
}

version() {
	echo -e "\n    ${WHITE}Version 2.2${RESETCOLOR}"
}

case "$1" in

	daily_dark_mode_toggle ) daily_dark_mode_toggle ;;
	studio_dark_mode_toggle ) studio_dark_mode_toggle ;;
	ver ) ver ;;
	version ) version ;;

	* )

	echo -e "
${GREEN}\n	------------------------------------------------------------------
							RecBox Dark Mode
	------------------------------------------------------------------\n
				Made by Projekt:Root projekt.root@tuta.io
				for RecBox and Garuda OpenBox.
				If you want to use script on other setup 
				probably you'll need to modify it.\n
	------------------------------------------------------------------${RESETCOLOR}

	Usage:\n
	script-box.sh ${YELLOW}[$RED OPTION ${YELLOW}]

${GREEN}	Options${RESETCOLOR}:

${YELLOW}	>${RED} daily_dark_mode_toggle${YELLOW}    -${RESETCOLOR} toggle dark mode for Daily
${YELLOW}	>${RED} studio_dark_mode_toggle${YELLOW}   -${RESETCOLOR} toggle dark mode for Studio

${YELLOW}	>${RED} ver, version${YELLOW}              -${RESETCOLOR} show script version
" >&2
exit 1
;;
esac

echo -e $RESETCOLOR
exit 0
