#!/usr/bin/sh

# VERSION=3.9

my_preset() {

	echo ":: Cleaning...";echo ""; sleep 2; bleachbit -c --preset; echo ""; read -p ":: Press enter to exit"

}

disk_wiping() {

	echo -e ":: This process is slow and can make your system unresponsive,\n:: please be patient and don't interrupt the process.\n"
	echo -e ":: Do you want to proceed? [Y/n]"

	while :
	do

	read -r -p ":: Answer: " input

		case $input in

		    [yY] | [yY])
			bleachbit -c system.free_disk_space
			sleep 1
			echo ""
			read -p ":: Press enter to exit "
			exit 0
			;;
		    [nN] | [nN])
			exit 0
			;;
    		*)

			echo -e ":: Invalid input..."

		esac

	done

}

touchpad() {
# To see the name of the touchpad open terminal and type "xinput list"
# Example:
# ↳ SynPS/2 Synaptics TouchPad                    	id=15	[slave  pointer  (2)]
# https://elementaryos.stackexchange.com/questions/7160/how-can-i-disable-enable-my-laptops-touchpad-on-elementary-os-loki

TOUCHPAD=$(xinput --list --name-only | grep Synaptics)

		CMD=$(echo "Enable TouchPad|Disable TouchPad" | rofi -sep "|" -dmenu -i -p 'TouchPad ' "")

			case $CMD in

				*"Enable TouchPad") xinput set-prop "$TOUCHPAD" "Device Enabled" 1 ;;
				*"Disable TouchPad") xinput set-prop "$TOUCHPAD" "Device Enabled" 0 ;;
				*)

			esac

}

pdf_menu() {

cd /usr/share/doc/recbox/

TITLE="RecBox Documentation"
TEXT="<b>Choose document you want to read.</b>\n"
OK_BUTTON="Read"
CANCEL_BUTTON="Close"
COLUMN_TITLE="Pick"
ICON_PATH="/usr/share/icons/Flat-Remix-Green/apps/scalable/gnome-documents.svg"
ZEN_MENU="zenity --list --width=360 --height=265 --hide-header"

		CMD=$(ls | $ZEN_MENU --title="$TITLE" --text="$TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --column="$COLUMN_TITLE" --window-icon="$ICON_PATH")

			case $? in

				0) 

					[ "$CMD" ] && exo-open "$CMD" || notify-send -t 2600 "You've not chosen any document." & scripts-box.sh pdf_menu

				;;

				*)

			esac

}

zenity_calendar_ex() {

USER_WELCOME=$(whoami)
TITLE="Calendar"
TEXT="Welcome, <b>$USER_WELCOME</b>!"
ICON_PATH="/usr/share/icons/Flat-Remix-Green/apps/scalable/calendar.svg"
ZEN_MENU="zenity --calendar --width=720 --height=330"
NOTE_BOX="zenity --list --width=720 --height=330 --hide-header"
NOTE_LIST="zenity --list --checklist --width=720 --height=330  --hide-header"
LIST_PATH=$(ls $HOME/.zen-calendar -I ToDo | sed 's/^/FALSE /')
NOTE_MENU_TITLE="Calendar - Search"
NOTE_MENU_TEXT="Choose which note you pleased to edit."
NOTE_OK_BUTTON="Open"
MANAGE_OK_BUTTON=Apply
NOTE_CANCEL_BUTTON="Close"
NOTE_EXTRA_BUTTON="Back"
OK_BUTTON="Add"
CANCEL_BUTTON="Close"
EXTRA_BUTTON_OPEN_NOTES="Open"
EXTRA_BUTTON_MANAGE_NOTES="Manage"
EXTRA_BUTTON_TODO="ToDo"
FONT="Noto Sans Regular 9"
DATE="%A-%d.%m.%y"

CALENDAR_TEST=$(ps -x | grep 'Calendar' | grep -v 'grep' | awk '{print $6}' | cut -d "-" -f 3)

	if [ "$CALENDAR_TEST" = "calendar" ]; then

		kill 15 $(ps -x | grep 'Calendar' | grep -v 'grep' | awk '{print $1}')

	else

	BOX=$($ZEN_MENU --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON_OPEN_NOTES" \
	--extra-button=$EXTRA_BUTTON_MANAGE_NOTES --extra-button="$EXTRA_BUTTON_TODO" --title="$TITLE" --text="$TEXT" --date-format="$DATE" \
	--window-icon="$ICON_PATH")

	case $? in

		0 )

			mkdir -p $HOME/.zen-calendar

			if [ $? = 0 ]; then

				touch $HOME/.zen-calendar/"$BOX" && exo-open $HOME/.zen-calendar/"$BOX"

			fi
		;;

		1 )

			if [ "$BOX" = "$EXTRA_BUTTON_OPEN_NOTES" ]; then

				NOTE_MENU=$(ls $HOME/.zen-calendar/* | cut -d '/' -f 5 | grep -v 'ToDo' | $NOTE_BOX --title="$NOTE_MENU_TITLE" --text="$NOTE_MENU_TEXT" --column="Pick" --ok-label="$NOTE_OK_BUTTON" \
				--cancel-label="$NOTE_CANCEL_BUTTON" --extra-button="$NOTE_EXTRA_BUTTON" --window-icon="$ICON_PATH")

				case $? in

					0 )

						if [ "$NOTE_MENU" ]; then

							exo-open $HOME/.zen-calendar/"$NOTE_MENU" || xdg-open $HOME/.zen-calendar/"$NOTE_MENU"

						else

							notify-send -t 3000 "No note was chosen." & exec scripts-box.sh zenity_calendar_ex

						fi

					;;

					1 )

						if [ "$NOTE_MENU" = "$NOTE_EXTRA_BUTTON" ]; then

							exec scripts-box.sh zenity_calendar_ex

						fi

					;;

					* )

				esac

			fi

			if [ "$BOX" = "$EXTRA_BUTTON_TODO" ]; then

				touch $HOME/.zen-calendar/ToDo; exo-open $HOME/.zen-calendar/ToDo || xdg-open $HOME/.zen-calendar/ToDo

			elif [ "$BOX" = "$EXTRA_BUTTON_MANAGE_NOTES" ]; then

				 	BOX=$($NOTE_LIST --title="Calendar - Notes Manager" --text="Pick which note you want to delete." --ok-label="$MANAGE_OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
				 	--column="Pick" --column="Note" --extra-button="$NOTE_EXTRA_BUTTON" $LIST_PATH --separator=" " --multiple --window-icon="$ICON_PATH")

					 	case $? in
					
					 		0 )

								rm $HOME/.zen-calendar/$BOX; scripts-box.sh zenity_calendar_ex
					
					 		;;
					
					 		1 )
					
							 	[ "$BOX" = "Back" ] && exec scripts-box.sh zenity_calendar_ex
					
							;;
					
							* )
					
					 	esac

			fi

		;;

		* )

	esac

	fi

}

checkupdates() {

	echo ":: Checking for Updates:"; pamac checkupdates; echo ""; echo ":: Disk Usage Status:"; df -h | grep -v tmpfs | grep -v run; echo ""; read -p ":: Press enter to exit "

}

mirrors_cmd() {

	termite --exec='scripts-box.sh update_mirrors'

}

update_mirrors() {

MIRRORS=$(grep MIRRORS $HOME/.config/garuda-mirrors.conf | cut -d '"' -f2)

	echo ":: Updating Mirrors list"; echo ""; pkexec $MIRRORS; echo ""; read -p ":: Press enter to exit "

}

wise_update() {

echo -e ":: To use this script you need Timeshift configured first.";
read -r -p ":: Do you want to proceed? [y/N] " input

	case $input in

		[yY] | [eE] | [sS])

			clear; echo -e ":: Starting Timeshift to create system backup"
			sleep 1; sudo timeshift --create
			echo -e "\n:: Performing system update"
			pamac update; echo " "; read -p ":: Press Enter to exit ";;

		[nN] | [oO])

			clear
			echo -e ":: Click Desktop button and choose System Backupor type in terminal:\n\n    $ timeshift-gtk\n"
			read -p ":: Press Enter to exit ";;

		*)

			echo -e "\n	Invalid input...\n"; exec scripts-box.sh wise_update;;

	esac

}

obmenu_static() {

	switchmenu --static && xdotool key ctrl+space

}

obmenu_dynamic() {

	switchmenu --dynamic && xdotool key ctrl+space

}

simple_menu() {

	cp $HOME/.config/obmenu-generator/simpe_menu.pl /$HOME/.config/obmenu-generator/schema.pl && xdotool key ctrl+space

}

default_menu() {

	cp $HOME/.config/obmenu-generator/default_menu.pl /$HOME/.config/obmenu-generator/schema.pl && xdotool key ctrl+space

}

display_toggle() {

	if ! echo "xfsettingsd" | ps -A | grep -q xfsettingsd; then

		exec arandr

	else

		exec xfce4-display-settings

	fi

}

dark_mode_toggle() {

	if ! echo "Daily" | grep -q 'Studio' ~/.config/workflow-settings/session-test-config; then

		rb-dark-mode.sh daily_dark_mode_toggle

	else

		rb-dark-mode.sh studio_dark_mode_toggle

	fi

}

wspace1() {

WORKSPACE_TEST=$(grep '<number>' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$WORKSPACE_TEST|<number>1</number>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure; sleep 1; notify-send -t 2600 "Log again to apply changes."

}

wspaces2() {

WORKSPACE_TEST=$(grep '<number>' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$WORKSPACE_TEST|<number>2</number>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure; sleep 1; notify-send -t 2600 "Log again to apply changes."

}

wspaces3() {

WORKSPACE_TEST=$(grep '<number>' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$WORKSPACE_TEST|<number>3</number>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure; sleep 1; notify-send -t 2600 "Log again to apply changes."

}

wspaces4() {

WORKSPACE_TEST=$(grep '<number>' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$WORKSPACE_TEST|<number>4</number>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure; sleep 1; notify-send -t 2600 "Log again to apply changes."

}

wspaces5() {

WORKSPACE_TEST=$(grep '<number>' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$WORKSPACE_TEST|<number>5</number>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure; sleep 1; notify-send -t 2600 "Log again to apply changes."

}

wspaces6() {

WORKSPACE_TEST=$(grep '<number>' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$WORKSPACE_TEST|<number>6</number>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure; sleep 1; notify-send -t 2600 "Log again to apply changes."

}

wspaces7() {

WORKSPACE_TEST=$(grep '<number>' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$WORKSPACE_TEST|<number>7</number>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure; sleep 1; notify-send -t 2600 "Log again to apply changes."

}

wspaces8() {

WORKSPACE_TEST=$(grep '<number>' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$WORKSPACE_TEST|<number>8</number>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure; sleep 1; notify-send -t 2600 "Log again to apply changes."

}

add_workspace_tint() {

	xdotool key Super+Page_Up; exec tint2-buttons.sh sidemenu

}

remove_workspace_tint() {

	xdotool key Super+Page_Down; exec tint2-buttons.sh sidemenu

}

add_workspace_polybar() {

	xdotool key Super+Page_Up; exec polybar_buttons.sh sidemenu

}

remove_workspace_polybar() {

	xdotool key Super+Page_Down; exec polybar_buttons.sh sidemenu

}

rofi_places() {

	PLACES=$(ls -d $HOME/*/ | cut -d '/' -f 4 | rofi -dmenu -i -p '' "")

	if [ -z $PLACES ]; then

		break

		else

			cd $HOME/$PLACES; exo-open --launch FileManager

	fi

}

mount_device() {

	exec tint2-buttons.sh mount; sleep 0.5; exec tint2-buttons.sh device_menu

}

unmount_device() {

	exec tint2-buttons.sh unmount; sleep 0.5; exec tint2-buttons.sh device_menu	

}

panel_switch_on() {

	cp $HOME/.config/tint2/.tint2rc.bak $HOME/.config/tint2/tint2rc; rm $HOME/.config/tint2/.tint2rc.bak; sleep 0.5; killall -SIGUSR1 tint2

}

panel_switch_off() {

	cp $HOME/.config/tint2/tint2rc $HOME/.config/tint2/.tint2rc.bak; cp $HOME/.config/tint2/recbox-hidden.tint2rc $HOME/.config/tint2/tint2rc; sleep 0.5; killall -SIGUSR1 tint2

}

rofi_icons_on() {

	sed -i "s/    show-icons:      false;/    show-icons:      true;/" /usr/share/rofi/themes/RecBox-Dark.rasi; sed -i "s/    show-icons:      false;/    show-icons:      true;/" /usr/share/rofi/themes/RecBox-Light.rasi

}

rofi_icons_off() {

	sed -i "s/    show-icons:      true;/    show-icons:      false;/" /usr/share/rofi/themes/RecBox-Dark.rasi; sed -i "s/    show-icons:      true;/    show-icons:      false;/" /usr/share/rofi/themes/RecBox-Light.rasi

}

sound_system_alsa() {

	sed -i 's/rb-control-pa.sh/rb-control.sh/' $HOME/.config/openbox/rc.xml
	sed -i 's/rb-control-pa.sh/rb-control.sh/' $HOME/.config/tint2/recbox-daily-dark.tint2rc
	sed -i 's/rb-control-pa.sh/rb-control.sh/' $HOME/.config/tint2/recbox-daily-light.tint2rc
	sed -i 's/rb-control-pa.sh/rb-control.sh/' $HOME/.config/tint2/recbox-studio-dark.tint2rc
	sed -i 's/rb-control-pa.sh/rb-control.sh/' $HOME/.config/tint2/recbox-studio-light.tint2rc
	sed -i 's/rb-control-pa.sh/rb-control.sh/' $HOME/.config/tint2/tint2rc
	sed -i 's/rb-control-pa.sh/rb-control.sh/' $HOME/.config/polybar/modules/rb_volume.conf

	notify-send -t 2300 "Audio control set to use ALSA."

}

sound_system_pulseaudio() {

	sed -i 's/rb-control.sh/rb-control-pa.sh/' $HOME/.config/openbox/rc.xml
	sed -i 's/rb-control.sh/rb-control-pa.sh/' $HOME/.config/tint2/recbox-daily-dark.tint2rc
	sed -i 's/rb-control.sh/rb-control-pa.sh/' $HOME/.config/tint2/recbox-daily-light.tint2rc
	sed -i 's/rb-control.sh/rb-control-pa.sh/' $HOME/.config/tint2/recbox-studio-dark.tint2rc
	sed -i 's/rb-control.sh/rb-control-pa.sh/' $HOME/.config/tint2/recbox-studio-light.tint2rc
	sed -i 's/rb-control.sh/rb-control-pa.sh/' $HOME/.config/tint2/tint2rc
	sed -i 's/rb-control.sh/rb-control-pa.sh/' $HOME/.config/polybar/modules/rb_volume.conf

	notify-send -t 2300 "Audio control set to use PulseAudio."

}

start_smenu() {

MIN_MENU_HEIGHT_LIGHT=$(grep 'menu_height_min' $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc)
MAX_MENU_HEIGHT_LIGHT=$(grep 'menu_height_max' $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc)
MIN_MENU_HEIGHT_DARK=$(grep 'menu_height_min' $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc)
MAX_MENU_HEIGHT_DARK=$(grep 'menu_height_max' $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc)
SCREEN_NUMBER=$(xdotool getmouselocation --shell | grep 'SCREEN' | sed 's/^SCREEN=//')
SCREEN_RESOLUTION=$(xrandr | grep "Screen $SCREEN_NUMBER:" | awk '{print $10}' | sed 's/,$//')
PANEL_SIZE=$(grep 'panel_size' $HOME/.config/tint2/tint2rc | awk '{print $4}')
RESULT=$(expr $SCREEN_RESOLUTION - $PANEL_SIZE)

sed -i "s/$MIN_MENU_HEIGHT_LIGHT/menu_height_min = "$RESULT"/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc
sed -i "s/$MAX_MENU_HEIGHT_LIGHT/menu_height_max = "$RESULT"/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc
sed -i "s/$MIN_MENU_HEIGHT_DARK/menu_height_min = "$RESULT"/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc
sed -i "s/$MAX_MENU_HEIGHT_DARK/menu_height_max = "$RESULT"/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc

exec tint2-buttons.sh sidemenu

}

start_smenu_alt() {

MIN_MENU_HEIGHT_LIGHT=$(grep 'menu_height_min' $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc)
MAX_MENU_HEIGHT_LIGHT=$(grep 'menu_height_max' $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc)
MIN_MENU_HEIGHT_DARK=$(grep 'menu_height_min' $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc)
MAX_MENU_HEIGHT_DARK=$(grep 'menu_height_max' $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc)
SCREEN_NUMBER=$(xdotool getmouselocation --shell | grep 'SCREEN' | sed 's/^SCREEN=//')
SCREEN_RESOLUTION=$(xrandr | grep "Screen $SCREEN_NUMBER:" | awk '{print $10}' | sed 's/,$//')
PANEL_SIZE=$(grep 'height' $HOME/.config/polybar/master.conf | awk '{print $3}')
RESULT=$(expr $SCREEN_RESOLUTION - $PANEL_SIZE)

sed -i "s/$MIN_MENU_HEIGHT_LIGHT/menu_height_min = "$RESULT"/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc
sed -i "s/$MAX_MENU_HEIGHT_LIGHT/menu_height_max = "$RESULT"/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc
sed -i "s/$MIN_MENU_HEIGHT_DARK/menu_height_min = "$RESULT"/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc
sed -i "s/$MAX_MENU_HEIGHT_DARK/menu_height_max = "$RESULT"/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc

exec polybar-buttons.sh sidemenu

}

start_rclick_menu() {

RCMENU_RC=$(grep 'RCMENU_RC' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
MENU_ITEMS="$HOME/.config/jgmenu/right_click_menu.csv"

	jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/$RCMENU_RC" --csv-file=${MENU_ITEMS} 2>/dev/null

}

panel_task_tooltip_on() {

	sed -i 's/task_tooltip = 0/task_tooltip = 1/' $HOME/.config/tint2/tint2rc
	sed -i 's/task_tooltip = 0/task_tooltip = 1/' $HOME/.config/tint2/recbox-daily-light.tint2rc
	sed -i 's/task_tooltip = 0/task_tooltip = 1/' $HOME/.config/tint2/recbox-daily-dark.tint2rc
	sed -i 's/task_tooltip = 0/task_tooltip = 1/' $HOME/.config/tint2/recbox-studio-light.tint2rc
	sed -i 's/task_tooltip = 0/task_tooltip = 1/' $HOME/.config/tint2/recbox-studio-dark.tint2rc
	sed -i 's/task_tooltip = 0/task_tooltip = 1/' $HOME/.config/tint2/recbox-alternarive-dark.tint2rc
	exec garuda-tint2restart

}

panel_task_tooltip_off() {

	sed -i 's/task_tooltip = 1/task_tooltip = 0/' $HOME/.config/tint2/tint2rc
	sed -i 's/task_tooltip = 1/task_tooltip = 0/' $HOME/.config/tint2/recbox-daily-light.tint2rc
	sed -i 's/task_tooltip = 1/task_tooltip = 0/' $HOME/.config/tint2/recbox-daily-dark.tint2rc
	sed -i 's/task_tooltip = 1/task_tooltip = 0/' $HOME/.config/tint2/recbox-studio-light.tint2rc
	sed -i 's/task_tooltip = 1/task_tooltip = 0/' $HOME/.config/tint2/recbox-studio-dark.tint2rc
	sed -i 's/task_tooltip = 1/task_tooltip = 0/' $HOME/.config/tint2/recbox-alternarive-dark.tint2rc
	exec garuda-tint2restart

}

panel_task_thumbnail_on() {

	sed -i 's/task_tooltip = 0/task_tooltip = 1/' $HOME/.config/tint2/tint2rc
	sed -i 's/task_tooltip = 0/task_tooltip = 1/' $HOME/.config/tint2/recbox-daily-light.tint2rc
	sed -i 's/task_tooltip = 0/task_tooltip = 1/' $HOME/.config/tint2/recbox-daily-dark.tint2rc
	sed -i 's/task_tooltip = 0/task_tooltip = 1/' $HOME/.config/tint2/recbox-studio-light.tint2rc
	sed -i 's/task_tooltip = 0/task_tooltip = 1/' $HOME/.config/tint2/recbox-studio-dark.tint2rc
	sed -i 's/task_tooltip = 0/task_tooltip = 1/' $HOME/.config/tint2/recbox-alternarive-dark.tint2rc

	sed -i 's/task_thumbnail = 0/task_thumbnail = 1/' $HOME/.config/tint2/tint2rc
	sed -i 's/task_thumbnail = 0/task_thumbnail = 1/' $HOME/.config/tint2/recbox-daily-light.tint2rc
	sed -i 's/task_thumbnail = 0/task_thumbnail = 1/' $HOME/.config/tint2/recbox-daily-dark.tint2rc
	sed -i 's/task_thumbnail = 0/task_thumbnail = 1/' $HOME/.config/tint2/recbox-studio-light.tint2rc
	sed -i 's/task_thumbnail = 0/task_thumbnail = 1/' $HOME/.config/tint2/recbox-studio-dark.tint2rc
	sed -i 's/task_thumbnail = 0/task_thumbnail = 1/' $HOME/.config/tint2/recbox-alternarive-dark.tint2rc
	exec garuda-tint2restart

}

panel_task_thumbnail_off() {

	sed -i 's/task_tooltip = 1/task_tooltip = 0/' $HOME/.config/tint2/tint2rc
	sed -i 's/task_tooltip = 1/task_tooltip = 0/' $HOME/.config/tint2/recbox-daily-light.tint2rc
	sed -i 's/task_tooltip = 1/task_tooltip = 0/' $HOME/.config/tint2/recbox-daily-dark.tint2rc
	sed -i 's/task_tooltip = 1/task_tooltip = 0/' $HOME/.config/tint2/recbox-studio-light.tint2rc
	sed -i 's/task_tooltip = 1/task_tooltip = 0/' $HOME/.config/tint2/recbox-studio-dark.tint2rc
	sed -i 's/task_tooltip = 1/task_tooltip = 0/' $HOME/.config/tint2/recbox-alternarive-dark.tint2rc

	sed -i 's/task_thumbnail = 1/task_thumbnail = 0/' $HOME/.config/tint2/tint2rc
	sed -i 's/task_thumbnail = 1/task_thumbnail = 0/' $HOME/.config/tint2/recbox-daily-light.tint2rc
	sed -i 's/task_thumbnail = 1/task_thumbnail = 0/' $HOME/.config/tint2/recbox-daily-dark.tint2rc
	sed -i 's/task_thumbnail = 1/task_thumbnail = 0/' $HOME/.config/tint2/recbox-studio-light.tint2rc
	sed -i 's/task_thumbnail = 1/task_thumbnail = 0/' $HOME/.config/tint2/recbox-studio-dark.tint2rc
	sed -i 's/task_thumbnail = 1/task_thumbnail = 0/' $HOME/.config/tint2/recbox-alternarive-dark.tint2rc
	exec garuda-tint2restart

}

stacking_center() {

TEST=$(grep 'WINDOW_STACKING_TOP' $HOME/.config/openbox/rc.xml | sed 's/  *//')
STACKING_TOP=$(grep 'WINDOW_STACKING_TOP' $HOME/.config/openbox/rc.xml | sed 's/  *//')
STACKING_BOTTOM=$(grep 'WINDOW_STACKING_BOTTOM' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	if [ "$TEST" = "<position> <!-- WINDOW_STACKING_TOP -->" ]; then

		notify-send -t 2600 "Stacking windows to center already enabled."

	elif [ "$TEST" = "<!--<position> WINDOW_STACKING_TOP" ]; then

		sed -i "s|$STACKING_TOP|<position> <!-- WINDOW_STACKING_TOP -->|" $HOME/.config/openbox/rc.xml
		sed -i "s|$STACKING_BOTTOM|</position> <!-- WINDOW_STACKING_BOTTOM -->|" $HOME/.config/openbox/rc.xml
		openbox --reconfigure; notify-send -t 2600 "Stacking windows to center enabled."

	fi

}

smart_stacking() {

TEST=$(grep 'WINDOW_STACKING_TOP' $HOME/.config/openbox/rc.xml | sed 's/  *//')
STACKING_TOP=$(grep 'WINDOW_STACKING_TOP' $HOME/.config/openbox/rc.xml | sed 's/  *//')
STACKING_BOTTOM=$(grep 'WINDOW_STACKING_BOTTOM' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	if [ "$TEST" = "<!--<position> WINDOW_STACKING_TOP" ]; then

		notify-send -t 2600 "Smart stacking already enabled."

	elif [ "$TEST" = "<position> <!-- WINDOW_STACKING_TOP -->" ]; then

		sed -i "s|$STACKING_TOP|<!--<position> WINDOW_STACKING_TOP|" $HOME/.config/openbox/rc.xml
		sed -i "s|$STACKING_BOTTOM|</position> WINDOW_STACKING_BOTTOM -->|" $HOME/.config/openbox/rc.xml
		openbox --reconfigure; notify-send -t 2600 "Smart stacking enabled."

	fi

}

decor_toggle() {

DECORATION_TEST=$(grep 'WINDOW_DECORATION' $HOME/.config/openbox/rc.xml | sed -e 's|<decor>\(.*\)</decor> <!-- WINDOW_DECORATION -->|\1|' | sed 's/  *//')

	if [ $DECORATION_TEST = "yes" ]; then

		sed -i "s|<decor>$DECORATION_TEST</decor> <!-- WINDOW_DECORATION -->|<decor>no</decor> <!-- WINDOW_DECORATION -->|" $HOME/.config/openbox/rc.xml; openbox --reconfigure; notify-send -t 2300 "Window decorations Disabled."

	elif [ $DECORATION_TEST = "no" ]; then

		sed -i "s|<decor>$DECORATION_TEST</decor> <!-- WINDOW_DECORATION -->|<decor>yes</decor> <!-- WINDOW_DECORATION -->|" $HOME/.config/openbox/rc.xml; openbox --reconfigure; notify-send -t 2300 "Window decorations Disabled."

	fi

}

classic_left() {

TITLE_LAYOUT=$(grep 'titleLayout' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$TITLE_LAYOUT|<titleLayout>CIML</titleLayout>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure

}

classic_right() {

TITLE_LAYOUT=$(grep 'titleLayout' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$TITLE_LAYOUT|<titleLayout>LMIC</titleLayout>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure

}

custom_layout() {

LAYOUT_BOX_TITLE="Custom Layout"
LAYOUT_EXAMPLES="N:	Window Icon\nL:	Window Label (AKA title).\nI:	Iconify/Minimize\nM:	Maximize\nC:	Close\nS:	Shade (roll up/down)\nD:	Omnipresent (on all desktops)"
LAYOUT_OK_BUTTON="Apply"
LAYOUT_CANCEL_BUTTON="Close"
LAYOUT_ENTRY_TEXT=$(grep 'titleLayout' $HOME/.config/openbox/rc.xml | sed -e 's|<titleLayout>\(.*\)</titleLayout>|\1|' | sed 's/  *//')

	BOX=$(zenity --entry --entry-text="$LAYOUT_ENTRY_TEXT" --title="$LAYOUT_BOX_TITLE" --text="$LAYOUT_EXAMPLES" \
	--ok-label="$LAYOUT_OK_BUTTON" --cancel-label="$LAYOUT_CANCEL_BUTTON" --width=260)

		case $? in

	        0)

				sed -i "s|<titleLayout>$LAYOUT_ENTRY_TEXT</titleLayout>|<titleLayout>$BOX</titleLayout>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure

			;;

	        1)
                echo "No title layout selected.";;

	        -1)

                echo "An unexpected error has occurred.";;

		esac

}

elementary_left() {

TITLE_LAYOUT=$(grep 'titleLayout' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$TITLE_LAYOUT|<titleLayout>CLM</titleLayout>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure

}

elementary_right() {

TITLE_LAYOUT=$(grep 'titleLayout' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$TITLE_LAYOUT|<titleLayout>MLC</titleLayout>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure

}

gnome_left() {

TITLE_LAYOUT=$(grep 'titleLayout' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$TITLE_LAYOUT|<titleLayout>CL</titleLayout>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure

}

gnome_right() {

TITLE_LAYOUT=$(grep 'titleLayout' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$TITLE_LAYOUT|<titleLayout>LC</titleLayout>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure

}

label_only() {

TITLE_LAYOUT=$(grep 'titleLayout' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	sed -i "s|$TITLE_LAYOUT|<titleLayout>L</titleLayout>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure

}

double_click_maximize() {

TITLE_DOUBLECLICK=$(grep 'DOUBLECLICK_TITLEBAR' $HOME/.config/openbox/rc.xml | sed -e 's|<action name="\(.*\)"/> <!-- DOUBLECLICK_TITLEBAR -->|\1|' | sed 's/  *//')

	if [ $TITLE_DOUBLECLICK = "ToggleMaximize" ]; then

		exit 0

	elif [ $TITLE_DOUBLECLICK = "Iconify" ]; then

		sed -i 's|<action name="Iconify"/> <!-- DOUBLECLICK_TITLEBAR -->|<action name="ToggleMaximize"/> <!-- DOUBLECLICK_TITLEBAR -->|' $HOME/.config/openbox/rc.xml; openbox --reconfigure

	elif [ $TITLE_DOUBLECLICK = "ToggleShade" ]; then

		sed -i 's|<action name="ToggleShade"/> <!-- DOUBLECLICK_TITLEBAR -->|<action name="ToggleMaximize"/> <!-- DOUBLECLICK_TITLEBAR -->|' $HOME/.config/openbox/rc.xml; openbox --reconfigure

	fi

}

double_click_minimize() {

TITLE_DOUBLECLICK=$(grep 'DOUBLECLICK_TITLEBAR' $HOME/.config/openbox/rc.xml | sed -e 's|<action name="\(.*\)"/> <!-- DOUBLECLICK_TITLEBAR -->|\1|' | sed 's/  *//')

	if [ $TITLE_DOUBLECLICK = "Iconify" ]; then

		exit 0

	elif [ $TITLE_DOUBLECLICK = "ToggleMaximize" ]; then

		sed -i 's|<action name="ToggleMaximize"/> <!-- DOUBLECLICK_TITLEBAR -->|<action name="Iconify"/> <!-- DOUBLECLICK_TITLEBAR -->|' $HOME/.config/openbox/rc.xml; openbox --reconfigure

	elif [ $TITLE_DOUBLECLICK = "ToggleShade" ]; then

		sed -i 's|<action name="ToggleShade"/> <!-- DOUBLECLICK_TITLEBAR -->|<action name="Iconify"/> <!-- DOUBLECLICK_TITLEBAR -->|' $HOME/.config/openbox/rc.xml; openbox --reconfigure

	fi

}

double_click_shade() {

TITLE_DOUBLECLICK=$(grep 'DOUBLECLICK_TITLEBAR' $HOME/.config/openbox/rc.xml | sed -e 's|<action name="\(.*\)"/> <!-- DOUBLECLICK_TITLEBAR -->|\1|' | sed 's/  *//')

	if [ $TITLE_DOUBLECLICK = "ToggleShade" ]; then

		exit 0

	elif [ $TITLE_DOUBLECLICK = "ToggleMaximize" ]; then

		sed -i 's|<action name="ToggleMaximize"/> <!-- DOUBLECLICK_TITLEBAR -->|<action name="ToggleShade"/> <!-- DOUBLECLICK_TITLEBAR -->|' $HOME/.config/openbox/rc.xml; openbox --reconfigure

	elif [ $TITLE_DOUBLECLICK = "Iconify" ]; then

		sed -i 's|<action name="Iconify"/> <!-- DOUBLECLICK_TITLEBAR -->|<action name="ToggleShade"/> <!-- DOUBLECLICK_TITLEBAR -->|' $HOME/.config/openbox/rc.xml; openbox --reconfigure

	fi

}

middle_click_maximize() {

TITLEBAR_MIDDLECLICK=$(grep 'MIDDLECLICK_TITLEBAR' $HOME/.config/openbox/rc.xml | sed -e 's|<action name="\(.*\)"/> <!-- MIDDLECLICK_TITLEBAR -->|\1|' | sed 's/  *//')

	if [ $TITLEBAR_MIDDLECLICK = "ToggleMaximize" ]; then

		exit 0

	elif [ $TITLEBAR_MIDDLECLICK = "Iconify" ]; then

		sed -i 's|<action name="Iconify"/> <!-- MIDDLECLICK_TITLEBAR -->|<action name="ToggleMaximize"/> <!-- MIDDLECLICK_TITLEBAR -->|' $HOME/.config/openbox/rc.xml; openbox --reconfigure

	elif [ $TITLEBAR_MIDDLECLICK = "ToggleShade" ]; then

		sed -i 's|<action name="ToggleShade"/> <!-- MIDDLECLICK_TITLEBAR -->|<action name="ToggleMaximize"/> <!-- MIDDLECLICK_TITLEBAR -->|' $HOME/.config/openbox/rc.xml; openbox --reconfigure

	fi

}

middle_click_minimize() {

TITLEBAR_MIDDLECLICK=$(grep 'MIDDLECLICK_TITLEBAR' $HOME/.config/openbox/rc.xml | sed -e 's|<action name="\(.*\)"/> <!-- MIDDLECLICK_TITLEBAR -->|\1|' | sed 's/  *//')

	if [ $TITLEBAR_MIDDLECLICK = "Iconify" ]; then

		exit 0

	elif [ $TITLEBAR_MIDDLECLICK = "ToggleMaximize" ]; then

		sed -i 's|<action name="ToggleMaximize"/> <!-- MIDDLECLICK_TITLEBAR -->|<action name="Iconify"/> <!-- MIDDLECLICK_TITLEBAR -->|' $HOME/.config/openbox/rc.xml; openbox --reconfigure

	elif [ $TITLEBAR_MIDDLECLICK = "ToggleShade" ]; then

		sed -i 's|<action name="ToggleShade"/> <!-- MIDDLECLICK_TITLEBAR -->|<action name="Iconify"/> <!-- MIDDLECLICK_TITLEBAR -->|' $HOME/.config/openbox/rc.xml; openbox --reconfigure

	fi

}

middle_click_shade() {

TITLEBAR_MIDDLECLICK=$(grep 'MIDDLECLICK_TITLEBAR' $HOME/.config/openbox/rc.xml | sed -e 's|<action name="\(.*\)"/> <!-- MIDDLECLICK_TITLEBAR -->|\1|' | sed 's/  *//')

	if [ $TITLEBAR_MIDDLECLICK = "ToggleShade" ]; then

		exit 0

	elif [ $TITLEBAR_MIDDLECLICK = "ToggleMaximize" ]; then

		sed -i 's|<action name="ToggleMaximize"/> <!-- MIDDLECLICK_TITLEBAR -->|<action name="ToggleShade"/> <!-- MIDDLECLICK_TITLEBAR -->|' $HOME/.config/openbox/rc.xml; openbox --reconfigure

	elif [ $TITLEBAR_MIDDLECLICK = "Iconify" ]; then

		sed -i 's|<action name="Iconify"/> <!-- MIDDLECLICK_TITLEBAR -->|<action name="ToggleShade"/> <!-- MIDDLECLICK_TITLEBAR -->|' $HOME/.config/openbox/rc.xml; openbox --reconfigure

	fi

}

toggle_border() {

BORDER_TEST=$(grep 'keepBorder' $HOME/.config/openbox/rc.xml | sed 's/  *//')

	if [ $BORDER_TEST = "<keepBorder>yes</keepBorder>" ]; then

		sed -i "s|$BORDER_TEST|<keepBorder>no</keepBorder>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure

	elif [ $BORDER_TEST = "<keepBorder>no</keepBorder>" ]; then

		sed -i "s|$BORDER_TEST|<keepBorder>yes</keepBorder>|" $HOME/.config/openbox/rc.xml; openbox --reconfigure

	fi


}

### MAIN MENU SCRIPTS

stay_alive_on() {

	sed -i 's/stay_alive = 0/stay_alive = 1/' $HOME/.config/jgmenu/jgmenurc
	sed -i 's/stay_alive = 0/stay_alive = 1/' $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu
	sed -i 's/stay_alive = 0/stay_alive = 1/' $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu
	notify-send -t 2600 "Menu stay alive mode enabled."

}

stay_alive_off() {

	sed -i 's/stay_alive = 1/stay_alive = 0/' $HOME/.config/jgmenu/jgmenurc
	sed -i 's/stay_alive = 1/stay_alive = 0/' $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu
	sed -i 's/stay_alive = 1/stay_alive = 0/' $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu
	notify-send -t 2600 "Menu stay alive mode disabled."

}

mmenu_font_change() {

	WINDOW_TITLE="Choose Font"
	WINDOW_TEXT="Input here font name and size."
	OK_BUTTON="Apply"
	CANCEL_BUTTON="Close"
	ENTRY_TEXT=$(grep 'font =' $HOME/.config/jgmenu/jgmenurc | sed 's/.*= //')

	ACTIVE_PATH="$HOME/.config/jgmenu/jgmenurc"
	LIGHT_PATH="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu"
	DARK_PATH="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu"

	FONT_BOX=$(zenity --entry --entry-text="$ENTRY_TEXT" --title="$WINDOW_TITLE" --text="$WINDOW_TEXT" \
	--ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON")

	case $? in

		0) sed -i "s/font = $ENTRY_TEXT/font = $FONT_BOX/" "$ACTIVE_PATH" "$LIGHT_PATH" "$DARK_PATH"; exec jgmenu;;
		1) echo "Window Close";;
		-1) echo "An unexpected error.";;

	esac

}

### COMMANDS

case "$1" in
	touchpad) touchpad;;
	pdf_menu) pdf_menu;;
	zenity_calendar_ex) zenity_calendar_ex;;
	recbox_conf) recbox_conf;;
	my_preset) my_preset;;
	disk_wiping) disk_wiping;;
	checkupdates) checkupdates;;
	mirrors_cmd) mirrors_cmd;;
	update_mirrors) update_mirrors;;
	wise_update) wise_update;;
	obmenu_static) obmenu_static;;
	obmenu_dynamic) obmenu_dynamic;;
	display_toggle) display_toggle;;
	dark_mode_toggle) dark_mode_toggle;;
	wspace1) wspace1;;
	wspaces2) wspaces2;;
	wspaces3) wspaces3;;
	wspaces4) wspaces4;;
	wspaces5) wspaces5;;
	wspaces6) wspaces6;;
	wspaces7) wspaces7;;
	wspaces8) wspaces8;;
	add_workspace_tint) add_workspace_tint;;
	remove_workspace_tint) remove_workspace_tint;;
	add_workspace_polybar) add_workspace_polybar;;
	remove_workspace_polybar) remove_workspace_polybar;;
	rofi_places) rofi_places;;
	mount_device) mount_device;;
	unmount_device) unmount_device;;
	panel_switch_on) panel_switch_on;;
	panel_switch_off) panel_switch_off;;
	rofi_icons_on) rofi_icons_on;;
	rofi_icons_off) rofi_icons_off;;
	sound_system_alsa) sound_system_alsa;;
	sound_system_pulseaudio) sound_system_pulseaudio;;
	start_smenu) start_smenu;;
	start_smenu_alt) start_smenu_alt;;
	start_rclick_menu) start_rclick_menu;;
	panel_task_tooltip_on) panel_task_tooltip_on;;
	panel_task_tooltip_off) panel_task_tooltip_off;;
	panel_task_thumbnail_on) panel_task_thumbnail_on;;
	panel_task_thumbnail_off) panel_task_thumbnail_off;;
	stacking_center) stacking_center;;
	smart_stacking) smart_stacking;;
	classic_left) classic_left;;
	classic_right) classic_right;;
	custom_layout) custom_layout;;
	elementary_left) elementary_left;;
	elementary_right) elementary_right;;
	gnome_left) gnome_left;;
	gnome_right) gnome_right;;
	label_only) label_only;;
	double_click_maximize) double_click_maximize;;
	double_click_minimize) double_click_minimize;;
	double_click_shade) double_click_shade;;
	middle_click_maximize) middle_click_maximize;;
	middle_click_minimize) middle_click_minimize;;
	middle_click_shade) middle_click_shade;;
	toggle_border) toggle_border;;
	--stay-alive-on) stay_alive_on;;
	--stay-alive-off) stay_alive_off;;
	--mmenu-font-change) mmenu_font_change;;
	simple_menu) simple_menu;;
	default_menu) default_menu;;
	--version | --ver) echo -e "\n    Version 3.9\n";;
	*)

	echo -e "

	RecBox Scripts Box\n
	Made by Projekt:Root projekt.root@tuta.io
	for RecBox and Garuda OpenBox.
	If you want to use script on other setup 
	probably you'll need to modify it.\n
	Usage:\n
	scripts-box.sh [ OPTION ]

	Options:

	> touchpad                     - execute rofi touchpad menu
	> pdf_menu                     - execute zenity documentation menu
	> zenity_calendar_ex           - execute calendar with notes
	> update_cmd                   - execute checkupdates comand in terminal
	> checkupdates                 - execute pamac checkupdates
	> mirrors_cmd                  - execute update_mirrors in terminal
	> update_mirrors               - execute pacman-mirrors update
	> wise_update                  - execute update script (snapshot & update)
	> obmenu_static                - executing switched menu
	> obmenu_dynamic               - executing switched menu
	> display_toggle               - enable arandr or xfce4-display-settings
	> dark_mode_toggle             - enable dark mode for Dail or Studio
	> wspaces1-8                   - change number of workspaces
	> rofi_places                  - execute Rofi Places menu
	> rofi_icons_on                - enable icons in rofi menu
	> rofi_icons_off               - disable icons in rofi menu
	> start_smenu                  - change side menu height and y position (tint2)
	> start_smenu_alt              - change side menu height and y position (polybar)
	> start_rclick_menu            - opens custom jgmenu (right click)
	> panel_task_tooltip_on        - enable task tooltip
	> panel_task_tooltip_off       - disable task tooltip
	> panel_task_thumbnail_on      - enable task task thumbnail
	> panel_task_thumbnail_off     - disable task thumbnail
	> stacking_center              - WM stack windows to desktop center
	> smart_stacking               - Enable WM Smart window stacking
	> decor_toggle                 - Enable/Disable window decorations
	> classic_left                 - maximize, minimize and close icons set on left
	> classic_right                - maximize, minimize and close icons set on right
	> custom_layout                - user choice of title layout
	> elementary_left              - close button on left, maximize on right side
	> elementary_left              - close button on right, maximize on left side
	> gnome_left                   - close button on left
	> gnome_right                  - close button on right
	> label_only                   - only label in title layout
	> none_layout                  - empty title layout
	> double_click_maximize        - maximize window after double click title layout
	> double_click_minimize        - minimize window after double click title layout
	> double_click_shade           - roll up/down window after middle click
	> middle_click_maximize        - maximize window after middle click title layout
	> middle_click_minimize        - minimize window after middle click title layout
	> middle_click_shade           - roll up/down window after middle click
	> toggle_border                - turn on/off borders when window don't have decoration

	Main Menu (jgmenu):

	--mmenu-font-change            open zenity entry widdget, type font name and size
	--stay-alive-on                menu will run in background
	--stay-alive-off               quit all processes when menu exit

	--version, --ver               print script version
" >&2
exit 1;;
esac
exit 0
