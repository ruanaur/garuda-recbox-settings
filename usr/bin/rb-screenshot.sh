#!/usr/bin/sh

DESKTOP_TITLE="Capture Desktop"
WINDOW_TITLE="Capture Window"
REGION_TITLE="Capture a Region"
ENTR_TEXT="Save as..."
DEFAULT_DESKTOP_NAME="Desktop-%Y-%m-%d-%S.png"
DEFAULT_WINDOW_NAME="Window-%Y-%m-%d-%S.png"
DEFAULT_REGION_NAME="Captured-Region-%Y-%m-%d-%S.png"
OK_BUTTON="Apply"
CANCEL_BUTTON="Cancel"
ICON_PATH="/usr/share/icons/Flat-Remix-Green/apps/scalable/gnome-screenshot.svg"

ENTR_BOX="zenity --entry --width=360"

scrot_desktop_now() {

	MENU_BOX=$($ENTR_BOX --title="$DESKTOP_TITLE" --text="$ENTR_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --entry-text="$DEFAULT_DESKTOP_NAME" --window-icon="$ICON_PATH")

		if [ $MENU_BOX ]; then

			exec scrot -d 1 $MENU_BOX -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f'

		elif [ -z $MENU_BOX ]; then

				echo "exit"

		fi

}

scrot_desktop_after5() {

	MENU_BOX=$($ENTR_BOX --title="$DESKTOP_TITLE" --text="$ENTR_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --entry-text="$DEFAULT_DESKTOP_NAME" --window-icon="$ICON_PATH")

		if [ $MENU_BOX ]; then

			exec scrot -d 5 $MENU_BOX -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f'

		elif [ -z $MENU_BOX ]; then

				echo "exit"

		fi

}

scrot_desktop_after10() {

	MENU_BOX=$($ENTR_BOX --title="$DESKTOP_TITLE" --text="$ENTR_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --entry-text="$DEFAULT_DESKTOP_NAME" --window-icon="$ICON_PATH")

		if [ $MENU_BOX ]; then

			exec scrot -d 10 $MENU_BOX -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f'

		elif [ -z $MENU_BOX ]; then

				echo "exit"

		fi

}

scrot_window_now() {

	MENU_BOX=$($ENTR_BOX --title="$WINDOW_TITLE" --text="$ENTR_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --entry-text="$DEFAULT_WINDOW_NAME" --window-icon="$ICON_PATH")

		if [ $MENU_BOX ]; then

			exec scrot -d 1 -u $MENU_BOX -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f'

		elif [ -z $MENU_BOX ]; then

				echo "exit"

		fi

}

scrot_window_after5() {

	MENU_BOX=$($ENTR_BOX --title="$WINDOW_TITLE" --text="$ENTR_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --entry-text="$DEFAULT_WINDOW_NAME" --window-icon="$ICON_PATH")

		if [ $MENU_BOX ]; then

			exec scrot -d 5 -u $MENU_BOX -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f'

		elif [ -z $MENU_BOX ]; then

				echo "exit"

		fi

}

scrot_window_after10() {

	MENU_BOX=$($ENTR_BOX --title="$WINDOW_TITLE" --text="$ENTR_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --entry-text="$DEFAULT_WINDOW_NAME" --window-icon="$ICON_PATH")

		if [ $MENU_BOX ]; then

			exec scrot -d 10 -u $MENU_BOX -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f'

		elif [ -z $MENU_BOX ]; then

				echo "exit"

		fi

}

scrot_region() {

	MENU_BOX=$($ENTR_BOX --title="$REGION_TITLE" --text="$ENTR_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" --entry-text="$DEFAULT_REGION_NAME" --window-icon="$ICON_PATH")

		if [ $MENU_BOX ]; then

			exec scrot -s -d 1 $MENU_BOX -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f'

		elif [ -z $MENU_BOX ]; then

				echo "exit"

		fi

}

rofi_scrot_menu() {

	SCROT=$(echo "Desktop|Desktop 5s delay|Desktop 10s delay| |Capture a Region| |Window|Window 5s delay|Window 10s delay" | \
	rofi -sep "|" -dmenu -i -p ' ' "")

	case $SCROT in

		*"Desktop" )

			SCROT_DESKTOP=$(echo "Desktop-%Y-%m-%d-%S.png" | rofi -sep "|" -dmenu -i -p ' ' "")

				if [ $SCROT_DESKTOP = "Desktop-%Y-%m-%d-%S.png" ]; then

					exec scrot -d 1 'RecBox_%Y-%m-%d-%S.png' -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f'

				elif [ $SCROT_DESKTOP ]; then

					exec scrot -d 1 $SCROT_DESKTOP -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f'

				elif [ -z $SCROT_DESKTOP ]; then

					echo "exit"

				fi

		;;
		*"Desktop 5s delay" ) exec scrot -d 5 'RecBox_%Y-%m-%d-%S.png' -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f' ;;
		*"Desktop 10s delay" ) exec scrot -d 10 'RecBox_%Y-%m-%d-%S.png' -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f' ;;
		*"Capture a Region" ) exec scrot -s 'RecBox_%Y-%m-%d-%S.png' -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f' ;;
		*"Window" ) exec scrot -d 1 -u 'RecBox_%Y-%m-%d-%S.png' -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f' ;;
		*"Window 5s delay" ) exec scrot -d 5 -u 'RecBox_%Y-%m-%d-%S.png' -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f' ;;
		*"Window 10s delay" ) exec scrot -d 10 -u 'RecBox_%Y-%m-%d-%S.png' -e 'mv $f $$(xdg-user-dir PICTURES) ; viewnior $$(xdg-user-dir PICTURES)/$f' ;;
		*)

	esac

}

case $1 in

	--scrot-desktop-now) scrot_desktop_now;;
	--scrot_desktop-after5) scrot_desktop_after5;;
	--scrot-desktop-after10) scrot_desktop_after10;;
	--scrot-window-now) scrot_window_now;;
	--scrot-window-after5) scrot_window_after5;;
	--scrot-window-after10) scrot_window_after10;;
	--scrot-region) scrot_region;;
	--rofi-scrot-menu) rofi_scrot_menu;; 
	--version | --ver) 	echo -e "\n    Version 1.2\n";;
	*)

	echo -e "
	RecBox Screen Shots\n
	Made by Projekt:Root projekt.root@tuta.io
	for RecBox and Garuda OpenBox.
	If you want to use script on other setup 
	probably you'll need to modify it.\n
	Usage:\n
	scripts-box.sh [ OPTION ]\n
	Options:\n
	--scrot-desktop-now
	--scrot-desktop-after5
	--scrot-desktop-after10
	--scrot-window-now
	--scrot-window-after5
	--scrot-window-after10
	--scrot-region
	--rofi-scrot-menu
	--ver, --version
" >&2

exit 1;;

esac

exit 0
