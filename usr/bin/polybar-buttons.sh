#!/usr/bin/sh

# This script is made with help
# https://github.com/nwg-piotr/tint2-executors/tree/master/arch-package#t2ec---update-menu

# VERSION=3.4

PANEL_MENUS_CONIG="$HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc"
SIDEMENU_CONFIG=$(grep 'SIDEMENU_RC' $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)

places() {

MENU_ITEMS=$(mktemp)

trap "rm -f ${MENU_ITEMS}" EXIT

	if ! echo "Thunar" | grep -q "custom-FileManager" $HOME/.config/xfce4/helpers.rc; then

		ls -d /run/media/$USER/* | sed -e "s/[[:space:]]/\\\ /g" > ${MENU_ITEMS}
		sed -i 's|/$||' ${MENU_ITEMS}
		sed -i 's|$|,exo-open /|' ${MENU_ITEMS}
		sed -i 's/r[^h]*$/&&/' ${MENU_ITEMS}
		sed -i "s|^/run/media/$USER/||" ${MENU_ITEMS}
		sed -i 's|,exo-open /$||' ${MENU_ITEMS}
		sed -i '1i ^sep(USB Device | Partition)' ${MENU_ITEMS}
		sed -i "\$a ^sep()" ${MENU_ITEMS}
		sed -i "\$a ^sep(Places)" ${MENU_ITEMS}

		echo 'Home,exo-open $HOME' >> ${MENU_ITEMS}

		ls -d $HOME/*/ >> ${MENU_ITEMS}
		sed -i 's|/$||' ${MENU_ITEMS}
		sed -i 's|$|,exo-open /|' ${MENU_ITEMS}
		sed -i 's/h[^h]*$/&&/' ${MENU_ITEMS}
		sed -i "s|^/home/$USER/||" ${MENU_ITEMS}
		sed -i 's/^//' ${MENU_ITEMS}
		sed -i 's|,exo-open /$||' ${MENU_ITEMS}
		sed -i 's/^sep()/^sep()/' ${MENU_ITEMS}
		sed -i 's/^sep(USB Device | Partition)/^sep(USB Device | Partition)/' ${MENU_ITEMS}
		sed -i 's/^sep(Places)/^sep(Places)/' ${MENU_ITEMS}

	else

		ls -d /run/media/$USER/* | sed -e "s/[[:space:]]/\\\ /g" > ${MENU_ITEMS}
		sed -i 's|$|,termite --exec="ranger /|' ${MENU_ITEMS}
		sed -i 's/r[^h]*$/&&/' ${MENU_ITEMS}
		sed -i "s|^/run/media/$USER/||" ${MENU_ITEMS}
		sed -i 's|,termite --exec="ranger /$||' ${MENU_ITEMS}
		sed -i '1i ^sep(USB Device | Partition)' ${MENU_ITEMS}
		sed -i "\$a ^sep()" ${MENU_ITEMS}
		sed -i "\$a ^sep(Places)" ${MENU_ITEMS}

		echo 'Home,exo-open $HOME' >> ${MENU_ITEMS}

		ls -d $HOME/*/ >> ${MENU_ITEMS}
		sed -i 's|/$||' ${MENU_ITEMS}
		sed -i 's|$|,termite --exec="ranger /|' ${MENU_ITEMS}
		sed -i 's/h[^h]*$/&&/' ${MENU_ITEMS}
		sed -i "s|^/home/$USER/||" ${MENU_ITEMS}
		sed -i 's/^//' ${MENU_ITEMS}
		sed -i 's|,termite --exec="ranger /$||' ${MENU_ITEMS}
		sed -i 's/$/"/' ${MENU_ITEMS}
		sed -i 's/^sep()/^sep()/' ${MENU_ITEMS}
		sed -i 's/^sep(USB Device | Partition)/^sep(USB Device | Partition)/' ${MENU_ITEMS}
		sed -i 's/^sep(Places)/^sep(Places)/' ${MENU_ITEMS}

	fi

jgmenu --config-file=${PANEL_MENUS_CONIG} --csv-file=${MENU_ITEMS} 2>/dev/null

}

window() {

MENU_ITEMS="$HOME/.config/jgmenu/window.csv"
jgmenu --config-file=${PANEL_MENUS_CONIG} --csv-file=${MENU_ITEMS} 2>/dev/null

}

tools() {

MENU_ITEMS="$HOME/.config/jgmenu/tools.csv"
jgmenu --config-file=${PANEL_MENUS_CONIG} --csv-file=${MENU_ITEMS} 2>/dev/null

}

info() {

MENU_ITEMS="$HOME/.config/jgmenu/info.csv"
dunstctl close-all; jgmenu --config-file=${PANEL_MENUS_CONIG} --csv-file=${MENU_ITEMS} 2>/dev/null

}

software() {

MENU_ITEMS="$HOME/.config/jgmenu/software.csv"
jgmenu --config-file=${PANEL_MENUS_CONIG} --csv-file=${MENU_ITEMS} 2>/dev/null

}

device_menu() {

MENU_ITEMS=$(mktemp)

trap "rm -f ${MENU_ITEMS}" EXIT

cat << 'EOF' > ${MENU_ITEMS}
^sep()
^sep(Device Menu    )
^sep()
Mount,scripts-box.sh mount_device
Unmount,scripts-box.sh unmount_device
^sep()
^sep(Open Device    )
^sep()
EOF

	if ! echo "Thunar" | grep -q "custom-FileManager" $HOME/.config/xfce4/helpers.rc; then

		ls -d /run/media/$USER/* | sed 's|$|,exo-open /|' | sed 's/r[^h]*$/&&/' | sed "s|^/run/media/$USER/||" | sed 's|,exo-open /$||' | sed 's/^//' >> ${MENU_ITEMS}

	else

		ls -d /run/media/$USER/* | sed 's|$|,termite --exec="ranger /|' | sed 's/r[^h]*$/&&/' | sed "s|^/run/media/$USER/||" | sed 's|,termite --exec="ranger /$||' | sed 's/$/"/' | sed 's/^//' >> ${MENU_ITEMS}

	fi

cat << 'EOF' >> ${MENU_ITEMS}
^sep()
<b>Back</b>,polybar-buttons.sh sidemenu
EOF

jgmenu --config-file="~/.config/openbox/jgmenu-theme-configs/$SIDEMENU_CONFIG" --csv-file=${MENU_ITEMS} 2>/dev/null

}

mount() {

MENU_ITEMS=$(mktemp)

trap "rm -f ${MENU_ITEMS}" EXIT

cat << 'EOF' > ${MENU_ITEMS}
^sep()
^sep(Mount    )
^sep()
EOF

ls /dev/sd* | grep -v sda | sed 's|$|,udisksctl mount -b |' | sed 's|/dev/sd[^/]*$|&&|' | sed 's|,udisksctl mount -b $||' | sed 's|/dev/||' | sed 's/^/ /' >> ${MENU_ITEMS}

cat << 'EOF' >> ${MENU_ITEMS}
^sep()
<b>Back</b>,polybar-buttons.sh device_menu
EOF

jgmenu --config-file="~/.config/openbox/jgmenu-theme-configs/$SIDEMENU_CONFIG" --csv-file=${MENU_ITEMS} 2>/dev/null

}

unmount() {

MENU_ITEMS=$(mktemp)

trap "rm -f ${MENU_ITEMS}" EXIT

cat << 'EOF' > ${MENU_ITEMS}
^sep()
^sep(Unmount    )
^sep()
EOF

ls -d /run/media/$USER/* | sed 's|$|,udisksctl unmount -b /dev/disk/by-label/|' | sed 's|\([a-z]*\).*$|&&|' | sed "s|^/run/media/$USER/||" | sed 's|,udisksctl unmount -b /dev/disk/by-label/$||' | sed 's/^/ /' >> ${MENU_ITEMS}

cat << 'EOF' >> ${MENU_ITEMS}
^sep()
<b>Back</b>,polybar-buttons.sh device_menu
EOF

jgmenu --config-file="~/.config/openbox/jgmenu-theme-configs/$SIDEMENU_CONFIG" --csv-file=${MENU_ITEMS} 2>/dev/null

}

sidemenu() {

MENU_ITEMS=$(mktemp)

trap "rm -f ${MENU_ITEMS}" EXIT

dunstctl close-all

cat <<'EOF' > ${MENU_ITEMS}
^sep()
^sep(Menu    )
^sep()
Network Menu,polybar-buttons.sh network_manager
Audio Control,^root(audio_control)
Microphone,rb-control.sh mic_box
Bluetooth Settings,blueberry
Media Settings,thunar-volman-settings
Mount Device,polybar-buttons.sh device_menu
Display Settings,scripts-box.sh display_toggle
Screenshots,^root(screen_shots)
Look and Feel,xfce4-settings-manager
Workspaces,^root(workspaces)
WM Settings,^root(wm_settings)
Conky,^root(conky)
^sep()
^sep(Night Light    )
^sep()
Toggle,rb-night-light_run.sh
Settings,rb-night-light-settings.sh
^sep()
^sep(Dark Mode    )
^sep()
Toggle,scripts-box.sh dark_mode_toggle
Settings,rb-dark-mode-settings-test.sh
^sep()
^sep(Work Flow    )
^sep()
Daily,termite --exec="rb-workflow.sh daily"
Studio,termite --exec="rb-workflow.sh studio"
Alternative,termite --exec="rb-workflow.sh altr"
Settings,rb-workflow-settings.sh
^sep()
^sep(Session    )
^sep()
Shutdown,rb-session.sh shutdown
Reboot,rb-session.sh reboot
Suspend,^root(suspend)
Hibernate,rb-session.sh hibernate
Logout,rb-session.sh logout
Lock Screen,betterlockscreen -l

Screenshots,^tag(screen_shots)
^sep()
^sep(Desktop    )
^sep()
Now,rb-screenshot.sh --scrot-desktop_now
After 5 seconds,rb-screenshot.sh --scrot-desktop-after5
After 10 seconds,rb-screenshot.sh --scrot-desktop-after10
^sep()
^sep(Window    )
^sep()
Now,rb-screenshot.sh --scrot-window-now
After 5 seconds,rb-screenshot.sh --scrot-window-after5
After 10 seconds,rb-screenshot.sh --scrot-window-after10
^sep()
^sep(Region    )
^sep()
Capture a Region,rb-screenshot.sh --scrot-region
^sep()
<b>Back</b>,^back()

Suspend,^tag(suspend)
^sep()
^sep(Suspend    )
^sep()
Now,sleep 1; systemctl suspend
After 15 minutes,rb-session-suspend.sh susp_a15ms
After 45 minutes,rb-session-suspend.sh susp_a45ms
After 1 hour,rb-session-suspend.sh susp_a1h
After 2 hours,rb-session-suspend.sh susp_a2hs
Cancel,rb-session-suspend.sh cancel
^sep()
<b>Back</b>,^back()

Workspaces,^tag(workspaces)
^sep()
^sep(Workspaces    )
^sep()
Show workspaces,xdotool key Ctrl+0
^sep()
Add workspace,scripts-box.sh add_workspace_tint
Remove workspace,scripts-box.sh remove_workspace_tint
^sep()
Use 1 workspace, scripts-box.sh wspace1
Use 2 workspaces, scripts-box.sh wspaces2
Use 3 workspaces, scripts-box.sh wspaces3
Use 4 workspaces, scripts-box.sh wspaces4
Use 5 workspaces, scripts-box.sh wspaces5
Use 6 workspaces, scripts-box.sh wspaces6
Use 7 workspaces, scripts-box.sh wspaces7
Use 8 workspaces, scripts-box.sh wspaces8
^sep()
<b>Back</b>,^back()

Conky,^tag(conky)
^sep()
^sep(Conky    )
^sep()
Choose Conky,garuda-conkyzen
Conky Session,garuda-conky-session
Pin Conky,garuda-conkypin
Edit Conky,garuda-conkyedit
^sep()
<b>Back</b>,^back()

Audio Control,^tag(audio_control)
^sep()
^sep(Sound System    )
^sep()
ALSA,scripts-box.sh sound_system_alsa
PulseAudio,scripts-box.sh sound_system_pulseaudio
^sep()
<b>Back</b>,^back()

WM Settings,^tag(wm_settings)
^sep()
^sep(Window Stacking    )
^sep()
Smart stacking,scripts-box.sh smart_stacking
Stacking to center,scripts-box.sh stacking_center
^sep()
^sep(Window Decoration    )
^sep()
Toggle decoration,scripts-box.sh decor_toggle
^sep()
^sep(Border when undecorated    )
^sep()
Toggle borders,scripts-box.sh toggle_border && polybar-buttons.sh sidemenu
^sep()
^sep(Title Layout    )
^sep()
Classic (Left),scripts-box.sh classic_left && polybar-buttons.sh sidemenu
Classic (Right),scripts-box.sh classic_right && polybar-buttons.sh sidemenu
Custom Layout,scripts-box.sh custom_layout && polybar-buttons.sh sidemenu
Elementary (Left),scripts-box.sh elementary_left && polybar-buttons.sh sidemenu
Elementary (Right),scripts-box.sh elementary_right && polybar-buttons.sh sidemenu
GNOME (Left),scripts-box.sh gnome_left && polybar-buttons.sh sidemenu
GNOME (Right),scripts-box.sh gnome_right && polybar-buttons.sh sidemenu
Label Only,scripts-box.sh label_only && polybar-buttons.sh sidemenu
^sep()
^sep(Title Double Click    )
^sep()
Maximize,scripts-box.sh double_click_maximize && polybar-buttons.sh sidemenu
Minimize,scripts-box.sh double_click_minimize && polybar-buttons.sh sidemenu
Shade,scripts-box.sh double_click_shade && polybar-buttons.sh sidemenu
^sep()
^sep(Title Middle Click    )
^sep()
Maximize,scripts-box.sh middle_click_maximize && polybar-buttons.sh sidemenu
Minimize,scripts-box.sh middle_click_minimize && polybar-buttons.sh sidemenu
Shade,scripts-box.sh middle_click_shade && polybar-buttons.sh sidemenu
^sep()
<b>Back</b>,^back()
EOF

jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/$SIDEMENU_CONFIG" --csv-file=${MENU_ITEMS} 2>/dev/null

}

network_manager() {

ETHERNET=$(nmcli --terse --fields STATE,TYPE dev status | grep ethernet | sed 's/:.*//')
WIFI_TEST=$(nmcli radio wifi)
VPN_TEST=$(nmcli --terse --fields TYPE con show | grep vpn | head -1 | sed 's/:.*//')
MENU_ITEMS=$(mktemp)

trap "rm -f ${MENU_ITEMS}" EXIT

cat <<'EOF' > ${MENU_ITEMS}
^sep()
^sep(Wired    )
^sep()
EOF

nmcli --terse --fields NAME,TYPE con show -a | grep -v 'wireless\|vpn\|tun' | sed 's/:.*//' | sed "s/$/,nmcli con down '/" | sed 's|\([a-z]*\).*$|&&|' | sed "s/,nmcli con down '$//" | sed 's/^//' | sed "s/$/' \&\& notify-send -t 2600 'Wire connection Deactivated.'/" >> ${MENU_ITEMS}
nmcli --terse --fields NAME,TYPE,DEVICE con show | grep ethernet | grep -v 'enp' | sed 's/:.*//' | sed "s/$/,nmcli con up '/" | sed 's|\([a-z]*\).*$|&&|' | sed "s/,nmcli con up '$//" | sed 's/^//' | sed "s/$/' \&\& notify-send -t 2600 'Wire connection Activated.'/" >> ${MENU_ITEMS}

if [ "$WIFI_TEST" = "enabled" ]; then

cat <<'EOF' >> ${MENU_ITEMS}
^sep()
^sep(Wi-Fi    )
^sep()
EOF

nmcli --terse --fields NAME,TYPE con show -a | grep wireless | sed 's/:.*//' | sed "s/$/,nmcli con down '/" | sed 's|\([a-z]*\).*$|&&|' | sed "s/,nmcli con down '$//" | sed 's/^//' | sed "s/$/' \&\& notify-send -t 2600 'WiFi connection Deactivated.'/" >> ${MENU_ITEMS}
nmcli --terse --fields SSID,IN-USE dev wifi list | grep -v '*' | sed 's/:.*//' | sed "s/$/,nmcli dev wifi con '/" | sed 's|\([a-z]*\).*$|&&|' | sed "s/,nmcli dev wifi con '$//" | sed 's/^//' | sed "s/$/' \&\& notify-send -t 2600 'WiFi connection Activated.'/" >> ${MENU_ITEMS}

else

cat <<'EOF' >> ${MENU_ITEMS}
^sep()
EOF

fi

if [ "$VPN_TEST" = "vpn" ]; then

cat <<'EOF' >> ${MENU_ITEMS}
^sep()
^sep(VPN    )
^sep()
EOF

nmcli --terse --fields NAME,TYPE con show -a | grep vpn | sed 's/:.*//' | sed "s/$/,nmcli con down '/" | sed 's|\([a-z]*\).*$|&&|' | sed "s/,nmcli con down '$//" | sed 's/^//' | sed "s/$/' \&\& notify-send -t 2600 'VPN connection Deactivated.'/" >> ${MENU_ITEMS}
nmcli --terse --fields NAME,TYPE,DEVICE con show | grep vpn | grep -v 'wlp\|enp' | sed 's/:.*//' | sed "s/$/,nmcli con up '/" | sed 's|\([a-z]*\).*$|&&|' | sed "s/,nmcli con up '$//" | sed 's/^//' | sed "s/$/' \&\& notify-send -t 2600 'VPN connection Activated.'/" >> ${MENU_ITEMS}

else

cat <<'EOF' >> ${MENU_ITEMS}
^sep()
EOF

fi

cat <<'EOF' >> ${MENU_ITEMS}
^sep()
Connection Editor,nm-connection-editor
Network TUI,termite --exec="nmtui"
^sep()
<b>Back</b>,polybar-buttons.sh sidemenu
EOF

jgmenu --config-file="~/.config/openbox/jgmenu-theme-configs/$SIDEMENU_CONFIG" --csv-file=${MENU_ITEMS} 2>/dev/null

}

case "$1" in
	desktop) desktop;;
	places) places;;
	window) window;;
	tools) tools;;
	software) software;;
    info) info;;
	mount) mount;;
	unmount) unmount;;
	device_menu) device_menu;;
	sidemenu) sidemenu;;
	network_manager) network_manager;;
	version | ver) echo -e "\n    Version 3.4\n";;
	*)

echo -e "
	RecBox helper script to control polybar panel buttons\n
	Usage:\n
	polybar-buttons.sh [ OPTION ]\n
	Options:\n
	> places       - execute places menu 
	> window       - execute window menu 
	> tools        - execute tools menu
	> software     - execute software menu
	> info         - execute info menu
	> sidemenu     - execute sidemenu menu\n
	> ver, version - show script version
" >&2
exit 1;;
esac
exit 0
