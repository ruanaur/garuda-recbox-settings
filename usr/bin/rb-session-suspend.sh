#!/usr/bin/sh

susp_a15ms() {

	[ "`ps -x | grep rb-session-suspend.sh | grep -v grep | grep -v susp_ | awk '{print $1}' | head -n 1`" = "$1" ] && exec rb-session-suspend.sh a15ms || notify-send -t 2300 "System suspend process already active."

}

a15ms() {

	sleep 1; notify-send -t 2300 "System will be suspended after 15 minutes."; sleep 15m; systemctl suspend

}

susp_a45ms() {

	[ "`ps -x | grep rb-session-suspend.sh | grep -v grep | grep -v susp_ | awk '{print $1}' | head -n 1`" = "$1" ] && exec rb-session-suspend.sh a45ms || notify-send -t 2300 "System suspend process already active."

}

a45ms() {

	sleep 1; notify-send -t 2300 "System will be suspended after 45 minutes."; sleep 45m; systemctl suspend

}

susp_a1h() {

	[ "`ps -x | grep rb-session-suspend.sh | grep -v grep | grep -v susp_ | awk '{print $1}' | head -n 1`" = "$1" ] && exec rb-session-suspend.sh a1h || notify-send -t 2300 "System suspend process already active."

}

a1h() {

	sleep 1; notify-send -t 2300 "System will be suspended after 1 hour."; sleep 1h; systemctl suspend

}

susp_a2hs() {

	[ "`ps -x | grep rb-session-suspend.sh | grep -v grep | grep -v susp_ | awk '{print $1}' | head -n 1`" = "$1" ] && exec rb-session-suspend.sh a2hs || notify-send -t 2300 "System suspend process already active."

}

a2hs() {

	sleep 1; notify-send -t 2300 "System will be suspended after 2 hours."; sleep 2h; systemctl suspend

}

cancel_suspend() {

	if [ -z $(ps -x | grep 'rb-session-suspend.sh' | grep -v 'grep' | grep -v 'cancel' | awk '{print $1}') ]; then

		notify-send -t 2300 "There is nothing to cancel."

			else

				sleep 1; notify-send -t 2300 "System suspension canceled." && kill 15 $(ps -x | grep "rb-session-suspend.sh" | grep -v grep | awk '{print $1}')

	fi

}

case "$1" in

	susp_a15ms) susp_a15ms;;
	a15ms) a15ms;;
	susp_a45ms) susp_a45ms;;
	a45ms) a45ms;;
	susp_a1h) susp_a1h;;
	a1h) a1h;;
	susp_a2hs) susp_a2hs;;
	a2hs) a2hs;;
	cancel) cancel_suspend;;
	*)

	echo -e "
	RecBox Session Suspend\n
	Made by Projekt:Root projekt.root@tuta.io
	for RecBox and Garuda OpenBox.
	If you want to use script on other setup 
	probably you'll need to modify it.\n
	Usage:\n
	script-box.sh [ OPTION ]\n
	Options${RESETCOLOR}:\n
	> susp_a15ms      - suspends system after 15 minutes
	> susp_a45ms      - suspends system after 45 minutes
	> susp_a1h        - suspends system after 1 hour
	> susp_a2hs       - suspends system after 2 hours
	> cancel_suspend  - cancel system suspend
" >&2
exit 1
;;
esac

echo -e $RESETCOLOR
exit 0
