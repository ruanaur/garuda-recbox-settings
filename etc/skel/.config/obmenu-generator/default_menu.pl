#!/usr/bin/perl

require "$ENV{HOME}/.config/obmenu-generator/config.pl";

my $editor = $CONFIG->{editor};

our $SCHEMA = [

  {sep => 'RecBox'},

  {item => ['exo-open --launch WebBrowser',                '  Web Browser']},
  {item => ['exo-open --launch TerminalEmulator',          '  Terminal']},
  {item => ['exo-open --launch FileManager',               '  File Manager']},

  {sep => 'Categories'},

  {beg => ['꜒  Accessories',                               'accessories']},

    {item => ['catfish',                                   '•  Catfish']},
    {item => ['micro %F',                                  '•  Micro']},
    {item => ['xed',                                       '•  Xed']},

  {end => undef},

  {beg => ['꜔  Studio',                                    'studio']},

    {item => ['ardour5',                                   '•  REC ']},
    {item => ['guitarix',                                  '•  Guitarix']},
    {item => ['hydrogen',                                  '•  Hydrogen']},
    {item => ['helm-synth',                                '•  Helm']},
    {item => ['carla-control',                             '•  CarlaCtl']},
    {sep => undef},
    {item => ['kid3',                                      '•  MetaData']},

  {end => undef},

  {beg => ['꜔  Multimedia',                                'multimedia']},

    {item => ['termite --exec="cmus"',                     '•  Cmus']},
    {item => ['deadbeef',                                  '•  DeaDBeeF']},
    {item => ['mpv --player-operation-mode=pseudo-gui --', '•  MPV']},
    {item => ['smplayer',                                  '•  SMPlayer']},

  {end => undef},

  {beg => ['꜔  Graphics',                                  'graphics']},

    {item => ['gimp',                                      '•  GIMP']},
    {item => ['inkscape',                                  '•  InkScape']},

  {end => undef},

  {beg => ['꜔  Development',                               'development']},

    {item => ['meld',                                      '•  Meld']},

  {end => undef},

  {beg => ['꜖  Office',                                    'office']},

    {item => ['libreoffice',                               '•  Libre Office']},
    {item => ['qpdfview',                                  '•  PDF Reader']},

  {end => undef},

  {sep => 'Extra'},

  {item => ['rb-center.sh main',                           '  RecBox Center']},
  {item => ['scripts-box.sh pdf_menu',                     '  Documentation']},
  {item => ['RecBox-about.py',                             '  About RecBox']},

  {sep => 'Settings'},

  {item => ['xfce4-settings-manager',                      '  Look and Feel']},
  {item => ['scripts-box.sh obmenu_static',                '  RecBox Settings']},

  {beg => ['  Openbox Settings',                                   'openBox']},

    {item => ['exo-open ~/.config/obmenu-generator/default_menu.pl', '  Edit menu']},
    {item => ['scripts-box.sh default_menu',                         '  Reconfigure menu']},

    {sep => undef},

    {item => ['scripts-box.sh simple_menu',                '  Switch to simple menu']},
    {item => ['openbox --reconfigure',                     '  Reconfigure Openbox']},
    {item => ['openbox --restart',                         '  Restart Openbox']},

  {end => undef},

  {beg => ['  Work Flow',                                 'workflow']},

    {item => ['termite --exec="rb-workflow.sh daily"',      '  Daily']},
    {item => ['termite --exec="rb-workflow.sh studio"',     '  Studio']},
    {item => ['termite --exec="rb-workflow.sh altr"',       '  Alternative']},

  {end => undef},

  {sep => undef},

  {item => ['betterlockscreen -l',                          '  Lock']},
  {item => ['scripts-box.sh rofi_logout',                   '  Exit']},

]
