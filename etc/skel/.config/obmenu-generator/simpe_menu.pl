#!/usr/bin/perl

require "$ENV{HOME}/.config/obmenu-generator/config.pl";

my $editor = $CONFIG->{editor};

our $SCHEMA = [

  {item => ['exo-open --launch WebBrowser',                       '  Open Web Browser']},
  {item => ['exo-open --launch FileManager',                      '  Open File Manager']},

  {sep => undef},

  {item => ['exo-open --launch TerminalEmulator',                 '  Open Terminal Emulator']},
  {item => ['xed',                                                '  Open Text Editor']},

  {sep => undef},

  {beg => ['  Openbox Settings',                                 'openBox']},

    {item => ['exo-open ~/.config/obmenu-generator/simpe_menu.pl', '  Edit menu']},
    {item => ['scripts-box.sh simple_menu',                        '  Reconfigure menu']},

  {sep => undef},

    {item => ['scripts-box.sh default_menu',                          '  Switch to default menu']},
    {item => ['openbox --reconfigure',                                '  Reconfigure Openbox']},
    {item => ['openbox --restart',                                    '  Restart Openbox']},

  {end => undef},

  {item => ['nitrogen',                                           '  Change Wallpaper']},
  {item => ['betterlockscreen -l',                                '  Lock Screen']},

]
